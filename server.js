require("source-map-support").install();
module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// object to store loaded chunks
/******/ 	// "0" means "already loaded"
/******/ 	var installedChunks = {
/******/ 		1: 0
/******/ 	};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/ 	// This file contains only the entry chunk.
/******/ 	// The chunk loading function for additional chunks
/******/ 	__webpack_require__.e = function requireEnsure(chunkId) {
/******/ 		// "0" is the signal for "already loaded"
/******/ 		if(installedChunks[chunkId] !== 0) {
/******/ 			var chunk = require("./chunks/" + ({"0":"not-found"}[chunkId]||chunkId) + ".js");
/******/ 			var moreModules = chunk.modules, chunkIds = chunk.ids;
/******/ 			for(var moduleId in moreModules) {
/******/ 				modules[moduleId] = moreModules[moduleId];
/******/ 			}
/******/ 			for(var i = 0; i < chunkIds.length; i++)
/******/ 				installedChunks[chunkIds[i]] = 0;
/******/ 		}
/******/ 		return Promise.resolve();
/******/ 	};
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/assets/";
/******/
/******/ 	// uncatched error handler for webpack runtime
/******/ 	__webpack_require__.oe = function(err) {
/******/ 		process.nextTick(function() {
/******/ 			throw err; // catch this error by using System.import().catch()
/******/ 		});
/******/ 	};
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 25);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

module.exports = require("react");

/***/ }),
/* 1 */
/***/ (function(module, exports) {

module.exports = require("prop-types");

/***/ }),
/* 2 */
/***/ (function(module, exports) {

module.exports = require("antd");

/***/ }),
/* 3 */
/***/ (function(module, exports) {

module.exports = require("react-apollo");

/***/ }),
/* 4 */
/***/ (function(module, exports) {

module.exports = require("react-redux");

/***/ }),
/* 5 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "h", function() { return UPDATE_PROTOCOL_NEW_ENTRY; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "g", function() { return UPDATE_PREPROCESSING_NEW_ENTRY; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "j", function() { return UPDATE_SOURCEDATA_NEW_ENTRY; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return UPDATE_DOMAIN_NEW_ENTRY; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "k", function() { return UPDATE_STANDARD_NEW_ENTRY; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return COMMIT_CREATION_NEW_ENTRY; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "i", function() { return UPDATE_SOURCEDATA_FROM_API_NEW_ENTRY; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ADD_ENTRY_TO_USER_SELECTION; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "l", function() { return UPDATE_USER_SELECTION; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return RESET_USER_SELECTION; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "e", function() { return UPDATE_OUTPUT_TARGET; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "f", function() { return UPDATE_OUTPUT_VCS; });
// NEW ENTRY
var UPDATE_PROTOCOL_NEW_ENTRY = 'UPDATE_PROTOCOL_NEW_ENTRY';
var UPDATE_PREPROCESSING_NEW_ENTRY = 'UPDATE_PREPROCESSING_NEW_ENTRY';
var UPDATE_SOURCEDATA_NEW_ENTRY = 'UPDATE_SOURCEDATA_NEW_ENTRY';
var UPDATE_DOMAIN_NEW_ENTRY = 'UPDATE_DOMAIN_NEW_ENTRY';
var UPDATE_STANDARD_NEW_ENTRY = 'UPDATE_STANDARD_NEW_ENTRY';
var COMMIT_CREATION_NEW_ENTRY = 'COMMIT_CREATION_NEW_ENTRY'; // YP API DATA

var UPDATE_SOURCEDATA_FROM_API_NEW_ENTRY = 'UPDATE_SOURCEDATA_FROM_API_NEW_ENTRY'; // USER_SELECTION

var ADD_ENTRY_TO_USER_SELECTION = 'ADD_ENTRY_TO_USER_SELECTION';
var UPDATE_USER_SELECTION = 'UPDATE_USER_SELECTION';
var RESET_USER_SELECTION = 'RESET_USER_SELECTION';
var UPDATE_OUTPUT_TARGET = 'UPDATE_OUTPUT_TARGET';
var UPDATE_OUTPUT_VCS = 'UPDATE_OUTPUT_VCS';

/***/ }),
/* 6 */
/***/ (function(module, exports) {

module.exports = require("isomorphic-style-loader/lib/withStyles");

/***/ }),
/* 7 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__queries__ = __webpack_require__(67);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__mutations__ = __webpack_require__(68);
/* harmony reexport (module object) */ __webpack_require__.d(__webpack_exports__, "b", function() { return __WEBPACK_IMPORTED_MODULE_0__queries__; });
/* harmony reexport (module object) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_1__mutations__; });




/***/ }),
/* 8 */
/***/ (function(module, exports) {

module.exports = require("semantic-ui-react");

/***/ }),
/* 9 */
/***/ (function(module, exports) {

/*
	MIT License http://www.opensource.org/licenses/mit-license.php
	Author Tobias Koppers @sokra
*/
// css base code, injected by the css-loader
module.exports = function(useSourceMap) {
	var list = [];

	// return the list of modules as css string
	list.toString = function toString() {
		return this.map(function (item) {
			var content = cssWithMappingToString(item, useSourceMap);
			if(item[2]) {
				return "@media " + item[2] + "{" + content + "}";
			} else {
				return content;
			}
		}).join("");
	};

	// import a list of modules into the list
	list.i = function(modules, mediaQuery) {
		if(typeof modules === "string")
			modules = [[null, modules, ""]];
		var alreadyImportedModules = {};
		for(var i = 0; i < this.length; i++) {
			var id = this[i][0];
			if(typeof id === "number")
				alreadyImportedModules[id] = true;
		}
		for(i = 0; i < modules.length; i++) {
			var item = modules[i];
			// skip already imported module
			// this implementation is not 100% perfect for weird media query combinations
			//  when a module is imported multiple times with different media queries.
			//  I hope this will never occur (Hey this way we have smaller bundles)
			if(typeof item[0] !== "number" || !alreadyImportedModules[item[0]]) {
				if(mediaQuery && !item[2]) {
					item[2] = mediaQuery;
				} else if(mediaQuery) {
					item[2] = "(" + item[2] + ") and (" + mediaQuery + ")";
				}
				list.push(item);
			}
		}
	};
	return list;
};

function cssWithMappingToString(item, useSourceMap) {
	var content = item[1] || '';
	var cssMapping = item[3];
	if (!cssMapping) {
		return content;
	}

	if (useSourceMap && typeof btoa === 'function') {
		var sourceMapping = toComment(cssMapping);
		var sourceURLs = cssMapping.sources.map(function (source) {
			return '/*# sourceURL=' + cssMapping.sourceRoot + source + ' */'
		});

		return [content].concat(sourceURLs).concat([sourceMapping]).join('\n');
	}

	return [content].join('\n');
}

// Adapted from convert-source-map (MIT)
function toComment(sourceMap) {
	// eslint-disable-next-line no-undef
	var base64 = btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap))));
	var data = 'sourceMappingURL=data:application/json;charset=utf-8;base64,' + base64;

	return '/*# ' + data + ' */';
}


/***/ }),
/* 10 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _stringify = __webpack_require__(48);

var _stringify2 = _interopRequireDefault(_stringify);

var _slicedToArray2 = __webpack_require__(49);

var _slicedToArray3 = _interopRequireDefault(_slicedToArray2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Isomorphic CSS style loader for Webpack
 *
 * Copyright © 2015-present Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

var prefix = 's';
var inserted = {};

// Base64 encoding and decoding - The "Unicode Problem"
// https://developer.mozilla.org/en-US/docs/Web/API/WindowBase64/Base64_encoding_and_decoding#The_Unicode_Problem
function b64EncodeUnicode(str) {
  return btoa(encodeURIComponent(str).replace(/%([0-9A-F]{2})/g, function (match, p1) {
    return String.fromCharCode('0x' + p1);
  }));
}

/**
 * Remove style/link elements for specified node IDs
 * if they are no longer referenced by UI components.
 */
function removeCss(ids) {
  ids.forEach(function (id) {
    if (--inserted[id] <= 0) {
      var elem = document.getElementById(prefix + id);
      if (elem) {
        elem.parentNode.removeChild(elem);
      }
    }
  });
}

/**
 * Example:
 *   // Insert CSS styles object generated by `css-loader` into DOM
 *   var removeCss = insertCss([[1, 'body { color: red; }']]);
 *
 *   // Remove it from the DOM
 *   removeCss();
 */
function insertCss(styles) {
  var _ref = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {},
      _ref$replace = _ref.replace,
      replace = _ref$replace === undefined ? false : _ref$replace,
      _ref$prepend = _ref.prepend,
      prepend = _ref$prepend === undefined ? false : _ref$prepend;

  var ids = [];
  for (var i = 0; i < styles.length; i++) {
    var _styles$i = (0, _slicedToArray3.default)(styles[i], 4),
        moduleId = _styles$i[0],
        css = _styles$i[1],
        media = _styles$i[2],
        sourceMap = _styles$i[3];

    var id = moduleId + '-' + i;

    ids.push(id);

    if (inserted[id]) {
      if (!replace) {
        inserted[id]++;
        continue;
      }
    }

    inserted[id] = 1;

    var elem = document.getElementById(prefix + id);
    var create = false;

    if (!elem) {
      create = true;

      elem = document.createElement('style');
      elem.setAttribute('type', 'text/css');
      elem.id = prefix + id;

      if (media) {
        elem.setAttribute('media', media);
      }
    }

    var cssText = css;
    if (sourceMap && typeof btoa === 'function') {
      // skip IE9 and below, see http://caniuse.com/atob-btoa
      cssText += '\n/*# sourceMappingURL=data:application/json;base64,' + b64EncodeUnicode((0, _stringify2.default)(sourceMap)) + '*/';
      cssText += '\n/*# sourceURL=' + sourceMap.file + '?' + id + '*/';
    }

    if ('textContent' in elem) {
      elem.textContent = cssText;
    } else {
      elem.styleSheet.cssText = cssText;
    }

    if (create) {
      if (prepend) {
        document.head.insertBefore(elem, document.head.childNodes[0]);
      } else {
        document.head.appendChild(elem);
      }
    }
  }

  return removeCss.bind(null, ids);
}

module.exports = insertCss;

/***/ }),
/* 11 */
/***/ (function(module, exports, __webpack_require__) {


    var content = __webpack_require__(70);
    var insertCss = __webpack_require__(10);

    if (typeof content === 'string') {
      content = [[module.i, content, '']];
    }

    module.exports = content.locals || {};
    module.exports._getContent = function() { return content; };
    module.exports._getCss = function() { return content.toString(); };
    module.exports._insertCss = function(options) { return insertCss(content, options) };
    
    // Hot Module Replacement
    // https://webpack.github.io/docs/hot-module-replacement
    // Only activated in browser context
    if (false) {
      var removeCss = function() {};
      module.hot.accept("!!../../node_modules/css-loader/index.js??ref--2-rules-2!../../node_modules/postcss-loader/lib/index.js??ref--2-rules-3!./style.scss", function() {
        content = require("!!../../node_modules/css-loader/index.js??ref--2-rules-2!../../node_modules/postcss-loader/lib/index.js??ref--2-rules-3!./style.scss");

        if (typeof content === 'string') {
          content = [[module.id, content, '']];
        }

        removeCss = insertCss(content, { replace: true });
      });
      module.hot.dispose(function() { removeCss(); });
    }
  

/***/ }),
/* 12 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Link; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_prop_types__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__history__ = __webpack_require__(57);
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }





var isLeftClickEvent = function isLeftClickEvent(event) {
  return event.button === 0;
};

var isModifiedEvent = function isModifiedEvent(event) {
  return !!(event.metaKey || event.altKey || event.ctrlKey || event.shiftKey);
};

var Link =
/*#__PURE__*/
function (_Component) {
  _inherits(Link, _Component);

  function Link() {
    var _ref;

    var _temp, _this;

    _classCallCheck(this, Link);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _possibleConstructorReturn(_this, (_temp = _this = _possibleConstructorReturn(this, (_ref = Link.__proto__ || Object.getPrototypeOf(Link)).call.apply(_ref, [this].concat(args))), Object.defineProperty(_assertThisInitialized(_this), "handleClick", {
      configurable: true,
      enumerable: true,
      writable: true,
      value: function value(event) {
        if (_this.props.onClick) {
          _this.props.onClick(event);
        }

        if (isModifiedEvent(event) || !isLeftClickEvent(event)) {
          return;
        }

        if (event.defaultPrevented === true) {
          return;
        }

        event.preventDefault(); // $FlowFixMe

        __WEBPACK_IMPORTED_MODULE_2__history__["a" /* default */].push(_this.props.to);
      }
    }), _temp));
  }

  _createClass(Link, [{
    key: "render",
    value: function render() {
      var _props = this.props,
          to = _props.to,
          children = _props.children,
          props = _objectWithoutProperties(_props, ["to", "children"]);

      return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("a", _extends({
        href: to
      }, props, {
        onClick: this.handleClick,
        style: {
          textDecoration: 'none'
        }
      }), children);
    }
  }]);

  return Link;
}(__WEBPACK_IMPORTED_MODULE_0_react__["Component"]);

Object.defineProperty(Link, "defaultProps", {
  configurable: true,
  enumerable: true,
  writable: true,
  value: {
    onClick: null
  }
});


/***/ }),
/* 13 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Styled; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_prop_types__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_isomorphic_style_loader_lib_withStyles__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_isomorphic_style_loader_lib_withStyles___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_isomorphic_style_loader_lib_withStyles__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__Layout_css__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__Layout_css___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3__Layout_css__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_components_Header__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_semantic_ui_react__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_semantic_ui_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_semantic_ui_react__);
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

var REACT_ELEMENT_TYPE;

function _jsx(type, props, key, children) { if (!REACT_ELEMENT_TYPE) { REACT_ELEMENT_TYPE = typeof Symbol === "function" && Symbol.for && Symbol.for("react.element") || 0xeac7; } var defaultProps = type && type.defaultProps; var childrenLength = arguments.length - 3; if (!props && childrenLength !== 0) { props = {}; } if (props && defaultProps) { for (var propName in defaultProps) { if (props[propName] === void 0) { props[propName] = defaultProps[propName]; } } } else if (!props) { props = defaultProps || {}; } if (childrenLength === 1) { props.children = children; } else if (childrenLength > 1) { var childArray = new Array(childrenLength); for (var i = 0; i < childrenLength; i++) { childArray[i] = arguments[i + 3]; } props.children = childArray; } return { $$typeof: REACT_ELEMENT_TYPE, type: type, key: key === undefined ? null : '' + key, ref: null, props: props, _owner: null }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }








var _ref2 = _jsx(__WEBPACK_IMPORTED_MODULE_4_components_Header__["a" /* Header */], {});

var _ref3 = _jsx(__WEBPACK_IMPORTED_MODULE_5_semantic_ui_react__["Divider"], {});

var Layout =
/*#__PURE__*/
function (_PureComponent) {
  _inherits(Layout, _PureComponent);

  function Layout() {
    var _ref;

    var _temp, _this;

    _classCallCheck(this, Layout);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _possibleConstructorReturn(_this, (_temp = _this = _possibleConstructorReturn(this, (_ref = Layout.__proto__ || Object.getPrototypeOf(Layout)).call.apply(_ref, [this].concat(args))), Object.defineProperty(_assertThisInitialized(_this), "componentDidMount", {
      configurable: true,
      enumerable: true,
      writable: true,
      value: function value() {
        return console.log('mounted');
      }
    }), Object.defineProperty(_assertThisInitialized(_this), "componentWillReceiveProps", {
      configurable: true,
      enumerable: true,
      writable: true,
      value: function value() {
        return console.log('will receive props');
      }
    }), Object.defineProperty(_assertThisInitialized(_this), "render", {
      configurable: true,
      enumerable: true,
      writable: true,
      value: function value() {
        return _jsx(__WEBPACK_IMPORTED_MODULE_0_react__["Fragment"], {}, void 0, _ref2, _ref3, _this.props.children);
      }
    }), _temp));
  }

  return Layout;
}(__WEBPACK_IMPORTED_MODULE_0_react__["PureComponent"]);

var Styled = __WEBPACK_IMPORTED_MODULE_2_isomorphic_style_loader_lib_withStyles___default()(__WEBPACK_IMPORTED_MODULE_3__Layout_css___default.a)(Layout);


/***/ }),
/* 14 */
/***/ (function(module, exports) {

module.exports = require("bluebird");

/***/ }),
/* 15 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return App; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_prop_types__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_react_redux__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_react_redux___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_react_redux__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_react_apollo__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_react_apollo___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_react_apollo__);
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

var REACT_ELEMENT_TYPE;

function _jsx(type, props, key, children) { if (!REACT_ELEMENT_TYPE) { REACT_ELEMENT_TYPE = typeof Symbol === "function" && Symbol.for && Symbol.for("react.element") || 0xeac7; } var defaultProps = type && type.defaultProps; var childrenLength = arguments.length - 3; if (!props && childrenLength !== 0) { props = {}; } if (props && defaultProps) { for (var propName in defaultProps) { if (props[propName] === void 0) { props[propName] = defaultProps[propName]; } } } else if (!props) { props = defaultProps || {}; } if (childrenLength === 1) { props.children = children; } else if (childrenLength > 1) { var childArray = new Array(childrenLength); for (var i = 0; i < childrenLength; i++) { childArray[i] = arguments[i + 3]; } props.children = childArray; } return { $$typeof: REACT_ELEMENT_TYPE, type: type, key: key === undefined ? null : '' + key, ref: null, props: props, _owner: null }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }






var ContextType = _extends({
  // Enables critical path CSS rendering
  // https://github.com/kriasoft/isomorphic-style-loader
  insertCss: __WEBPACK_IMPORTED_MODULE_1_prop_types___default.a.func.isRequired,
  // Universal HTTP client
  fetch: __WEBPACK_IMPORTED_MODULE_1_prop_types___default.a.func.isRequired,
  pathname: __WEBPACK_IMPORTED_MODULE_1_prop_types___default.a.string.isRequired,
  query: __WEBPACK_IMPORTED_MODULE_1_prop_types___default.a.object
}, __WEBPACK_IMPORTED_MODULE_2_react_redux__["Provider"].childContextTypes, {
  // Apollo Client
  client: __WEBPACK_IMPORTED_MODULE_1_prop_types___default.a.object.isRequired
  /**
   * The top-level React component setting context (global) variables
   * that can be accessed from all the child components.
   *
   * https://facebook.github.io/react/docs/context.html
   *
   * Usage example:
   *
   *   const context = {
   *     history: createBrowserHistory(),
   *     store: createStore(),
   *   };
   *
   *   ReactDOM.render(
   *     <App context={context}>
   *       <Layout>
   *         <LandingPage />
   *       </Layout>
   *     </App>,
   *     container,
   *   );
   */

});

var App =
/*#__PURE__*/
function (_Component) {
  _inherits(App, _Component);

  function App() {
    var _ref;

    var _temp, _this;

    _classCallCheck(this, App);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _possibleConstructorReturn(_this, (_temp = _this = _possibleConstructorReturn(this, (_ref = App.__proto__ || Object.getPrototypeOf(App)).call.apply(_ref, [this].concat(args))), Object.defineProperty(_assertThisInitialized(_this), "getChildContext", {
      configurable: true,
      enumerable: true,
      writable: true,
      value: function value() {
        return _this.props.context;
      }
    }), Object.defineProperty(_assertThisInitialized(_this), "render", {
      configurable: true,
      enumerable: true,
      writable: true,
      value: function value() {
        var client = _this.props.context.client;
        return _jsx(__WEBPACK_IMPORTED_MODULE_3_react_apollo__["ApolloProvider"], {
          client: client
        }, void 0, _this.props.children);
      }
    }), _temp));
  } // .Children.only(this.props.children)


  return App;
}(__WEBPACK_IMPORTED_MODULE_0_react__["Component"]);

Object.defineProperty(App, "childContextTypes", {
  configurable: true,
  enumerable: true,
  writable: true,
  value: ContextType
});


/***/ }),
/* 16 */
/***/ (function(module, exports, __webpack_require__) {

/* global process */
if (false) {
  throw new Error('Do not import `config.js` from inside the client-side code.');
}

module.exports = {
  // Node.js app
  port: process.env.PORT || 3000,
  // API Gateway
  api: {
    // API URL to be used in the client-side code
    clientUrl: process.env.API_CLIENT_URL || '',
    // API URL to be used in the server-side code
    serverUrl: process.env.API_SERVER_URL || "http://localhost:".concat(process.env.PORT || 3000)
  },
  // Database
  databaseUrl: process.env.DATABASE_URL || 'sqlite:database.sqlite',
  // Web analytics
  analytics: {
    // https://analytics.google.com/
    googleTrackingId: process.env.GOOGLE_TRACKING_ID // UA-XXXXX-X

  },
  // Authentication
  auth: {
    jwt: {
      secret: process.env.JWT_SECRET || 'React Starter Kit'
    },
    // https://developers.facebook.com/
    facebook: {
      id: process.env.FACEBOOK_APP_ID || '186244551745631',
      secret: process.env.FACEBOOK_APP_SECRET || 'a970ae3240ab4b9b8aae0f9f0661c6fc'
    },
    // https://cloud.google.com/console/project
    google: {
      id: process.env.GOOGLE_CLIENT_ID || '251410730550-ahcg0ou5mgfhl8hlui1urru7jn5s12km.apps.googleusercontent.com',
      secret: process.env.GOOGLE_CLIENT_SECRET || 'Y8yR9yZAhm9jQ8FKAL8QIEcd'
    },
    // https://apps.twitter.com/
    twitter: {
      key: process.env.TWITTER_CONSUMER_KEY || 'Ie20AZvLJI2lQD5Dsgxgjauns',
      secret: process.env.TWITTER_CONSUMER_SECRET || 'KTZ6cxoKnEakQCeSpZlaUCJWGAlTEBJj0y2EMkUBujA7zWSvaQ'
    }
  }
};

/***/ }),
/* 17 */
/***/ (function(module, exports, __webpack_require__) {


    var content = __webpack_require__(47);
    var insertCss = __webpack_require__(10);

    if (typeof content === 'string') {
      content = [[module.i, content, '']];
    }

    module.exports = content.locals || {};
    module.exports._getContent = function() { return content; };
    module.exports._getCss = function() { return content.toString(); };
    module.exports._insertCss = function(options) { return insertCss(content, options) };
    
    // Hot Module Replacement
    // https://webpack.github.io/docs/hot-module-replacement
    // Only activated in browser context
    if (false) {
      var removeCss = function() {};
      module.hot.accept("!!../../../node_modules/css-loader/index.js??ref--2-rules-2!../../../node_modules/postcss-loader/lib/index.js??ref--2-rules-3!./ErrorPage.css", function() {
        content = require("!!../../../node_modules/css-loader/index.js??ref--2-rules-2!../../../node_modules/postcss-loader/lib/index.js??ref--2-rules-3!./ErrorPage.css");

        if (typeof content === 'string') {
          content = [[module.id, content, '']];
        }

        removeCss = insertCss(content, { replace: true });
      });
      module.hot.dispose(function() { removeCss(); });
    }
  

/***/ }),
/* 18 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Styled; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_components_Link__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_components_Navigation__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_isomorphic_style_loader_lib_withStyles__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_isomorphic_style_loader_lib_withStyles___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_isomorphic_style_loader_lib_withStyles__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__Header_css__ = __webpack_require__(61);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__Header_css___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4__Header_css__);
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

var REACT_ELEMENT_TYPE;

function _jsx(type, props, key, children) { if (!REACT_ELEMENT_TYPE) { REACT_ELEMENT_TYPE = typeof Symbol === "function" && Symbol.for && Symbol.for("react.element") || 0xeac7; } var defaultProps = type && type.defaultProps; var childrenLength = arguments.length - 3; if (!props && childrenLength !== 0) { props = {}; } if (props && defaultProps) { for (var propName in defaultProps) { if (props[propName] === void 0) { props[propName] = defaultProps[propName]; } } } else if (!props) { props = defaultProps || {}; } if (childrenLength === 1) { props.children = children; } else if (childrenLength > 1) { var childArray = new Array(childrenLength); for (var i = 0; i < childrenLength; i++) { childArray[i] = arguments[i + 3]; } props.children = childArray; } return { $$typeof: REACT_ELEMENT_TYPE, type: type, key: key === undefined ? null : '' + key, ref: null, props: props, _owner: null }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }







var _ref2 = _jsx(__WEBPACK_IMPORTED_MODULE_2_components_Navigation__["a" /* Navigation */], {});

var Header =
/*#__PURE__*/
function (_PureComponent) {
  _inherits(Header, _PureComponent);

  function Header() {
    var _ref;

    var _temp, _this;

    _classCallCheck(this, Header);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _possibleConstructorReturn(_this, (_temp = _this = _possibleConstructorReturn(this, (_ref = Header.__proto__ || Object.getPrototypeOf(Header)).call.apply(_ref, [this].concat(args))), Object.defineProperty(_assertThisInitialized(_this), "render", {
      configurable: true,
      enumerable: true,
      writable: true,
      value: function value() {
        return _jsx("div", {
          className: __WEBPACK_IMPORTED_MODULE_4__Header_css___default.a.root
        }, void 0, _jsx("div", {
          className: __WEBPACK_IMPORTED_MODULE_4__Header_css___default.a.container
        }, void 0, _ref2, _jsx(__WEBPACK_IMPORTED_MODULE_1_components_Link__["a" /* Link */], {
          className: __WEBPACK_IMPORTED_MODULE_4__Header_css___default.a.brand,
          to: "/"
        }, void 0, _jsx("img", {
          src: "https://www.novartis.com/sites/all/themes/novartis/novartis-logo.svg",
          width: "120",
          height: "38",
          alt: "Novartis",
          style: {
            paddingTop: '0.3em'
          }
        }))));
      }
    }), _temp));
  }

  return Header;
}(__WEBPACK_IMPORTED_MODULE_0_react__["PureComponent"]);

var Styled = __WEBPACK_IMPORTED_MODULE_3_isomorphic_style_loader_lib_withStyles___default()(__WEBPACK_IMPORTED_MODULE_4__Header_css___default.a)(Header);


/***/ }),
/* 19 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Styled; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_isomorphic_style_loader_lib_withStyles__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_isomorphic_style_loader_lib_withStyles___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_isomorphic_style_loader_lib_withStyles__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__Navigation_css__ = __webpack_require__(59);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__Navigation_css___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2__Navigation_css__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_antd__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_antd___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_antd__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_components_Link__ = __webpack_require__(12);
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

var REACT_ELEMENT_TYPE;

function _jsx(type, props, key, children) { if (!REACT_ELEMENT_TYPE) { REACT_ELEMENT_TYPE = typeof Symbol === "function" && Symbol.for && Symbol.for("react.element") || 0xeac7; } var defaultProps = type && type.defaultProps; var childrenLength = arguments.length - 3; if (!props && childrenLength !== 0) { props = {}; } if (props && defaultProps) { for (var propName in defaultProps) { if (props[propName] === void 0) { props[propName] = defaultProps[propName]; } } } else if (!props) { props = defaultProps || {}; } if (childrenLength === 1) { props.children = children; } else if (childrenLength > 1) { var childArray = new Array(childrenLength); for (var i = 0; i < childrenLength; i++) { childArray[i] = arguments[i + 3]; } props.children = childArray; } return { $$typeof: REACT_ELEMENT_TYPE, type: type, key: key === undefined ? null : '' + key, ref: null, props: props, _owner: null }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }







var _ref = _jsx(__WEBPACK_IMPORTED_MODULE_3_antd__["Icon"], {
  type: "appstore-o"
});

var _ref2 = _jsx(__WEBPACK_IMPORTED_MODULE_3_antd__["Icon"], {
  type: "paper-clip"
});

var _ref3 = _jsx(__WEBPACK_IMPORTED_MODULE_3_antd__["Icon"], {
  type: "user"
});

var Navigation =
/*#__PURE__*/
function (_React$Component) {
  _inherits(Navigation, _React$Component);

  function Navigation() {
    _classCallCheck(this, Navigation);

    return _possibleConstructorReturn(this, (Navigation.__proto__ || Object.getPrototypeOf(Navigation)).apply(this, arguments));
  }

  _createClass(Navigation, [{
    key: "render",
    value: function render() {
      return _jsx("div", {
        role: "navigation",
        style: {
          float: 'right',
          padding: '0.3em'
        }
      }, void 0, _jsx("a", {
        rel: "noopener noreferrer",
        className: __WEBPACK_IMPORTED_MODULE_2__Navigation_css___default.a.link,
        href: "http://go/am",
        target: "_blank"
      }, void 0, _ref, "\xA0Applications"), _jsx(__WEBPACK_IMPORTED_MODULE_4_components_Link__["a" /* Link */], {
        className: __WEBPACK_IMPORTED_MODULE_2__Navigation_css___default.a.link,
        to: "/"
      }, void 0, _ref2, "\xA0Metadata Manager"), _jsx(__WEBPACK_IMPORTED_MODULE_4_components_Link__["a" /* Link */], {
        className: __WEBPACK_IMPORTED_MODULE_2__Navigation_css___default.a.link,
        to: "/user"
      }, void 0, _ref3, "\xA0User"));
    }
  }]);

  return Navigation;
}(__WEBPACK_IMPORTED_MODULE_0_react___default.a.Component);

var Styled = __WEBPACK_IMPORTED_MODULE_1_isomorphic_style_loader_lib_withStyles___default()(__WEBPACK_IMPORTED_MODULE_2__Navigation_css___default.a)(Navigation);


/***/ }),
/* 20 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Styled; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_semantic_ui_react__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_semantic_ui_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_semantic_ui_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_isomorphic_style_loader_lib_withStyles__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_isomorphic_style_loader_lib_withStyles___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_isomorphic_style_loader_lib_withStyles__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__Home_css__ = __webpack_require__(63);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__Home_css___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3__Home_css__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_antd__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_antd___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_antd__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_components__ = __webpack_require__(65);
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

var REACT_ELEMENT_TYPE;

function _jsx(type, props, key, children) { if (!REACT_ELEMENT_TYPE) { REACT_ELEMENT_TYPE = typeof Symbol === "function" && Symbol.for && Symbol.for("react.element") || 0xeac7; } var defaultProps = type && type.defaultProps; var childrenLength = arguments.length - 3; if (!props && childrenLength !== 0) { props = {}; } if (props && defaultProps) { for (var propName in defaultProps) { if (props[propName] === void 0) { props[propName] = defaultProps[propName]; } } } else if (!props) { props = defaultProps || {}; } if (childrenLength === 1) { props.children = children; } else if (childrenLength > 1) { var childArray = new Array(childrenLength); for (var i = 0; i < childrenLength; i++) { childArray[i] = arguments[i + 3]; } props.children = childArray; } return { $$typeof: REACT_ELEMENT_TYPE, type: type, key: key === undefined ? null : '' + key, ref: null, props: props, _owner: null }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

 // import PropTypes from 'prop-types';





var TabPane = __WEBPACK_IMPORTED_MODULE_4_antd__["Tabs"].TabPane;


var _ref = _jsx(TabPane, {
  tab: _jsx("span", {}, void 0, _jsx(__WEBPACK_IMPORTED_MODULE_4_antd__["Icon"], {
    type: "search"
  }), "Search Entries")
}, "1", _jsx(__WEBPACK_IMPORTED_MODULE_5_components__["b" /* SearchEntries */], {}));

var _ref2 = _jsx(TabPane, {
  tab: _jsx("span", {}, void 0, _jsx(__WEBPACK_IMPORTED_MODULE_4_antd__["Icon"], {
    type: "edit"
  }), "New Entry")
}, "2", _jsx(__WEBPACK_IMPORTED_MODULE_5_components__["a" /* NewEntry */], {}));

var _ref3 = _jsx(TabPane, {
  tab: _jsx("span", {}, void 0, _jsx(__WEBPACK_IMPORTED_MODULE_4_antd__["Icon"], {
    type: "upload"
  }), "Upload CSV")
}, "3", _jsx(__WEBPACK_IMPORTED_MODULE_5_components__["d" /* Upload */], {}));

var _ref4 = _jsx(TabPane, {
  tab: _jsx("span", {}, void 0, _jsx(__WEBPACK_IMPORTED_MODULE_4_antd__["Icon"], {
    type: "setting"
  }), "Settings")
}, "4", _jsx(__WEBPACK_IMPORTED_MODULE_5_components__["c" /* Settings */], {}));

var _ref5 = _jsx("br", {});

var _ref6 = _jsx("br", {});

var _ref7 = _jsx("br", {});

var Home =
/*#__PURE__*/
function (_Component) {
  _inherits(Home, _Component);

  function Home(props) {
    var _this;

    _classCallCheck(this, Home);

    _this = _possibleConstructorReturn(this, (Home.__proto__ || Object.getPrototypeOf(Home)).call(this, props));
    Object.defineProperty(_assertThisInitialized(_this), "render", {
      configurable: true,
      enumerable: true,
      writable: true,
      value: function value() {
        return _jsx("div", {
          className: __WEBPACK_IMPORTED_MODULE_3__Home_css___default.a.root
        }, void 0, _jsx(__WEBPACK_IMPORTED_MODULE_4_antd__["Tabs"], {
          tabBarStyle: {
            margin: '5',
            marginLeft: '10px',
            marginRight: '10px'
          },
          defaultActiveKey: "1",
          onChange: function onChange() {},
          animated: false,
          tabPosition: "top",
          tabBarGutter: 0,
          size: "small"
        }, void 0, _ref, _ref2, _ref3, _ref4), _ref5, _ref6, _jsx(__WEBPACK_IMPORTED_MODULE_5_components__["e" /* UserSelection */], {
          className: __WEBPACK_IMPORTED_MODULE_3__Home_css___default.a.card
        }), _ref7);
      }
    });
    _this.state = {
      count: 0
    };
    return _this;
  }

  return Home;
}(__WEBPACK_IMPORTED_MODULE_0_react__["Component"]);

var Styled = __WEBPACK_IMPORTED_MODULE_2_isomorphic_style_loader_lib_withStyles___default()(__WEBPACK_IMPORTED_MODULE_3__Home_css___default.a)(Home);


/***/ }),
/* 21 */
/***/ (function(module, exports) {

module.exports = require("graphql-tag");

/***/ }),
/* 22 */
/***/ (function(module, exports) {

module.exports = require("lodash");

/***/ }),
/* 23 */
/***/ (function(module, exports) {

module.exports = require("redux");

/***/ }),
/* 24 */
/***/ (function(module, exports) {

module.exports = require("mongodb");

/***/ }),
/* 25 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(26);
module.exports = __webpack_require__(27);


/***/ }),
/* 26 */
/***/ (function(module, exports) {

module.exports = require("@babel/polyfill");

/***/ }),
/* 27 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_path__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_path___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_path__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_bluebird__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_bluebird___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_bluebird__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_express__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_express___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_express__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_cookie_parser__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_cookie_parser___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_cookie_parser__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_body_parser__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_body_parser___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_body_parser__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_graphql__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_graphql___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_graphql__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_express_graphql__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_express_graphql___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_express_graphql__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_node_fetch__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_node_fetch___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_node_fetch__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_react__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_8_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_react_dom_server__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_react_dom_server___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_9_react_dom_server__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_react_apollo__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_react_apollo___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_10_react_apollo__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_pretty_error__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_pretty_error___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_11_pretty_error__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__core_createApolloClient__ = __webpack_require__(37);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__components_App__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__components_Html__ = __webpack_require__(44);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__routes_error_ErrorPage__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__routes_error_ErrorPage_css__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__routes_error_ErrorPage_css___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_16__routes_error_ErrorPage_css__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__createFetch__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__router__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__assets_json__ = __webpack_require__(93);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__assets_json___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_19__assets_json__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__store_configureStore__ = __webpack_require__(94);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__config__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__config___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_21__config__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__data__ = __webpack_require__(100);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23_https__ = __webpack_require__(124);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23_https___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_23_https__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24_fs__ = __webpack_require__(125);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24_fs___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_24_fs__);
function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

var REACT_ELEMENT_TYPE;

function _jsx(type, props, key, children) { if (!REACT_ELEMENT_TYPE) { REACT_ELEMENT_TYPE = typeof Symbol === "function" && Symbol.for && Symbol.for("react.element") || 0xeac7; } var defaultProps = type && type.defaultProps; var childrenLength = arguments.length - 3; if (!props && childrenLength !== 0) { props = {}; } if (props && defaultProps) { for (var propName in defaultProps) { if (props[propName] === void 0) { props[propName] = defaultProps[propName]; } } } else if (!props) { props = defaultProps || {}; } if (childrenLength === 1) { props.children = children; } else if (childrenLength > 1) { var childArray = new Array(childrenLength); for (var i = 0; i < childrenLength; i++) { childArray[i] = arguments[i + 3]; } props.children = childArray; } return { $$typeof: REACT_ELEMENT_TYPE, type: type, key: key === undefined ? null : '' + key, ref: null, props: props, _owner: null }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } } function _next(value) { step("next", value); } function _throw(err) { step("throw", err); } _next(); }); }; }

/* global __DEV__ __dirname process */



















 // eslint-disable-line import/no-unresolved

 // import { setRuntimeVariable } from 'store/actions/runtime'


 // https



var app = __WEBPACK_IMPORTED_MODULE_2_express___default()(); //
// Tell any CSS tooling (such as Material UI) to use all vendor prefixes if the
// user agent is not known.
// -----------------------------------------------------------------------------

global.navigator = global.navigator || {};
global.navigator.userAgent = global.navigator.userAgent || 'all'; //
// Register Node.js middleware
// -----------------------------------------------------------------------------

app.use(__WEBPACK_IMPORTED_MODULE_2_express___default.a.static(__WEBPACK_IMPORTED_MODULE_0_path___default.a.resolve(__dirname, 'public')));
app.use(__WEBPACK_IMPORTED_MODULE_3_cookie_parser___default()());
app.use(__WEBPACK_IMPORTED_MODULE_4_body_parser___default.a.urlencoded({
  extended: true
}));
app.use(__WEBPACK_IMPORTED_MODULE_4_body_parser___default.a.json()); // if (__DEV__ ) {
//   app.enable('trust proxy')
// }
//
// Register API middleware
// -----------------------------------------------------------------------------
// https://github.com/graphql/express-graphql#options

var graphqlMiddleware = __WEBPACK_IMPORTED_MODULE_6_express_graphql___default()(function (req) {
  return {
    schema: __WEBPACK_IMPORTED_MODULE_22__data__["a" /* schema */],
    graphiql: false,
    rootValue: {
      request: req
    },
    pretty: false
  };
});
app.use('/graphql', graphqlMiddleware); // dummy server response to handle success on client side

app.post('/dummy-upload', function (req, res) {
  res.send(JSON.stringify({
    status: 'success'
  }));
}); //
// Register server-side rendering middleware
// -----------------------------------------------------------------------------

app.get('*',
/*#__PURE__*/
function () {
  var _ref = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee(req, res, next) {
    var css, insertCss, apolloClient, fetch, initialState, store, context, route, data, rootComponent, _data$scripts, html;

    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            css = new Set(); // Enables critical path CSS rendering
            // https://github.com/kriasoft/isomorphic-style-loader

            insertCss = function insertCss() {
              for (var _len = arguments.length, styles = new Array(_len), _key = 0; _key < _len; _key++) {
                styles[_key] = arguments[_key];
              }

              // eslint-disable-next-line no-underscore-dangle
              styles.forEach(function (style) {
                return css.add(style._getCss());
              });
            };

            apolloClient = Object(__WEBPACK_IMPORTED_MODULE_12__core_createApolloClient__["a" /* default */])({
              schema: __WEBPACK_IMPORTED_MODULE_22__data__["a" /* schema */],
              rootValue: {
                request: req
              }
            }); // Universal HTTP client

            fetch = Object(__WEBPACK_IMPORTED_MODULE_17__createFetch__["a" /* default */])(__WEBPACK_IMPORTED_MODULE_7_node_fetch___default.a, {
              baseUrl: __WEBPACK_IMPORTED_MODULE_21__config___default.a.api.serverUrl,
              cookie: req.headers.cookie,
              apolloClient: apolloClient,
              schema: __WEBPACK_IMPORTED_MODULE_22__data__["a" /* schema */],
              graphql: __WEBPACK_IMPORTED_MODULE_5_graphql__["graphql"]
            });
            initialState = {// user: req.user || null
            };
            store = Object(__WEBPACK_IMPORTED_MODULE_20__store_configureStore__["a" /* default */])(initialState, {
              cookie: req.headers.cookie,
              fetch: fetch,
              // I should not use `history` on server.. but how I do redirection? follow universal-router
              history: null
            }); // store.dispatch(
            //   setRuntimeVariable({
            //     name: 'initialNow',
            //     value: Date.now()
            //   })
            // )
            // Global (context) variables that can be easily accessed from any React component
            // https://facebook.github.io/react/docs/context.html

            context = {
              insertCss: insertCss,
              fetch: fetch,
              // The twins below are wild, be careful!
              pathname: req.path,
              query: req.query,
              // You can access redux through react-redux connect
              store: store,
              storeSubscription: null,
              // Apollo Client for use with react-apollo
              client: apolloClient
            };
            _context.next = 10;
            return __WEBPACK_IMPORTED_MODULE_18__router__["a" /* default */].resolve(context);

          case 10:
            route = _context.sent;

            if (!route.redirect) {
              _context.next = 14;
              break;
            }

            res.redirect(route.status || 302, route.redirect);
            return _context.abrupt("return");

          case 14:
            data = _extends({}, route);
            rootComponent = _jsx(__WEBPACK_IMPORTED_MODULE_13__components_App__["a" /* App */], {
              context: context
            }, void 0, route.component);
            _context.next = 18;
            return Object(__WEBPACK_IMPORTED_MODULE_10_react_apollo__["getDataFromTree"])(rootComponent);

          case 18:
            _context.next = 20;
            return __WEBPACK_IMPORTED_MODULE_1_bluebird___default.a.delay(0);

          case 20:
            _context.next = 22;
            return __WEBPACK_IMPORTED_MODULE_9_react_dom_server___default.a.renderToString(rootComponent);

          case 22:
            data.children = _context.sent;
            data.styles = [{
              id: 'css',
              cssText: _toConsumableArray(css).join('')
            }];
            data.scripts = [__WEBPACK_IMPORTED_MODULE_19__assets_json___default.a.vendor.js];

            if (route.chunks) {
              (_data$scripts = data.scripts).push.apply(_data$scripts, _toConsumableArray(route.chunks.map(function (chunk) {
                return __WEBPACK_IMPORTED_MODULE_19__assets_json___default.a[chunk].js;
              })));
            }

            data.scripts.push(__WEBPACK_IMPORTED_MODULE_19__assets_json___default.a.client.js); // Furthermore invoked actions will be ignored, client will not receive them!

            data.app = {
              apiUrl: __WEBPACK_IMPORTED_MODULE_21__config___default.a.api.clientUrl,
              state: context.store.getState(),
              apolloState: context.client.extract()
            };
            html = __WEBPACK_IMPORTED_MODULE_9_react_dom_server___default.a.renderToStaticMarkup(__WEBPACK_IMPORTED_MODULE_8_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_14__components_Html__["a" /* Html */], data));
            res.status(route.status || 200);
            res.send("<!doctype html>".concat(html));
            _context.next = 36;
            break;

          case 33:
            _context.prev = 33;
            _context.t0 = _context["catch"](0);
            next(_context.t0);

          case 36:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, this, [[0, 33]]);
  }));

  return function (_x, _x2, _x3) {
    return _ref.apply(this, arguments);
  };
}()); //
// Error handling
// -----------------------------------------------------------------------------

var pe = new __WEBPACK_IMPORTED_MODULE_11_pretty_error___default.a();
pe.skipNodeFiles();
pe.skipPackage('express'); // eslint-disable-next-line no-unused-vars

app.use(function (err, res, next) {
  var html = __WEBPACK_IMPORTED_MODULE_9_react_dom_server___default.a.renderToStaticMarkup(_jsx(__WEBPACK_IMPORTED_MODULE_14__components_Html__["a" /* Html */], {
    title: "Internal Server Error",
    description: err.message,
    styles: [{
      id: 'css',
      cssText: __WEBPACK_IMPORTED_MODULE_16__routes_error_ErrorPage_css___default.a._getCss()
    }]
  }, void 0, __WEBPACK_IMPORTED_MODULE_9_react_dom_server___default.a.renderToString(_jsx(__WEBPACK_IMPORTED_MODULE_15__routes_error_ErrorPage__["a" /* ErrorPageWithoutStyle */], {
    error: err
  }))));
  res.status(err.status || 500);
  res.send("<!doctype html>".concat(html));
}); //
// Launch the server
// -----------------------------------------------------------------------------
// eslint-disable-next-line
// const promise = models.sync().catch((err: { stack: any }) => console.error(err.stack))

if (true) {
  // promise.then(() => {
  // eslint-disable-next-line
  app.listen(__WEBPACK_IMPORTED_MODULE_21__config___default.a.port, function () {
    return console.info("The server is running at http://localhost:".concat(__WEBPACK_IMPORTED_MODULE_21__config___default.a.port, "/"));
  }); // })
} //
// Hot Module Replacement
// -----------------------------------------------------------------------------


if (false) {
  app.hot = module.hot;
  module.hot.accept('./router');
}

if (process.env.DAVINCI_ENV) {
  var options = {
    key: __WEBPACK_IMPORTED_MODULE_24_fs___default.a.readFileSync('./cert.pem'),
    cert: __WEBPACK_IMPORTED_MODULE_24_fs___default.a.readFileSync('./key.pem')
  };
  __WEBPACK_IMPORTED_MODULE_23_https___default.a.createServer(options, app).listen(8080);
}

/* harmony default export */ __webpack_exports__["default"] = (app);

/***/ }),
/* 28 */
/***/ (function(module, exports) {

module.exports = require("path");

/***/ }),
/* 29 */
/***/ (function(module, exports) {

module.exports = require("express");

/***/ }),
/* 30 */
/***/ (function(module, exports) {

module.exports = require("cookie-parser");

/***/ }),
/* 31 */
/***/ (function(module, exports) {

module.exports = require("body-parser");

/***/ }),
/* 32 */
/***/ (function(module, exports) {

module.exports = require("graphql");

/***/ }),
/* 33 */
/***/ (function(module, exports) {

module.exports = require("express-graphql");

/***/ }),
/* 34 */
/***/ (function(module, exports) {

module.exports = require("node-fetch");

/***/ }),
/* 35 */
/***/ (function(module, exports) {

module.exports = require("react-dom/server");

/***/ }),
/* 36 */
/***/ (function(module, exports) {

module.exports = require("pretty-error");

/***/ }),
/* 37 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (immutable) */ __webpack_exports__["a"] = createApolloClient;
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_apollo_client__ = __webpack_require__(38);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_apollo_client___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_apollo_client__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_apollo_link__ = __webpack_require__(39);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_apollo_link___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_apollo_link__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_apollo_link_error__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_apollo_link_error___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_apollo_link_error__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_apollo_link_schema__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_apollo_link_schema___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_apollo_link_schema__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__createCache__ = __webpack_require__(42);
function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }






function createApolloClient(schema) {
  var link = Object(__WEBPACK_IMPORTED_MODULE_1_apollo_link__["from"])([Object(__WEBPACK_IMPORTED_MODULE_2_apollo_link_error__["onError"])(function (_ref) {
    var graphQLErrors = _ref.graphQLErrors,
        networkError = _ref.networkError;
    if (graphQLErrors) graphQLErrors.map(function (_ref2) {
      var message = _ref2.message,
          locations = _ref2.locations,
          path = _ref2.path;
      return console.warn("[GraphQL error]: Message: ".concat(message, ", Location: ").concat(locations, ", Path: ").concat(path));
    });
    if (networkError) console.warn("[Network error]: ".concat(networkError));
  }), new __WEBPACK_IMPORTED_MODULE_3_apollo_link_schema__["SchemaLink"](_extends({}, schema))]);
  return new __WEBPACK_IMPORTED_MODULE_0_apollo_client__["ApolloClient"]({
    link: link,
    cache: Object(__WEBPACK_IMPORTED_MODULE_4__createCache__["a" /* default */])(),
    ssrMode: true,
    queryDeduplication: true
  });
}

/***/ }),
/* 38 */
/***/ (function(module, exports) {

module.exports = require("apollo-client");

/***/ }),
/* 39 */
/***/ (function(module, exports) {

module.exports = require("apollo-link");

/***/ }),
/* 40 */
/***/ (function(module, exports) {

module.exports = require("apollo-link-error");

/***/ }),
/* 41 */
/***/ (function(module, exports) {

module.exports = require("apollo-link-schema");

/***/ }),
/* 42 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (immutable) */ __webpack_exports__["a"] = createCache;
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_apollo_cache_inmemory__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_apollo_cache_inmemory___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_apollo_cache_inmemory__);


function dataIdFromObject(obj) {
  switch (obj.__typename) {
    case 'NewsItem':
      return obj.link ? "NewsItem:".concat(obj.link) : Object(__WEBPACK_IMPORTED_MODULE_0_apollo_cache_inmemory__["defaultDataIdFromObject"])(obj);

    default:
      return Object(__WEBPACK_IMPORTED_MODULE_0_apollo_cache_inmemory__["defaultDataIdFromObject"])(obj);
  }
}

function createCache() {
  // https://www.apollographql.com/docs/react/basics/caching.html#configuration
  return new __WEBPACK_IMPORTED_MODULE_0_apollo_cache_inmemory__["InMemoryCache"]({
    dataIdFromObject: dataIdFromObject
  });
}

/***/ }),
/* 43 */
/***/ (function(module, exports) {

module.exports = require("apollo-cache-inmemory");

/***/ }),
/* 44 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Html; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_prop_types__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_serialize_javascript__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_serialize_javascript___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_serialize_javascript__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__config__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__config___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3__config__);
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

var REACT_ELEMENT_TYPE;

function _jsx(type, props, key, children) { if (!REACT_ELEMENT_TYPE) { REACT_ELEMENT_TYPE = typeof Symbol === "function" && Symbol.for && Symbol.for("react.element") || 0xeac7; } var defaultProps = type && type.defaultProps; var childrenLength = arguments.length - 3; if (!props && childrenLength !== 0) { props = {}; } if (props && defaultProps) { for (var propName in defaultProps) { if (props[propName] === void 0) { props[propName] = defaultProps[propName]; } } } else if (!props) { props = defaultProps || {}; } if (childrenLength === 1) { props.children = children; } else if (childrenLength > 1) { var childArray = new Array(childrenLength); for (var i = 0; i < childrenLength; i++) { childArray[i] = arguments[i + 3]; } props.children = childArray; } return { $$typeof: REACT_ELEMENT_TYPE, type: type, key: key === undefined ? null : '' + key, ref: null, props: props, _owner: null }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

/* eslint-disable react/no-danger */





var _ref2 = _jsx("meta", {
  charSet: "utf-8"
});

var _ref3 = _jsx("meta", {
  httpEquiv: "x-ua-compatible",
  content: "ie=edge"
});

var _ref4 = _jsx("meta", {
  name: "app",
  content: ""
});

var _ref5 = _jsx("meta", {
  name: "viewport",
  content: "width=device-width, initial-scale=1"
});

var _ref6 = _jsx("link", {
  rel: "manifest",
  href: "/site.webmanifest"
});

var _ref7 = _jsx("link", {
  rel: "apple-touch-icon",
  href: "/icon.png"
});

var _ref8 = _jsx("link", {
  rel: "stylesheet",
  href: "https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.3.0/semantic.min.css",
  integrity: "sha256-/mC8AIsSmTcTtaf8vgnfbZXZLYhJCd0b9If/M0Y5nDw=",
  crossOrigin: "anonymous"
});

var _ref9 = _jsx("link", {
  rel: "stylesheet",
  href: "https://cdnjs.cloudflare.com/ajax/libs/antd/3.2.3/antd.min.css",
  integrity: "sha256-lkm3kluM/5UZ+PEnNJURptGRRICYecI5IfPi/1ThFVE=",
  crossOrigin: "anonymous"
});

var _ref10 = _jsx("script", {
  src: "https://www.google-analytics.com/analytics.js",
  async: true,
  defer: true
});

// $FlowFixMe
var Html =
/*#__PURE__*/
function (_Component) {
  _inherits(Html, _Component);

  function Html() {
    var _ref;

    var _temp, _this;

    _classCallCheck(this, Html);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _possibleConstructorReturn(_this, (_temp = _this = _possibleConstructorReturn(this, (_ref = Html.__proto__ || Object.getPrototypeOf(Html)).call.apply(_ref, [this].concat(args))), Object.defineProperty(_assertThisInitialized(_this), "render", {
      configurable: true,
      enumerable: true,
      writable: true,
      value: function value() {
        var _this$props = _this.props,
            title = _this$props.title,
            description = _this$props.description,
            styles = _this$props.styles,
            scripts = _this$props.scripts,
            app = _this$props.app,
            children = _this$props.children;
        return _jsx("html", {
          className: "no-js",
          lang: "en"
        }, void 0, _jsx("head", {}, void 0, _ref2, _ref3, _jsx("title", {}, void 0, title), _ref4, _jsx("meta", {
          name: "description",
          content: description
        }), _ref5, scripts.map(function (script) {
          return _jsx("link", {
            rel: "preload",
            href: script,
            as: "script"
          }, script);
        }), _ref6, _ref7, _ref8, _ref9, styles.map(function (style) {
          return _jsx("style", {
            id: style.id,
            dangerouslySetInnerHTML: {
              __html: style.cssText
            }
          }, style.id);
        })), _jsx("body", {}, void 0, _jsx("div", {
          id: "app",
          dangerouslySetInnerHTML: {
            __html: children
          }
        }), _jsx("script", {
          dangerouslySetInnerHTML: {
            __html: "window.App=".concat(__WEBPACK_IMPORTED_MODULE_2_serialize_javascript___default()(app))
          }
        }), scripts.map(function (script) {
          return _jsx("script", {
            src: script
          }, script);
        }), __WEBPACK_IMPORTED_MODULE_3__config___default.a.analytics.googleTrackingId && _jsx("script", {
          dangerouslySetInnerHTML: {
            __html: 'window.ga=function(){ga.q.push(arguments)};ga.q=[];ga.l=+new Date;' + "ga('create','".concat(__WEBPACK_IMPORTED_MODULE_3__config___default.a.analytics.googleTrackingId, "','auto');ga('send','pageview')")
          }
        }), __WEBPACK_IMPORTED_MODULE_3__config___default.a.analytics.googleTrackingId && _ref10));
      }
    }), _temp));
  }

  return Html;
}(__WEBPACK_IMPORTED_MODULE_0_react__["Component"]);

Object.defineProperty(Html, "defaultProps", {
  configurable: true,
  enumerable: true,
  writable: true,
  value: {
    styles: [],
    scripts: []
  }
});


/***/ }),
/* 45 */
/***/ (function(module, exports) {

module.exports = require("serialize-javascript");

/***/ }),
/* 46 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ErrorPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_prop_types__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_isomorphic_style_loader_lib_withStyles__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_isomorphic_style_loader_lib_withStyles___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_isomorphic_style_loader_lib_withStyles__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ErrorPage_css__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ErrorPage_css___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3__ErrorPage_css__);
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

var REACT_ELEMENT_TYPE;

function _jsx(type, props, key, children) { if (!REACT_ELEMENT_TYPE) { REACT_ELEMENT_TYPE = typeof Symbol === "function" && Symbol.for && Symbol.for("react.element") || 0xeac7; } var defaultProps = type && type.defaultProps; var childrenLength = arguments.length - 3; if (!props && childrenLength !== 0) { props = {}; } if (props && defaultProps) { for (var propName in defaultProps) { if (props[propName] === void 0) { props[propName] = defaultProps[propName]; } } } else if (!props) { props = defaultProps || {}; } if (childrenLength === 1) { props.children = children; } else if (childrenLength > 1) { var childArray = new Array(childrenLength); for (var i = 0; i < childrenLength; i++) { childArray[i] = arguments[i + 3]; } props.children = childArray; } return { $$typeof: REACT_ELEMENT_TYPE, type: type, key: key === undefined ? null : '' + key, ref: null, props: props, _owner: null }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }






var _ref = _jsx("div", {}, void 0, _jsx("h1", {}, void 0, "Error"), _jsx("p", {}, void 0, "Sorry, a critical error occurred on this page."));

var ErrorPage =
/*#__PURE__*/
function (_React$Component) {
  _inherits(ErrorPage, _React$Component);

  function ErrorPage() {
    _classCallCheck(this, ErrorPage);

    return _possibleConstructorReturn(this, (ErrorPage.__proto__ || Object.getPrototypeOf(ErrorPage)).apply(this, arguments));
  }

  _createClass(ErrorPage, [{
    key: "render",
    value: function render() {
      if (false) {
        return _jsx("div", {}, void 0, _jsx("h1", {}, void 0, this.props.error.name), _jsx("pre", {}, void 0, this.props.error.stack));
      }

      return _ref;
    }
  }]);

  return ErrorPage;
}(__WEBPACK_IMPORTED_MODULE_0_react___default.a.Component);

Object.defineProperty(ErrorPage, "defaultProps", {
  configurable: true,
  enumerable: true,
  writable: true,
  value: {
    error: null
  }
});

/* unused harmony default export */ var _unused_webpack_default_export = (__WEBPACK_IMPORTED_MODULE_2_isomorphic_style_loader_lib_withStyles___default()(__WEBPACK_IMPORTED_MODULE_3__ErrorPage_css___default.a)(ErrorPage));

/***/ }),
/* 47 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(9)(false);
// imports


// module
exports.push([module.i, "html{display:-ms-flexbox;display:flex;-ms-flex-align:center;align-items:center;-ms-flex-pack:center;justify-content:center;padding:0 32px;padding:0 2rem;height:100%;font-family:sans-serif;text-align:center;color:#888}body{margin:0}h1{font-weight:400;color:#555}pre{white-space:pre-wrap;text-align:left}", ""]);

// exports


/***/ }),
/* 48 */
/***/ (function(module, exports) {

module.exports = require("babel-runtime/core-js/json/stringify");

/***/ }),
/* 49 */
/***/ (function(module, exports) {

module.exports = require("babel-runtime/helpers/slicedToArray");

/***/ }),
/* 50 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } } function _next(value) { step("next", value); } function _throw(err) { step("throw", err); } _next(); }); }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

/**
 * Creates a wrapper function around the HTML5 Fetch API that provides
 * default arguments to fetch(...) and is intended to reduce the amount
 * of boilerplate code in the application.
 * https://developer.mozilla.org/docs/Web/API/Fetch_API/Using_Fetch
 */
var createFetch = function createFetch(fetch, _ref) {
  var baseUrl = _ref.baseUrl,
      cookie = _ref.cookie,
      schema = _ref.schema,
      graphql = _ref.graphql;
  // NOTE: Tweak the default options to suite your application needs
  var defaults = {
    method: 'POST',
    // handy with GraphQL backends
    mode: baseUrl ? 'cors' : 'same-origin',
    credentials: baseUrl ? 'include' : 'same-origin',
    headers: _extends({
      Accept: 'application/json',
      'Content-Type': 'application/json'
    }, cookie ? {
      Cookie: cookie
    } : null)
  };
  return (
    /*#__PURE__*/
    function () {
      var _ref2 = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee(url, options) {
        var isGraphQL, query, result;
        return regeneratorRuntime.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                isGraphQL = url.startsWith('/graphql');

                if (!(schema && graphql && isGraphQL)) {
                  _context.next = 7;
                  break;
                }

                // We're SSR, so route the graphql internal to avoid latency
                query = JSON.parse(options.body);
                _context.next = 5;
                return graphql(schema, query.query, {
                  request: {}
                }, // fill in request vars needed by graphql
                null, query.variables);

              case 5:
                result = _context.sent;
                return _context.abrupt("return", Promise.resolve({
                  status: result.errors ? 400 : 200,
                  json: function json() {
                    return Promise.resolve(result);
                  }
                }));

              case 7:
                return _context.abrupt("return", isGraphQL || url.startsWith('/api') ? fetch("".concat(baseUrl).concat(url), _extends({}, defaults, options, {
                  headers: _extends({}, defaults.headers, options && options.headers)
                })) : fetch(url, options));

              case 8:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      return function (_x, _x2) {
        return _ref2.apply(this, arguments);
      };
    }()
  );
};

/* harmony default export */ __webpack_exports__["a"] = (createFetch);

/***/ }),
/* 51 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_universal_router__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_universal_router___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_universal_router__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__routes__ = __webpack_require__(53);


/* harmony default export */ __webpack_exports__["a"] = (new __WEBPACK_IMPORTED_MODULE_0_universal_router___default.a(__WEBPACK_IMPORTED_MODULE_1__routes__["a" /* default */], {
  resolveRoute: function resolveRoute(context, params) {
    if (typeof context.route.load === 'function') {
      return context.route.load().then(function (action) {
        return action.default(context, params);
      });
    }

    if (typeof context.route.action === 'function') {
      return context.route.action(context, params);
    }

    return undefined;
  }
}));

/***/ }),
/* 52 */
/***/ (function(module, exports) {

module.exports = require("universal-router");

/***/ }),
/* 53 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } } function _next(value) { step("next", value); } function _throw(err) { step("throw", err); } _next(); }); }; }

/* eslint-disable global-require */
// The top-level (parent) route
var routes = {
  path: '',
  // Keep in mind, routes are evaluated in order
  children: [// The home route is added to client.js to make sure shared components are
  // added to client.js as well and not repeated in individual each route chunk.
  {
    path: '',
    load: function load() {
      return new Promise(function(resolve) { resolve(__webpack_require__(54)); });
    }
  }, // Wildcard routes, e.g. { path: '(.*)', ... } (must go last)
  {
    path: '(.*)',
    load: function load() {
      return __webpack_require__.e/* import() */(0).then(__webpack_require__.bind(null, 126));
    }
  }],
  action: function () {
    var _action = _asyncToGenerator(
    /*#__PURE__*/
    regeneratorRuntime.mark(function _callee(_ref) {
      var next, route;
      return regeneratorRuntime.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              next = _ref.next;
              _context.next = 3;
              return next();

            case 3:
              route = _context.sent;
              // Provide default values for title, description etc.
              route.title = "".concat(route.title || 'Untitled Page');
              route.description = route.description || '';
              return _context.abrupt("return", route);

            case 7:
            case "end":
              return _context.stop();
          }
        }
      }, _callee, this);
    }));

    return function action(_x) {
      return _action.apply(this, arguments);
    };
  }()
}; // The error page is available by permanent url for development mode

if (false) {
  routes.children.unshift({
    path: '/error',
    action: require('./error').default
  });
}

/* harmony default export */ __webpack_exports__["a"] = (routes);

/***/ }),
/* 54 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_components_Layout__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_components_Home__ = __webpack_require__(20);
var REACT_ELEMENT_TYPE;

function _jsx(type, props, key, children) { if (!REACT_ELEMENT_TYPE) { REACT_ELEMENT_TYPE = typeof Symbol === "function" && Symbol.for && Symbol.for("react.element") || 0xeac7; } var defaultProps = type && type.defaultProps; var childrenLength = arguments.length - 3; if (!props && childrenLength !== 0) { props = {}; } if (props && defaultProps) { for (var propName in defaultProps) { if (props[propName] === void 0) { props[propName] = defaultProps[propName]; } } } else if (!props) { props = defaultProps || {}; } if (childrenLength === 1) { props.children = children; } else if (childrenLength > 1) { var childArray = new Array(childrenLength); for (var i = 0; i < childrenLength; i++) { childArray[i] = arguments[i + 3]; } props.children = childArray; } return { $$typeof: REACT_ELEMENT_TYPE, type: type, key: key === undefined ? null : '' + key, ref: null, props: props, _owner: null }; }


 // import Home from './Home'
// import newsQuery from './news.graphql'



var _ref = _jsx(__WEBPACK_IMPORTED_MODULE_1_components_Layout__["a" /* Layout */], {}, void 0, _jsx(__WEBPACK_IMPORTED_MODULE_2_components_Home__["a" /* Home */], {}));

function action() {
  // const data = await client.query({
  //   query: newsQuery,
  // })
  return {
    title: 'Metadata Manager',
    component: _ref
  };
}

/* harmony default export */ __webpack_exports__["default"] = (action);

/***/ }),
/* 55 */
/***/ (function(module, exports, __webpack_require__) {


    var content = __webpack_require__(56);
    var insertCss = __webpack_require__(10);

    if (typeof content === 'string') {
      content = [[module.i, content, '']];
    }

    module.exports = content.locals || {};
    module.exports._getContent = function() { return content; };
    module.exports._getCss = function() { return content.toString(); };
    module.exports._insertCss = function(options) { return insertCss(content, options) };
    
    // Hot Module Replacement
    // https://webpack.github.io/docs/hot-module-replacement
    // Only activated in browser context
    if (false) {
      var removeCss = function() {};
      module.hot.accept("!!../../../node_modules/css-loader/index.js??ref--2-rules-2!../../../node_modules/postcss-loader/lib/index.js??ref--2-rules-3!./Layout.css", function() {
        content = require("!!../../../node_modules/css-loader/index.js??ref--2-rules-2!../../../node_modules/postcss-loader/lib/index.js??ref--2-rules-3!./Layout.css");

        if (typeof content === 'string') {
          content = [[module.id, content, '']];
        }

        removeCss = insertCss(content, { replace: true });
      });
      module.hot.dispose(function() { removeCss(); });
    }
  

/***/ }),
/* 56 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(9)(false);
// imports


// module
exports.push([module.i, "html{color:#222;font-weight:100;font-size:1em;font-family:Segoe UI,HelveticaNeue-Light,sans-serif;line-height:1.375}body{margin:0}a{color:#0074c2}::-moz-selection{background:#b3d4fc;text-shadow:none}::selection{background:#b3d4fc;text-shadow:none}hr{display:block;height:1px;border:0;border-top:1px solid #ccc;margin:1em 0;padding:0}audio,canvas,iframe,img,svg,video{vertical-align:middle}fieldset{border:0;margin:0;padding:0}textarea{resize:vertical}.browserupgrade{margin:.2em 0;background:#ccc;color:#000;padding:.2em 0}@media print{*,:after,:before{background:transparent!important;color:#000!important;-webkit-box-shadow:none!important;box-shadow:none!important;text-shadow:none!important}a,a:visited{text-decoration:underline}a[href]:after{content:\" (\" attr(href) \")\"}abbr[title]:after{content:\" (\" attr(title) \")\"}a[href^=\"#\"]:after,a[href^=\"javascript:\"]:after{content:\"\"}blockquote,pre{border:1px solid #999;page-break-inside:avoid}thead{display:table-header-group}img,tr{page-break-inside:avoid}img{max-width:100%!important}h2,h3,p{orphans:3;widows:3}h2,h3{page-break-after:avoid}}", ""]);

// exports


/***/ }),
/* 57 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_history_createBrowserHistory__ = __webpack_require__(58);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_history_createBrowserHistory___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_history_createBrowserHistory__);
/* global process */
 // Navigation manager, e.g. history.push('/home')
// https://github.com/mjackson/history

/* harmony default export */ __webpack_exports__["a"] = (false && __WEBPACK_IMPORTED_MODULE_0_history_createBrowserHistory___default()());

/***/ }),
/* 58 */
/***/ (function(module, exports) {

module.exports = require("history/createBrowserHistory");

/***/ }),
/* 59 */
/***/ (function(module, exports, __webpack_require__) {


    var content = __webpack_require__(60);
    var insertCss = __webpack_require__(10);

    if (typeof content === 'string') {
      content = [[module.i, content, '']];
    }

    module.exports = content.locals || {};
    module.exports._getContent = function() { return content; };
    module.exports._getCss = function() { return content.toString(); };
    module.exports._insertCss = function(options) { return insertCss(content, options) };
    
    // Hot Module Replacement
    // https://webpack.github.io/docs/hot-module-replacement
    // Only activated in browser context
    if (false) {
      var removeCss = function() {};
      module.hot.accept("!!../../../node_modules/css-loader/index.js??ref--2-rules-2!../../../node_modules/postcss-loader/lib/index.js??ref--2-rules-3!./Navigation.css", function() {
        content = require("!!../../../node_modules/css-loader/index.js??ref--2-rules-2!../../../node_modules/postcss-loader/lib/index.js??ref--2-rules-3!./Navigation.css");

        if (typeof content === 'string') {
          content = [[module.id, content, '']];
        }

        removeCss = insertCss(content, { replace: true });
      });
      module.hot.dispose(function() { removeCss(); });
    }
  

/***/ }),
/* 60 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(9)(false);
// imports


// module
exports.push([module.i, ".Ntl35{display:inline-block;padding:2px 8px;text-decoration:none;font-size:1em;margin-top:.8em;color:#000;-webkit-transition:color 1s ease;-o-transition:color 1s ease;transition:color 1s ease}.Ntl35:hover:before{height:2px;margin-top:-2px}.Ntl35:hover:after{height:2px;display:block;width:auto;background:#1890ff;content:\"\"}", ""]);

// exports
exports.locals = {
	"link": "Ntl35"
};

/***/ }),
/* 61 */
/***/ (function(module, exports, __webpack_require__) {


    var content = __webpack_require__(62);
    var insertCss = __webpack_require__(10);

    if (typeof content === 'string') {
      content = [[module.i, content, '']];
    }

    module.exports = content.locals || {};
    module.exports._getContent = function() { return content; };
    module.exports._getCss = function() { return content.toString(); };
    module.exports._insertCss = function(options) { return insertCss(content, options) };
    
    // Hot Module Replacement
    // https://webpack.github.io/docs/hot-module-replacement
    // Only activated in browser context
    if (false) {
      var removeCss = function() {};
      module.hot.accept("!!../../../node_modules/css-loader/index.js??ref--2-rules-2!../../../node_modules/postcss-loader/lib/index.js??ref--2-rules-3!./Header.css", function() {
        content = require("!!../../../node_modules/css-loader/index.js??ref--2-rules-2!../../../node_modules/postcss-loader/lib/index.js??ref--2-rules-3!./Header.css");

        if (typeof content === 'string') {
          content = [[module.id, content, '']];
        }

        removeCss = insertCss(content, { replace: true });
      });
      module.hot.dispose(function() { removeCss(); });
    }
  

/***/ }),
/* 62 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(9)(false);
// imports


// module
exports.push([module.i, ".O9oW9{background:#fff;color:#fff}.qQ2mF{margin:0 auto;padding:0;max-width:95%}._2oS_y{text-decoration:none;font-size:1.75em}", ""]);

// exports
exports.locals = {
	"root": "O9oW9",
	"container": "qQ2mF",
	"brand": "_2oS_y"
};

/***/ }),
/* 63 */
/***/ (function(module, exports, __webpack_require__) {


    var content = __webpack_require__(64);
    var insertCss = __webpack_require__(10);

    if (typeof content === 'string') {
      content = [[module.i, content, '']];
    }

    module.exports = content.locals || {};
    module.exports._getContent = function() { return content; };
    module.exports._getCss = function() { return content.toString(); };
    module.exports._insertCss = function(options) { return insertCss(content, options) };
    
    // Hot Module Replacement
    // https://webpack.github.io/docs/hot-module-replacement
    // Only activated in browser context
    if (false) {
      var removeCss = function() {};
      module.hot.accept("!!../../../node_modules/css-loader/index.js??ref--2-rules-2!../../../node_modules/postcss-loader/lib/index.js??ref--2-rules-3!./Home.css", function() {
        content = require("!!../../../node_modules/css-loader/index.js??ref--2-rules-2!../../../node_modules/postcss-loader/lib/index.js??ref--2-rules-3!./Home.css");

        if (typeof content === 'string') {
          content = [[module.id, content, '']];
        }

        removeCss = insertCss(content, { replace: true });
      });
      module.hot.dispose(function() { removeCss(); });
    }
  

/***/ }),
/* 64 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(9)(false);
// imports


// module
exports.push([module.i, ".dJEqk{padding-left:10px;padding-right:10px}._3Y3Ar{font-size:12px}", ""]);

// exports
exports.locals = {
	"root": "dJEqk",
	"tab": "_3Y3Ar"
};

/***/ }),
/* 65 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__App__ = __webpack_require__(15);
/* unused harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__Header__ = __webpack_require__(18);
/* unused harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__Home__ = __webpack_require__(20);
/* unused harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__Layout__ = __webpack_require__(13);
/* unused harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__Link__ = __webpack_require__(12);
/* unused harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__Navigation__ = __webpack_require__(19);
/* unused harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__SearchEntries__ = __webpack_require__(66);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "b", function() { return __WEBPACK_IMPORTED_MODULE_6__SearchEntries__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__NewEntry__ = __webpack_require__(71);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_7__NewEntry__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__UploadForm__ = __webpack_require__(78);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "d", function() { return __WEBPACK_IMPORTED_MODULE_8__UploadForm__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__Settings__ = __webpack_require__(79);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "c", function() { return __WEBPACK_IMPORTED_MODULE_9__Settings__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__UserSelection__ = __webpack_require__(85);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "e", function() { return __WEBPACK_IMPORTED_MODULE_10__UserSelection__["a"]; });
// Core Components





 // Metadata Manager Components







/***/ }),
/* 66 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Styled; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_prop_types__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_antd__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_antd___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_antd__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_semantic_ui_react__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_semantic_ui_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_semantic_ui_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_react_apollo__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_react_apollo___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_react_apollo__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__graphql__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__TableSearchEntries__ = __webpack_require__(69);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_isomorphic_style_loader_lib_withStyles__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_isomorphic_style_loader_lib_withStyles___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_isomorphic_style_loader_lib_withStyles__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_theme_style_scss__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_theme_style_scss___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_8_theme_style_scss__);
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

var REACT_ELEMENT_TYPE;

function _jsx(type, props, key, children) { if (!REACT_ELEMENT_TYPE) { REACT_ELEMENT_TYPE = typeof Symbol === "function" && Symbol.for && Symbol.for("react.element") || 0xeac7; } var defaultProps = type && type.defaultProps; var childrenLength = arguments.length - 3; if (!props && childrenLength !== 0) { props = {}; } if (props && defaultProps) { for (var propName in defaultProps) { if (props[propName] === void 0) { props[propName] = defaultProps[propName]; } } } else if (!props) { props = defaultProps || {}; } if (childrenLength === 1) { props.children = children; } else if (childrenLength > 1) { var childArray = new Array(childrenLength); for (var i = 0; i < childrenLength; i++) { childArray[i] = arguments[i + 3]; } props.children = childArray; } return { $$typeof: REACT_ELEMENT_TYPE, type: type, key: key === undefined ? null : '' + key, ref: null, props: props, _owner: null }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }




var Search = __WEBPACK_IMPORTED_MODULE_2_antd__["Input"].Search;

 // Data Layer







var _ref = _jsx(__WEBPACK_IMPORTED_MODULE_2_antd__["Breadcrumb"].Item, {}, void 0, "Search Metadata Entries");

var _ref2 = _jsx("span", {}, void 0, "Protocol Name:");

var _ref3 = _jsx(__WEBPACK_IMPORTED_MODULE_3_semantic_ui_react__["Divider"], {});

var SearchEntries =
/*#__PURE__*/
function (_Component) {
  _inherits(SearchEntries, _Component);

  function SearchEntries(props) {
    var _this;

    _classCallCheck(this, SearchEntries);

    _this = _possibleConstructorReturn(this, (SearchEntries.__proto__ || Object.getPrototypeOf(SearchEntries)).call(this, props));
    Object.defineProperty(_assertThisInitialized(_this), "commitSearch", {
      configurable: true,
      enumerable: true,
      writable: true,
      value: function value(protocol) {
        var refetch = _this.props.data.refetch;
        refetch({
          protocol: protocol
        }).then(function (response) {
          var entries = response.data['getAllEntries'];

          _this.setState({
            entries: entries,
            protocol: protocol
          }); //console.log(response)

        });
      }
    });
    Object.defineProperty(_assertThisInitialized(_this), "render", {
      configurable: true,
      enumerable: true,
      writable: true,
      value: function value() {
        var _this$state = _this.state,
            entries = _this$state.entries,
            protocol = _this$state.protocol;
        return _jsx(__WEBPACK_IMPORTED_MODULE_0_react__["Fragment"], {}, void 0, _jsx(__WEBPACK_IMPORTED_MODULE_2_antd__["Layout"], {
          className: __WEBPACK_IMPORTED_MODULE_8_theme_style_scss___default.a.card,
          style: {
            padding: '0 24px 24px'
          }
        }, void 0, _jsx(__WEBPACK_IMPORTED_MODULE_2_antd__["Breadcrumb"], {
          style: {
            margin: '16px 0'
          }
        }, void 0, _ref), _ref2, _jsx(Search, {
          placeholder: "Search Metadata Entries",
          onInput: function onInput(e) {
            if (e.target.value.length >= 3) {
              _this.commitSearch(e.target.value);
            }
          },
          onSearch: function onSearch(value) {
            return _this.commitSearch(value);
          },
          style: {
            width: 400
          }
        }), _ref3, protocol.length > 0 ? entries.length > 0 ? _jsx(__WEBPACK_IMPORTED_MODULE_6__TableSearchEntries__["a" /* TableSearchEntries */], {
          entries: entries
        }) : "No Result Found for [".concat(protocol, "]") : null));
      }
    });
    _this.state = {
      entries: [],
      protocol: ''
    };
    return _this;
  }

  return SearchEntries;
}(__WEBPACK_IMPORTED_MODULE_0_react__["Component"]);

var withApollo = Object(__WEBPACK_IMPORTED_MODULE_4_react_apollo__["compose"])(Object(__WEBPACK_IMPORTED_MODULE_4_react_apollo__["graphql"])(__WEBPACK_IMPORTED_MODULE_5__graphql__["b" /* queries */].search_entries, {
  options: {
    variables: {
      protocol: ''
    }
  }
}))(SearchEntries);
var Styled = __WEBPACK_IMPORTED_MODULE_7_isomorphic_style_loader_lib_withStyles___default()(__WEBPACK_IMPORTED_MODULE_8_theme_style_scss___default.a)(withApollo);


/***/ }),
/* 67 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "search_entries", function() { return search_entries; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "yp_protocols", function() { return yp_protocols; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "yp_sourcedata", function() { return yp_sourcedata; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "domains_query", function() { return domains_query; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "standards_query", function() { return standards_query; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "targets_query", function() { return targets_query; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_graphql_tag__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_graphql_tag___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_graphql_tag__);
var _templateObject = /*#__PURE__*/ _taggedTemplateLiteral(["\n  query getAllEntries($protocol: String) {\n    getAllEntries(protocol: $protocol) {\n      protocol\n      domain\n      standard\n      sourcedata\n      preprocessing\n    }\n  }\n"], ["\n  query getAllEntries($protocol: String) {\n    getAllEntries(protocol: $protocol) {\n      protocol\n      domain\n      standard\n      sourcedata\n      preprocessing\n    }\n  }\n"]),
    _templateObject2 = /*#__PURE__*/ _taggedTemplateLiteral(["\n  query getYPProtocols($pattern: String) {\n    getYPProtocols(pattern: $pattern)\n  }\n"], ["\n  query getYPProtocols($pattern: String) {\n    getYPProtocols(pattern: $pattern)\n  }\n"]),
    _templateObject3 = /*#__PURE__*/ _taggedTemplateLiteral(["\n  query getYPSourcedata($pattern: String) {\n    getYPSourcedata(pattern: $pattern)\n  }\n"], ["\n  query getYPSourcedata($pattern: String) {\n    getYPSourcedata(pattern: $pattern)\n  }\n"]),
    _templateObject4 = /*#__PURE__*/ _taggedTemplateLiteral(["\n  query domain_query {\n    getAllDomains {\n      name\n      code\n    }\n  }\n"], ["\n  query domain_query {\n    getAllDomains {\n      name\n      code\n    }\n  }\n"]),
    _templateObject5 = /*#__PURE__*/ _taggedTemplateLiteral(["\n  query standard_query {\n    getAllStandards {\n      name\n      code\n    }\n  }\n"], ["\n  query standard_query {\n    getAllStandards {\n      name\n      code\n    }\n  }\n"]),
    _templateObject6 = /*#__PURE__*/ _taggedTemplateLiteral(["\n  query standard_query {\n    getAllTargets\n  }\n"], ["\n  query standard_query {\n    getAllTargets\n  }\n"]);

function _taggedTemplateLiteral(strings, raw) { return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

 // Search Entries

var search_entries = __WEBPACK_IMPORTED_MODULE_0_graphql_tag___default()(_templateObject); // New Entry

var yp_protocols = __WEBPACK_IMPORTED_MODULE_0_graphql_tag___default()(_templateObject2);
var yp_sourcedata = __WEBPACK_IMPORTED_MODULE_0_graphql_tag___default()(_templateObject3); // Settings

var domains_query = __WEBPACK_IMPORTED_MODULE_0_graphql_tag___default()(_templateObject4);
var standards_query = __WEBPACK_IMPORTED_MODULE_0_graphql_tag___default()(_templateObject5);
var targets_query = __WEBPACK_IMPORTED_MODULE_0_graphql_tag___default()(_templateObject6);


/***/ }),
/* 68 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "new_entry_mm", function() { return new_entry_mm; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_graphql_tag__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_graphql_tag___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_graphql_tag__);
var _templateObject = /*#__PURE__*/ _taggedTemplateLiteral(["\n  mutation createNewEntry($meta: EntryMM_Meta!) {\n    createNewEntry(meta: $meta)\n  }\n"], ["\n  mutation createNewEntry($meta: EntryMM_Meta!) {\n    createNewEntry(meta: $meta)\n  }\n"]);

function _taggedTemplateLiteral(strings, raw) { return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }


var new_entry_mm = __WEBPACK_IMPORTED_MODULE_0_graphql_tag___default()(_templateObject);


/***/ }),
/* 69 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return withRedux; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_prop_types__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_antd__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_antd___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_antd__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_react_redux__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_react_redux___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_react_redux__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_store_constants__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_lodash__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_lodash__);
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

var REACT_ELEMENT_TYPE;

function _jsx(type, props, key, children) { if (!REACT_ELEMENT_TYPE) { REACT_ELEMENT_TYPE = typeof Symbol === "function" && Symbol.for && Symbol.for("react.element") || 0xeac7; } var defaultProps = type && type.defaultProps; var childrenLength = arguments.length - 3; if (!props && childrenLength !== 0) { props = {}; } if (props && defaultProps) { for (var propName in defaultProps) { if (props[propName] === void 0) { props[propName] = defaultProps[propName]; } } } else if (!props) { props = defaultProps || {}; } if (childrenLength === 1) { props.children = children; } else if (childrenLength > 1) { var childArray = new Array(childrenLength); for (var i = 0; i < childrenLength; i++) { childArray[i] = arguments[i + 3]; } props.children = childArray; } return { $$typeof: REACT_ELEMENT_TYPE, type: type, key: key === undefined ? null : '' + key, ref: null, props: props, _owner: null }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }



 // redux





var TableSearchEntries =
/*#__PURE__*/
function (_Component) {
  _inherits(TableSearchEntries, _Component);

  function TableSearchEntries(props) {
    var _this;

    _classCallCheck(this, TableSearchEntries);

    _this = _possibleConstructorReturn(this, (TableSearchEntries.__proto__ || Object.getPrototypeOf(TableSearchEntries)).call(this, props));
    Object.defineProperty(_assertThisInitialized(_this), "processRaw", {
      configurable: true,
      enumerable: true,
      writable: true,
      value: function value(record) {
        var addEntryToSelection = _this.props.addEntryToSelection;
        var domain = record.domain,
            standard = record.standard,
            preprocessing = record.preprocessing,
            sourcedata = record.sourcedata,
            protocol = record.protocol;
        var entry = {
          domain: domain,
          standard: standard,
          preprocessing: preprocessing,
          sourcedata: sourcedata,
          protocol: protocol
        };
        new Promise(function (resolve) {
          return resolve(addEntryToSelection({
            entry: entry
          }));
        }).then(function () {
          return __WEBPACK_IMPORTED_MODULE_2_antd__["message"].success('Entry added to selection');
        }); // console.log(record)
      }
    });
    Object.defineProperty(_assertThisInitialized(_this), "hasBeenAdded", {
      configurable: true,
      enumerable: true,
      writable: true,
      value: function value(record) {
        var user_entries = _this.props.user_entries;

        for (var i = 0; i < user_entries.length; i++) {
          if (user_entries[i].protocol === record.protocol && user_entries[i].domain === record.domain && user_entries[i].standard === record.standard && user_entries[i].sourcedata === record.sourcedata) {
            return true;
          }
        }

        return false;
      }
    });
    Object.defineProperty(_assertThisInitialized(_this), "render", {
      configurable: true,
      enumerable: true,
      writable: true,
      value: function value() {
        var entries = _this.props.entries;
        var attributes = ['protocol', 'domain', 'standard', 'sourcedata'];
        var actions = [{
          title: 'Action',
          key: 'operation',
          fixed: 'right',
          width: 100,
          render: function render(record) {
            return _jsx(__WEBPACK_IMPORTED_MODULE_2_antd__["Button"], {
              disabled: _this.hasBeenAdded(record),
              icon: "plus",
              onClick: function onClick() {
                _this.processRaw(record);
              }
            });
          }
        }];
        var data = attributes.map(function (at) {
          return Object.assign({}, {
            title: __WEBPACK_IMPORTED_MODULE_5_lodash___default.a.capitalize(at),
            dataIndex: at,
            key: at //defaultSortOrder: 'ascend',
            //sorter: (a: any, b: any) => (a[at] - b[at])

          });
        });

        var columns = _toConsumableArray(data).concat(actions);

        var entries_with_keys = entries.map(function (el, i) {
          return Object.assign({}, _extends({}, el, {
            key: i
          }));
        });
        return _jsx(__WEBPACK_IMPORTED_MODULE_2_antd__["Table"], {
          dataSource: entries_with_keys,
          columns: columns
        });
      }
    });
    return _this;
  } // componentDidMount = () => {
  //   console.log(this.props)
  // }


  return TableSearchEntries;
}(__WEBPACK_IMPORTED_MODULE_0_react__["Component"]);

var mapStateToProps = function mapStateToProps(state) {
  return {
    protocol: state.new_entry.protocol,
    user_entries: state.user_selection.user_entries
  };
}; // REDUX STORE


var mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return {
    // actions
    addEntryToSelection: function addEntryToSelection(_ref) {
      var entry = _ref.entry;
      return dispatch({
        type: __WEBPACK_IMPORTED_MODULE_4_store_constants__["a" /* ADD_ENTRY_TO_USER_SELECTION */],
        entry: entry
      });
    }
  };
}; // MAPPING REDUX TO COMPONENT


var withRedux = Object(__WEBPACK_IMPORTED_MODULE_3_react_redux__["connect"])(mapStateToProps, mapDispatchToProps)(TableSearchEntries);


/***/ }),
/* 70 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(9)(false);
// imports


// module
exports.push([module.i, "._1Tdg7{background:#fff;border-radius:2px;-webkit-box-shadow:0 1px 3px rgba(0,0,0,.12),0 1px 2px rgba(0,0,0,.24);box-shadow:0 1px 3px rgba(0,0,0,.12),0 1px 2px rgba(0,0,0,.24);-webkit-transition:all .3s cubic-bezier(.25,.8,.25,1);-o-transition:all .3s cubic-bezier(.25,.8,.25,1);transition:all .3s cubic-bezier(.25,.8,.25,1);margin-bottom:3px;margin-left:5px;margin-right:5px;min-width:96vw;//:30vh}._1Tdg7:hover{-webkit-box-shadow:0 2px 6px rgba(0,0,0,.25),0 2px 4px rgba(0,0,0,.22);box-shadow:0 2px 6px rgba(0,0,0,.25),0 2px 4px rgba(0,0,0,.22)}._2DcmQ{background:#fbfbfb;border-radius:2px;-webkit-box-shadow:0 1px 3px rgba(0,0,0,.12),0 1px 2px rgba(0,0,0,.24);box-shadow:0 1px 3px rgba(0,0,0,.12),0 1px 2px rgba(0,0,0,.24);-webkit-transition:all .3s cubic-bezier(.25,.8,.25,1);-o-transition:all .3s cubic-bezier(.25,.8,.25,1);transition:all .3s cubic-bezier(.25,.8,.25,1);margin-bottom:3px;margin-left:5px;margin-right:5px;min-width:96vw;//:30vh}._2DcmQ:hover{-webkit-box-shadow:0 2px 6px rgba(0,0,0,.25),0 2px 4px rgba(0,0,0,.22);box-shadow:0 2px 6px rgba(0,0,0,.25),0 2px 4px rgba(0,0,0,.22)}.lcc6d:hover{cursor:pointer;text-decoration:underline}", ""]);

// exports
exports.locals = {
	"card": "_1Tdg7",
	"user_selection": "_2DcmQ",
	"btn_breadcrumb": "lcc6d"
};

/***/ }),
/* 71 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Styled; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_prop_types__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_semantic_ui_react__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_semantic_ui_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_semantic_ui_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_antd__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_antd___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_antd__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__AutocompleteProtocols__ = __webpack_require__(72);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__AutocompleteSourcedata__ = __webpack_require__(74);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__SelectDomains__ = __webpack_require__(75);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__SelectStandards__ = __webpack_require__(76);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__CreateNewEntry__ = __webpack_require__(77);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_react_redux__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_react_redux___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_9_react_redux__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_isomorphic_style_loader_lib_withStyles__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_isomorphic_style_loader_lib_withStyles___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_10_isomorphic_style_loader_lib_withStyles__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_theme_style_scss__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_theme_style_scss___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_11_theme_style_scss__);
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

var REACT_ELEMENT_TYPE;

function _jsx(type, props, key, children) { if (!REACT_ELEMENT_TYPE) { REACT_ELEMENT_TYPE = typeof Symbol === "function" && Symbol.for && Symbol.for("react.element") || 0xeac7; } var defaultProps = type && type.defaultProps; var childrenLength = arguments.length - 3; if (!props && childrenLength !== 0) { props = {}; } if (props && defaultProps) { for (var propName in defaultProps) { if (props[propName] === void 0) { props[propName] = defaultProps[propName]; } } } else if (!props) { props = defaultProps || {}; } if (childrenLength === 1) { props.children = children; } else if (childrenLength > 1) { var childArray = new Array(childrenLength); for (var i = 0; i < childrenLength; i++) { childArray[i] = arguments[i + 3]; } props.children = childArray; } return { $$typeof: REACT_ELEMENT_TYPE, type: type, key: key === undefined ? null : '' + key, ref: null, props: props, _owner: null }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }









 // redux

 // style




var _ref2 = _jsx(__WEBPACK_IMPORTED_MODULE_4__AutocompleteProtocols__["a" /* AutocompleteProtocols */], {});

var _ref3 = _jsx(__WEBPACK_IMPORTED_MODULE_2_semantic_ui_react__["Divider"], {});

var _ref4 = _jsx(__WEBPACK_IMPORTED_MODULE_5__AutocompleteSourcedata__["a" /* AutocompleteSourcedata */], {});

var _ref5 = _jsx(__WEBPACK_IMPORTED_MODULE_2_semantic_ui_react__["Divider"], {});

var _ref6 = _jsx(__WEBPACK_IMPORTED_MODULE_6__SelectDomains__["a" /* SelectDomains */], {});

var _ref7 = _jsx(__WEBPACK_IMPORTED_MODULE_2_semantic_ui_react__["Divider"], {});

var _ref8 = _jsx(__WEBPACK_IMPORTED_MODULE_7__SelectStandards__["a" /* SelectStandards */], {});

var _ref9 = _jsx(__WEBPACK_IMPORTED_MODULE_2_semantic_ui_react__["Divider"], {});

var _ref10 = _jsx(__WEBPACK_IMPORTED_MODULE_8__CreateNewEntry__["a" /* CreateNewEntry */], {});

var NewEntry =
/*#__PURE__*/
function (_PureComponent) {
  _inherits(NewEntry, _PureComponent);

  function NewEntry() {
    var _ref;

    var _temp, _this;

    _classCallCheck(this, NewEntry);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _possibleConstructorReturn(_this, (_temp = _this = _possibleConstructorReturn(this, (_ref = NewEntry.__proto__ || Object.getPrototypeOf(NewEntry)).call.apply(_ref, [this].concat(args))), Object.defineProperty(_assertThisInitialized(_this), "render", {
      configurable: true,
      enumerable: true,
      writable: true,
      value: function value() {
        var refresh_form = _this.props.refresh_form;
        return _jsx(__WEBPACK_IMPORTED_MODULE_3_antd__["Layout"], {
          className: __WEBPACK_IMPORTED_MODULE_11_theme_style_scss___default.a.card,
          style: {
            padding: '0 24px 24px'
          }
        }, refresh_form, _jsx(__WEBPACK_IMPORTED_MODULE_3_antd__["Breadcrumb"], {
          style: {
            margin: '16px 0'
          }
        }, void 0, _jsx(__WEBPACK_IMPORTED_MODULE_3_antd__["Breadcrumb"].Item, {}, void 0, "New Entry Form ", _jsx("span", {
          style: {
            color: 'red',
            textDecoration: 'underline'
          }
        }, void 0, "(*) mandatory fields"))), _ref2, _ref3, _ref4, _ref5, _ref6, _ref7, _ref8, _ref9, _ref10);
      }
    }), _temp));
  }

  return NewEntry;
}(__WEBPACK_IMPORTED_MODULE_0_react__["PureComponent"]);

var mapStateToProps = function mapStateToProps(state) {
  return {
    refresh_form: state.user_selection.refresh_form
  };
}; // MAPPING REDUX TO COMPONENT


var withRedux = Object(__WEBPACK_IMPORTED_MODULE_9_react_redux__["connect"])(mapStateToProps, null)(NewEntry);
var Styled = __WEBPACK_IMPORTED_MODULE_10_isomorphic_style_loader_lib_withStyles___default()(__WEBPACK_IMPORTED_MODULE_11_theme_style_scss___default.a)(withRedux);


/***/ }),
/* 72 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return withApollo; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_prop_types__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_antd__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_antd___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_antd__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_react_apollo__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_react_apollo___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_react_apollo__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__graphql__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_react_redux__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_react_redux___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_react_redux__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_store_constants__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_axios__ = __webpack_require__(73);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_axios___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_axios__);
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

var REACT_ELEMENT_TYPE;

function _jsx(type, props, key, children) { if (!REACT_ELEMENT_TYPE) { REACT_ELEMENT_TYPE = typeof Symbol === "function" && Symbol.for && Symbol.for("react.element") || 0xeac7; } var defaultProps = type && type.defaultProps; var childrenLength = arguments.length - 3; if (!props && childrenLength !== 0) { props = {}; } if (props && defaultProps) { for (var propName in defaultProps) { if (props[propName] === void 0) { props[propName] = defaultProps[propName]; } } } else if (!props) { props = defaultProps || {}; } if (childrenLength === 1) { props.children = children; } else if (childrenLength > 1) { var childArray = new Array(childrenLength); for (var i = 0; i < childrenLength; i++) { childArray[i] = arguments[i + 3]; } props.children = childArray; } return { $$typeof: REACT_ELEMENT_TYPE, type: type, key: key === undefined ? null : '' + key, ref: null, props: props, _owner: null }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }



 // Data Layer


 // redux





var _ref2 = _jsx("br", {});

var _ref3 = _jsx("span", {}, void 0, "(Fetched from the Yellow Pages database)");

var _ref4 = _jsx(__WEBPACK_IMPORTED_MODULE_2_antd__["Input"], {
  suffix: _jsx(__WEBPACK_IMPORTED_MODULE_2_antd__["Icon"], {
    type: "search",
    className: "certain-category-icon"
  })
});

var AutocompleteProtocols =
/*#__PURE__*/
function (_Component) {
  _inherits(AutocompleteProtocols, _Component);

  function AutocompleteProtocols(props) {
    var _this;

    _classCallCheck(this, AutocompleteProtocols);

    _this = _possibleConstructorReturn(this, (AutocompleteProtocols.__proto__ || Object.getPrototypeOf(AutocompleteProtocols)).call(this, props));
    Object.defineProperty(_assertThisInitialized(_this), "componentDidMount", {
      configurable: true,
      enumerable: true,
      writable: true,
      value: function value() {
        _this.setState({
          dataSource: []
        });
      }
    });
    Object.defineProperty(_assertThisInitialized(_this), "updateProtocols", {
      configurable: true,
      enumerable: true,
      writable: true,
      value: function value(pattern) {
        var _this$props = _this.props,
            refetch = _this$props.data.refetch,
            updateProtocolName = _this$props.updateProtocolName,
            updateYPSourcedata = _this$props.updateYPSourcedata;
        updateProtocolName({
          protocol: ''
        });

        if (pattern.length > 3) {
          refetch({
            pattern: pattern
          }).then(function (response) {
            var dataSource = response.data['getYPProtocols'];
            new Promise(function (resolve) {
              return resolve(_this.setState({
                dataSource: dataSource
              }));
            }); //  .then(() => updateYPSourcedata({ yp_sourcedata: ['A', 'B', 'C'] }))
          });
        }
      }
    });
    Object.defineProperty(_assertThisInitialized(_this), "selectProtocolName", {
      configurable: true,
      enumerable: true,
      writable: true,
      value: function value(_ref) {
        var protocol = _ref.protocol;
        var _this$props2 = _this.props,
            updateProtocolName = _this$props2.updateProtocolName,
            updateYPSourcedata = _this$props2.updateYPSourcedata;
        new Promise(function (resolve) {
          return resolve(updateProtocolName({
            protocol: protocol
          }));
        }).then(function () {
          return __WEBPACK_IMPORTED_MODULE_7_axios___default.a.get('https://jsonplaceholder.typicode.com/posts/1').then(function (response) {
            var data = response.data;
            console.log(data);
            updateYPSourcedata({
              yp_sourcedata: ['/path1', '/path2', '/path3']
            });
          }).catch(function (error) {
            console.log(error);
          });
        });
      }
    });
    Object.defineProperty(_assertThisInitialized(_this), "render", {
      configurable: true,
      enumerable: true,
      writable: true,
      value: function value() {
        var dataSource = _this.state.dataSource;
        return _jsx(__WEBPACK_IMPORTED_MODULE_0_react__["Fragment"], {}, void 0, _jsx("span", {
          style: {
            fontWeight: '600',
            textDecoration: 'underline'
          }
        }, void 0, "Protocol Name ", _jsx("span", {
          style: {
            color: 'red'
          }
        }, void 0, "(*)"), ":"), _ref2, _ref3, _jsx(__WEBPACK_IMPORTED_MODULE_2_antd__["AutoComplete"], {
          className: "certain-category-search",
          dropdownClassName: "certain-category-search-dropdown",
          placeholder: "Search For Protocols, (4 Characters min)",
          dataSource: dataSource,
          onSearch: function onSearch(value) {
            return _this.updateProtocols(value);
          },
          onSelect: function onSelect(el) {
            return _this.selectProtocolName({
              protocol: el
            });
          },
          style: {
            width: 350
          }
        }, void 0, _ref4));
      }
    });
    _this.state = {
      dataSource: []
    };
    return _this;
  }

  return AutocompleteProtocols;
}(__WEBPACK_IMPORTED_MODULE_0_react__["Component"]);

var mapStateToProps = function mapStateToProps(state) {
  return {
    protocol: state.new_entry.protocol
  };
}; // REDUX STORE


var mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return {
    // actions
    updateProtocolName: function updateProtocolName(_ref5) {
      var protocol = _ref5.protocol;
      return dispatch({
        type: __WEBPACK_IMPORTED_MODULE_6_store_constants__["h" /* UPDATE_PROTOCOL_NEW_ENTRY */],
        protocol: protocol
      });
    },
    updateYPSourcedata: function updateYPSourcedata(_ref6) {
      var yp_sourcedata = _ref6.yp_sourcedata;
      return dispatch({
        type: __WEBPACK_IMPORTED_MODULE_6_store_constants__["i" /* UPDATE_SOURCEDATA_FROM_API_NEW_ENTRY */],
        yp_sourcedata: yp_sourcedata
      });
    }
  };
}; // MAPPING REDUX TO COMPONENT


var withRedux = Object(__WEBPACK_IMPORTED_MODULE_5_react_redux__["connect"])(mapStateToProps, mapDispatchToProps)(AutocompleteProtocols);
var withApollo = Object(__WEBPACK_IMPORTED_MODULE_3_react_apollo__["compose"])(Object(__WEBPACK_IMPORTED_MODULE_3_react_apollo__["graphql"])(__WEBPACK_IMPORTED_MODULE_4__graphql__["b" /* queries */].yp_protocols, {
  options: {
    variables: {
      pattern: ''
    }
  }
}))(withRedux);


/***/ }),
/* 73 */
/***/ (function(module, exports) {

module.exports = require("axios");

/***/ }),
/* 74 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return withRedux; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_prop_types__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_antd__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_antd___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_antd__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_react_apollo__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_react_apollo___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_react_apollo__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__graphql__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_react_redux__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_react_redux___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_react_redux__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_store_constants__ = __webpack_require__(5);
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

var REACT_ELEMENT_TYPE;

function _jsx(type, props, key, children) { if (!REACT_ELEMENT_TYPE) { REACT_ELEMENT_TYPE = typeof Symbol === "function" && Symbol.for && Symbol.for("react.element") || 0xeac7; } var defaultProps = type && type.defaultProps; var childrenLength = arguments.length - 3; if (!props && childrenLength !== 0) { props = {}; } if (props && defaultProps) { for (var propName in defaultProps) { if (props[propName] === void 0) { props[propName] = defaultProps[propName]; } } } else if (!props) { props = defaultProps || {}; } if (childrenLength === 1) { props.children = children; } else if (childrenLength > 1) { var childArray = new Array(childrenLength); for (var i = 0; i < childrenLength; i++) { childArray[i] = arguments[i + 3]; } props.children = childArray; } return { $$typeof: REACT_ELEMENT_TYPE, type: type, key: key === undefined ? null : '' + key, ref: null, props: props, _owner: null }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }



 // Data Layer


 // redux




var _ref = _jsx("br", {});

var _ref2 = _jsx("span", {}, void 0, "(Fetched from the Yellow Pages database)");

var _ref3 = _jsx(__WEBPACK_IMPORTED_MODULE_2_antd__["Input"], {
  suffix: _jsx(__WEBPACK_IMPORTED_MODULE_2_antd__["Icon"], {
    type: "search",
    className: "certain-category-icon"
  })
});

var AutocompleteSourcedata =
/*#__PURE__*/
function (_Component) {
  _inherits(AutocompleteSourcedata, _Component);

  function AutocompleteSourcedata(props) {
    var _this;

    _classCallCheck(this, AutocompleteSourcedata);

    _this = _possibleConstructorReturn(this, (AutocompleteSourcedata.__proto__ || Object.getPrototypeOf(AutocompleteSourcedata)).call(this, props));
    Object.defineProperty(_assertThisInitialized(_this), "updateSourcedata", {
      configurable: true,
      enumerable: true,
      writable: true,
      value: function value(pattern) {
        var updateSourcedata = _this.props.updateSourcedata;
        updateSourcedata({
          sourcedata: ''
        }); // refetch({ pattern })
        //   .then((response: any) => {
        //     const {
        //       data: {
        //         ['getYPSourcedata']: dataSource
        //       }
        //     } = response
        //
        //     this.setState({
        //       dataSource
        //     })
        //   })
      }
    });
    Object.defineProperty(_assertThisInitialized(_this), "render", {
      configurable: true,
      enumerable: true,
      writable: true,
      value: function value() {
        var dataSource = _this.state.dataSource;
        var _this$props = _this.props,
            updatePreprocessing = _this$props.updatePreprocessing,
            updateSourcedata = _this$props.updateSourcedata,
            preprocessing = _this$props.preprocessing,
            protocol = _this$props.protocol,
            yp_sourcedata = _this$props.yp_sourcedata;
        return _jsx(__WEBPACK_IMPORTED_MODULE_0_react__["Fragment"], {}, void 0, _jsx("span", {
          style: {
            fontWeight: '600',
            textDecoration: 'underline'
          }
        }, void 0, "Source data ", _jsx("span", {
          style: {
            color: 'red'
          }
        }, void 0, "(*)"), " :"), "\xA0\xA0", _jsx(__WEBPACK_IMPORTED_MODULE_2_antd__["Checkbox"], {
          onChange: function onChange(e) {
            return updatePreprocessing({
              preprocessing: e.target.checked
            });
          },
          checked: preprocessing
        }, void 0, "Pre-processing"), _ref, !preprocessing ? _jsx(__WEBPACK_IMPORTED_MODULE_0_react__["Fragment"], {}, void 0, _ref2, _jsx(__WEBPACK_IMPORTED_MODULE_2_antd__["AutoComplete"], {
          className: "certain-category-search",
          dropdownClassName: "certain-category-search-dropdown",
          placeholder: "Search For Source data, (1 Character min)",
          dataSource: yp_sourcedata,
          onSearch: function onSearch(value) {
            return _this.updateSourcedata(value);
          },
          onSelect: function onSelect(el) {
            return updateSourcedata({
              sourcedata: el
            });
          },
          disabled: protocol.length === 0,
          style: {
            width: 350
          }
        }, void 0, _ref3)) : null);
      }
    });
    _this.state = {
      dataSource: []
    };
    return _this;
  }

  return AutocompleteSourcedata;
}(__WEBPACK_IMPORTED_MODULE_0_react__["Component"]);

var mapStateToProps = function mapStateToProps(state) {
  return {
    preprocessing: state.new_entry.preprocessing,
    sourcedata: state.new_entry.sourcedata,
    protocol: state.new_entry.protocol,
    yp_sourcedata: state.new_entry.yp_sourcedata
  };
}; // REDUX STORE


var mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return {
    // actions
    updatePreprocessing: function updatePreprocessing(_ref4) {
      var preprocessing = _ref4.preprocessing;
      return dispatch({
        type: __WEBPACK_IMPORTED_MODULE_6_store_constants__["g" /* UPDATE_PREPROCESSING_NEW_ENTRY */],
        preprocessing: preprocessing
      });
    },
    updateSourcedata: function updateSourcedata(_ref5) {
      var sourcedata = _ref5.sourcedata;
      return dispatch({
        type: __WEBPACK_IMPORTED_MODULE_6_store_constants__["j" /* UPDATE_SOURCEDATA_NEW_ENTRY */],
        sourcedata: sourcedata
      });
    }
  };
}; // MAPPING REDUX TO COMPONENT


var withRedux = Object(__WEBPACK_IMPORTED_MODULE_5_react_redux__["connect"])(mapStateToProps, mapDispatchToProps)(AutocompleteSourcedata); // const withApollo = compose(
//   graphql(queries.yp_sourcedata,
//     { options: { variables: { pattern: '' } } })
// )(withRedux)



/***/ }),
/* 75 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return withApollo; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_prop_types__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_semantic_ui_react__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_semantic_ui_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_semantic_ui_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_react_redux__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_react_redux___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_react_redux__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_store_constants__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_react_apollo__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_react_apollo___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_react_apollo__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__graphql__ = __webpack_require__(7);
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

var REACT_ELEMENT_TYPE;

function _jsx(type, props, key, children) { if (!REACT_ELEMENT_TYPE) { REACT_ELEMENT_TYPE = typeof Symbol === "function" && Symbol.for && Symbol.for("react.element") || 0xeac7; } var defaultProps = type && type.defaultProps; var childrenLength = arguments.length - 3; if (!props && childrenLength !== 0) { props = {}; } if (props && defaultProps) { for (var propName in defaultProps) { if (props[propName] === void 0) { props[propName] = defaultProps[propName]; } } } else if (!props) { props = defaultProps || {}; } if (childrenLength === 1) { props.children = children; } else if (childrenLength > 1) { var childArray = new Array(childrenLength); for (var i = 0; i < childrenLength; i++) { childArray[i] = arguments[i + 3]; } props.children = childArray; } return { $$typeof: REACT_ELEMENT_TYPE, type: type, key: key === undefined ? null : '' + key, ref: null, props: props, _owner: null }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }



 // redux


 // Data Layer


 // eslint-disable-line

var _ref2 = _jsx("br", {});

var SelectDomains =
/*#__PURE__*/
function (_Component) {
  _inherits(SelectDomains, _Component);

  function SelectDomains() {
    var _ref;

    var _temp, _this;

    _classCallCheck(this, SelectDomains);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _possibleConstructorReturn(_this, (_temp = _this = _possibleConstructorReturn(this, (_ref = SelectDomains.__proto__ || Object.getPrototypeOf(SelectDomains)).call.apply(_ref, [this].concat(args))), Object.defineProperty(_assertThisInitialized(_this), "render", {
      configurable: true,
      enumerable: true,
      writable: true,
      value: function value() {
        var _this$props = _this.props,
            updateDomain = _this$props.updateDomain,
            getAllDomains = _this$props.data.getAllDomains; // console.log(this.props)

        var extraOptions = getAllDomains ? getAllDomains.map(function (el) {
          return Object.assign({}, {
            key: el.code,
            value: el.code,
            text: el.code + ' - ' + el.name
          });
        }) : [];
        var domainsOptions = [{
          key: 'empty',
          text: 'Select a Domain',
          value: ''
        }].concat(extraOptions);
        return _jsx(__WEBPACK_IMPORTED_MODULE_0_react__["Fragment"], {}, void 0, _jsx("span", {
          style: {
            fontWeight: '600',
            textDecoration: 'underline'
          }
        }, void 0, "Domain ", _jsx("span", {
          style: {
            color: 'red'
          }
        }, void 0, "(*)"), " :"), _ref2, _jsx("div", {
          style: {
            maxWidth: '300px'
          }
        }, void 0, _jsx(__WEBPACK_IMPORTED_MODULE_2_semantic_ui_react__["Dropdown"], {
          upward: true,
          placeholder: "Select a Domain",
          search: true,
          selection: true,
          options: domainsOptions,
          onChange: function onChange(e, _ref3) {
            var value = _ref3.value;
            return updateDomain({
              domain: value
            });
          }
        })));
      }
    }), _temp));
  }

  return SelectDomains;
}(__WEBPACK_IMPORTED_MODULE_0_react__["Component"]);

var mapStateToProps = function mapStateToProps(state) {
  return {
    preprocessing: state.new_entry.preprocessing,
    sourcedata: state.new_entry.sourcedata
  };
}; // REDUX STORE


var mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return {
    // actions
    updateDomain: function updateDomain(_ref4) {
      var domain = _ref4.domain;
      return dispatch({
        type: __WEBPACK_IMPORTED_MODULE_4_store_constants__["d" /* UPDATE_DOMAIN_NEW_ENTRY */],
        domain: domain
      });
    }
  };
}; // MAPPING REDUX TO COMPONENT


var withRedux = Object(__WEBPACK_IMPORTED_MODULE_3_react_redux__["connect"])(mapStateToProps, mapDispatchToProps)(SelectDomains);
var withApollo = Object(__WEBPACK_IMPORTED_MODULE_5_react_apollo__["compose"])(Object(__WEBPACK_IMPORTED_MODULE_5_react_apollo__["graphql"])(__WEBPACK_IMPORTED_MODULE_6__graphql__["b" /* queries */].domains_query, {
  options: {}
}))(withRedux);


/***/ }),
/* 76 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return withApollo; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_prop_types__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_semantic_ui_react__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_semantic_ui_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_semantic_ui_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_react_redux__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_react_redux___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_react_redux__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_store_constants__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_react_apollo__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_react_apollo___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_react_apollo__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__graphql__ = __webpack_require__(7);
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

var REACT_ELEMENT_TYPE;

function _jsx(type, props, key, children) { if (!REACT_ELEMENT_TYPE) { REACT_ELEMENT_TYPE = typeof Symbol === "function" && Symbol.for && Symbol.for("react.element") || 0xeac7; } var defaultProps = type && type.defaultProps; var childrenLength = arguments.length - 3; if (!props && childrenLength !== 0) { props = {}; } if (props && defaultProps) { for (var propName in defaultProps) { if (props[propName] === void 0) { props[propName] = defaultProps[propName]; } } } else if (!props) { props = defaultProps || {}; } if (childrenLength === 1) { props.children = children; } else if (childrenLength > 1) { var childArray = new Array(childrenLength); for (var i = 0; i < childrenLength; i++) { childArray[i] = arguments[i + 3]; } props.children = childArray; } return { $$typeof: REACT_ELEMENT_TYPE, type: type, key: key === undefined ? null : '' + key, ref: null, props: props, _owner: null }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }



 // redux


 // Data Layer


 // eslint-disable-line

var _ref2 = _jsx("br", {});

var SelectStandards =
/*#__PURE__*/
function (_Component) {
  _inherits(SelectStandards, _Component);

  function SelectStandards() {
    var _ref;

    var _temp, _this;

    _classCallCheck(this, SelectStandards);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _possibleConstructorReturn(_this, (_temp = _this = _possibleConstructorReturn(this, (_ref = SelectStandards.__proto__ || Object.getPrototypeOf(SelectStandards)).call.apply(_ref, [this].concat(args))), Object.defineProperty(_assertThisInitialized(_this), "render", {
      configurable: true,
      enumerable: true,
      writable: true,
      value: function value() {
        var _this$props = _this.props,
            updateStandard = _this$props.updateStandard,
            getAllStandards = _this$props.data.getAllStandards;
        var extraOptions = getAllStandards ? getAllStandards.map(function (el) {
          return Object.assign({}, {
            key: el.code,
            value: el.code,
            text: el.code + ' - ' + el.name
          });
        }) : [];
        var standardsOptions = [{
          key: 'empty',
          text: 'Select a Standard',
          value: ''
        }].concat(extraOptions);
        return _jsx(__WEBPACK_IMPORTED_MODULE_0_react__["Fragment"], {}, void 0, _jsx("span", {
          style: {
            fontWeight: '600',
            textDecoration: 'underline'
          }
        }, void 0, "Standard ", _jsx("span", {
          style: {
            color: 'red'
          }
        }, void 0, "(*)"), " :"), _ref2, _jsx("div", {
          style: {
            maxWidth: '300px'
          }
        }, void 0, _jsx(__WEBPACK_IMPORTED_MODULE_2_semantic_ui_react__["Dropdown"], {
          upward: true,
          placeholder: "Select a Standard",
          search: true,
          selection: true,
          options: standardsOptions,
          onChange: function onChange(e, _ref3) {
            var value = _ref3.value;
            return updateStandard({
              standard: value
            });
          }
        })));
      }
    }), _temp));
  }

  return SelectStandards;
}(__WEBPACK_IMPORTED_MODULE_0_react__["Component"]);

var mapStateToProps = function mapStateToProps(state) {
  return {
    preprocessing: state.new_entry.preprocessing,
    sourcedata: state.new_entry.sourcedata
  };
}; // REDUX STORE


var mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return {
    // actions
    updateStandard: function updateStandard(_ref4) {
      var standard = _ref4.standard;
      return dispatch({
        type: __WEBPACK_IMPORTED_MODULE_4_store_constants__["k" /* UPDATE_STANDARD_NEW_ENTRY */],
        standard: standard
      });
    }
  };
}; // MAPPING REDUX TO COMPONENT


var withRedux = Object(__WEBPACK_IMPORTED_MODULE_3_react_redux__["connect"])(mapStateToProps, mapDispatchToProps)(SelectStandards);
var withApollo = Object(__WEBPACK_IMPORTED_MODULE_5_react_apollo__["compose"])(Object(__WEBPACK_IMPORTED_MODULE_5_react_apollo__["graphql"])(__WEBPACK_IMPORTED_MODULE_6__graphql__["b" /* queries */].standards_query, {
  options: {}
}))(withRedux);


/***/ }),
/* 77 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return withRedux; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_prop_types__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_antd__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_antd___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_antd__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_react_redux__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_react_redux___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_react_redux__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_store_constants__ = __webpack_require__(5);
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

var REACT_ELEMENT_TYPE;

function _jsx(type, props, key, children) { if (!REACT_ELEMENT_TYPE) { REACT_ELEMENT_TYPE = typeof Symbol === "function" && Symbol.for && Symbol.for("react.element") || 0xeac7; } var defaultProps = type && type.defaultProps; var childrenLength = arguments.length - 3; if (!props && childrenLength !== 0) { props = {}; } if (props && defaultProps) { for (var propName in defaultProps) { if (props[propName] === void 0) { props[propName] = defaultProps[propName]; } } } else if (!props) { props = defaultProps || {}; } if (childrenLength === 1) { props.children = children; } else if (childrenLength > 1) { var childArray = new Array(childrenLength); for (var i = 0; i < childrenLength; i++) { childArray[i] = arguments[i + 3]; } props.children = childArray; } return { $$typeof: REACT_ELEMENT_TYPE, type: type, key: key === undefined ? null : '' + key, ref: null, props: props, _owner: null }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }



 // Data Layer
// import { graphql, compose } from 'react-apollo'
// import { mutations } from '~graphql'
// redux




var _ref2 = _jsx(__WEBPACK_IMPORTED_MODULE_2_antd__["Icon"], {
  type: "plus"
});

var CreateNewEntry =
/*#__PURE__*/
function (_Component) {
  _inherits(CreateNewEntry, _Component);

  function CreateNewEntry() {
    var _ref;

    var _temp, _this;

    _classCallCheck(this, CreateNewEntry);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _possibleConstructorReturn(_this, (_temp = _this = _possibleConstructorReturn(this, (_ref = CreateNewEntry.__proto__ || Object.getPrototypeOf(CreateNewEntry)).call.apply(_ref, [this].concat(args))), Object.defineProperty(_assertThisInitialized(_this), "createEntry", {
      configurable: true,
      enumerable: true,
      writable: true,
      value: function value() {
        var _this$props = _this.props,
            addEntryToSelection = _this$props.addEntryToSelection,
            protocol = _this$props.protocol,
            preprocessing = _this$props.preprocessing,
            sourcedata = _this$props.sourcedata,
            domain = _this$props.domain,
            standard = _this$props.standard;
        var entry = {
          protocol: protocol,
          preprocessing: preprocessing,
          sourcedata: sourcedata,
          domain: domain,
          standard: standard
        };

        if (!_this.hasBeenAdded(entry)) {
          new Promise(function (resolve) {
            resolve(addEntryToSelection({
              entry: entry
            }));
          }).then(function () {
            __WEBPACK_IMPORTED_MODULE_2_antd__["message"].success('A New Entry has been created');
          });
        } else {
          __WEBPACK_IMPORTED_MODULE_2_antd__["message"].error('Entry is already in your selection');
        } // mutate({
        //   variables: { meta },
        //   // refetchQueries: [{ query: queries.cooking_actions_query }]
        // })
        //   .then((response: {data: {}} ) => {
        //     const {
        //       data
        //     } = response
        //     console.log('got data', data) // eslint-disable-line
        //   }).catch((error: {}) => {
        //     console.log('there was an error sending the query', error) // eslint-disable-line
        //   })
        //
        //
        // new Promise((resolve: any) => {
        //   resolve(commitNewEntryCreation())
        // }).then(()=> {
        //   message.success('A New Entry has been created')
        // })

      }
    }), Object.defineProperty(_assertThisInitialized(_this), "hasBeenAdded", {
      configurable: true,
      enumerable: true,
      writable: true,
      value: function value(entry) {
        var user_entries = _this.props.user_entries;

        for (var i = 0; i < user_entries.length; i++) {
          if (user_entries[i].protocol === entry.protocol && user_entries[i].domain === entry.domain && user_entries[i].standard === entry.standard && user_entries[i].sourcedata === entry.sourcedata) {
            return true;
          }
        }

        return false;
      }
    }), Object.defineProperty(_assertThisInitialized(_this), "render", {
      configurable: true,
      enumerable: true,
      writable: true,
      value: function value() {
        var _this$props2 = _this.props,
            protocol = _this$props2.protocol,
            preprocessing = _this$props2.preprocessing,
            sourcedata = _this$props2.sourcedata,
            domain = _this$props2.domain,
            standard = _this$props2.standard;
        var disabledButton = protocol.length === 0 || !preprocessing && sourcedata.length === 0 || domain.length === 0 || standard.length === 0;
        return _jsx(__WEBPACK_IMPORTED_MODULE_0_react__["Fragment"], {}, void 0, _jsx("div", {
          style: {
            maxWidth: '200px'
          }
        }, void 0, _jsx(__WEBPACK_IMPORTED_MODULE_2_antd__["Button"], {
          type: "primary",
          disabled: disabledButton,
          onClick: function onClick() {
            return _this.createEntry();
          }
        }, void 0, _ref2, "New Entry")));
      }
    }), _temp));
  }

  return CreateNewEntry;
}(__WEBPACK_IMPORTED_MODULE_0_react__["Component"]);

var mapStateToProps = function mapStateToProps(state) {
  return {
    protocol: state.new_entry.protocol,
    preprocessing: state.new_entry.preprocessing,
    sourcedata: state.new_entry.sourcedata,
    domain: state.new_entry.domain,
    standard: state.new_entry.standard,
    user_entries: state.user_selection.user_entries
  };
}; // REDUX STORE


var mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return {
    // actions
    commitNewEntryCreation: function commitNewEntryCreation() {
      return dispatch({
        type: __WEBPACK_IMPORTED_MODULE_4_store_constants__["b" /* COMMIT_CREATION_NEW_ENTRY */]
      });
    },
    addEntryToSelection: function addEntryToSelection(_ref3) {
      var entry = _ref3.entry;
      return dispatch({
        type: __WEBPACK_IMPORTED_MODULE_4_store_constants__["a" /* ADD_ENTRY_TO_USER_SELECTION */],
        entry: entry
      });
    }
  };
}; // MAPPING REDUX TO COMPONENT


var withRedux = Object(__WEBPACK_IMPORTED_MODULE_3_react_redux__["connect"])(mapStateToProps, mapDispatchToProps)(CreateNewEntry); // const withApollo = compose(
//   graphql(mutations.new_entry_mm)
// )(withRedux)



/***/ }),
/* 78 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Styled; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_antd__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_antd___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_antd__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_isomorphic_style_loader_lib_withStyles__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_isomorphic_style_loader_lib_withStyles___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_isomorphic_style_loader_lib_withStyles__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_theme_style_scss__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_theme_style_scss___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_theme_style_scss__);
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

var REACT_ELEMENT_TYPE;

function _jsx(type, props, key, children) { if (!REACT_ELEMENT_TYPE) { REACT_ELEMENT_TYPE = typeof Symbol === "function" && Symbol.for && Symbol.for("react.element") || 0xeac7; } var defaultProps = type && type.defaultProps; var childrenLength = arguments.length - 3; if (!props && childrenLength !== 0) { props = {}; } if (props && defaultProps) { for (var propName in defaultProps) { if (props[propName] === void 0) { props[propName] = defaultProps[propName]; } } } else if (!props) { props = defaultProps || {}; } if (childrenLength === 1) { props.children = children; } else if (childrenLength > 1) { var childArray = new Array(childrenLength); for (var i = 0; i < childrenLength; i++) { childArray[i] = arguments[i + 3]; } props.children = childArray; } return { $$typeof: REACT_ELEMENT_TYPE, type: type, key: key === undefined ? null : '' + key, ref: null, props: props, _owner: null }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }






var _ref2 = _jsx(__WEBPACK_IMPORTED_MODULE_1_antd__["Breadcrumb"].Item, {}, void 0, "Upload CSV Metadata File (.txt format)");

var _ref3 = _jsx(__WEBPACK_IMPORTED_MODULE_1_antd__["Button"], {}, void 0, _jsx(__WEBPACK_IMPORTED_MODULE_1_antd__["Icon"], {
  type: "upload"
}), " Upload");

var UploadForm =
/*#__PURE__*/
function (_Component) {
  _inherits(UploadForm, _Component);

  function UploadForm() {
    var _ref;

    var _temp, _this;

    _classCallCheck(this, UploadForm);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _possibleConstructorReturn(_this, (_temp = _this = _possibleConstructorReturn(this, (_ref = UploadForm.__proto__ || Object.getPrototypeOf(UploadForm)).call.apply(_ref, [this].concat(args))), Object.defineProperty(_assertThisInitialized(_this), "processFile", {
      configurable: true,
      enumerable: true,
      writable: true,
      value: function value(file) {
        console.log(file); // eslint-disable-line
      }
    }), Object.defineProperty(_assertThisInitialized(_this), "render", {
      configurable: true,
      enumerable: true,
      writable: true,
      value: function value() {
        var props = {
          action: 'dummy-upload',
          accept: '.pdf',
          // listType: 'picture',
          defaultFileList: [],
          beforeUpload: _this.processFile
        };
        return _jsx(__WEBPACK_IMPORTED_MODULE_0_react__["Fragment"], {}, void 0, _jsx(__WEBPACK_IMPORTED_MODULE_1_antd__["Layout"], {
          className: __WEBPACK_IMPORTED_MODULE_3_theme_style_scss___default.a.card,
          style: {
            padding: '0 24px 24px'
          }
        }, void 0, _jsx(__WEBPACK_IMPORTED_MODULE_1_antd__["Breadcrumb"], {
          style: {
            margin: '16px 0'
          }
        }, void 0, _ref2), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_1_antd__["Upload"], props, _ref3)));
      }
    }), _temp));
  }

  return UploadForm;
}(__WEBPACK_IMPORTED_MODULE_0_react__["Component"]);

var Styled = __WEBPACK_IMPORTED_MODULE_2_isomorphic_style_loader_lib_withStyles___default()(__WEBPACK_IMPORTED_MODULE_3_theme_style_scss___default.a)(UploadForm);


/***/ }),
/* 79 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Styled; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_antd__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_antd___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_antd__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_isomorphic_style_loader_lib_withStyles__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_isomorphic_style_loader_lib_withStyles___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_isomorphic_style_loader_lib_withStyles__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_theme_style_scss__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_theme_style_scss___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_theme_style_scss__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__Dictionaries__ = __webpack_require__(80);
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

var REACT_ELEMENT_TYPE;

function _jsx(type, props, key, children) { if (!REACT_ELEMENT_TYPE) { REACT_ELEMENT_TYPE = typeof Symbol === "function" && Symbol.for && Symbol.for("react.element") || 0xeac7; } var defaultProps = type && type.defaultProps; var childrenLength = arguments.length - 3; if (!props && childrenLength !== 0) { props = {}; } if (props && defaultProps) { for (var propName in defaultProps) { if (props[propName] === void 0) { props[propName] = defaultProps[propName]; } } } else if (!props) { props = defaultProps || {}; } if (childrenLength === 1) { props.children = children; } else if (childrenLength > 1) { var childArray = new Array(childrenLength); for (var i = 0; i < childrenLength; i++) { childArray[i] = arguments[i + 3]; } props.children = childArray; } return { $$typeof: REACT_ELEMENT_TYPE, type: type, key: key === undefined ? null : '' + key, ref: null, props: props, _owner: null }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }



var SubMenu = __WEBPACK_IMPORTED_MODULE_1_antd__["Menu"].SubMenu;
var Content = __WEBPACK_IMPORTED_MODULE_1_antd__["Layout"].Content,
    Sider = __WEBPACK_IMPORTED_MODULE_1_antd__["Layout"].Sider;




var _ref3 = _jsx(SubMenu, {
  title: _jsx("span", {}, void 0, _jsx(__WEBPACK_IMPORTED_MODULE_1_antd__["Icon"], {
    type: "book"
  }), "Dictionaries")
}, "sub1", _jsx(__WEBPACK_IMPORTED_MODULE_1_antd__["Menu"].Item, {}, "Domains", "Domains"), _jsx(__WEBPACK_IMPORTED_MODULE_1_antd__["Menu"].Item, {}, "Standards", "Standards"), _jsx(__WEBPACK_IMPORTED_MODULE_1_antd__["Menu"].Item, {}, "Targets", "Targets"), _jsx(__WEBPACK_IMPORTED_MODULE_1_antd__["Menu"].Item, {}, "Default Variables", "Default Variables"));

var _ref4 = _jsx(SubMenu, {
  title: _jsx("span", {}, void 0, _jsx(__WEBPACK_IMPORTED_MODULE_1_antd__["Icon"], {
    type: "table"
  }), "Entries")
}, "sub2", _jsx(__WEBPACK_IMPORTED_MODULE_1_antd__["Menu"].Item, {}, "Entries", "Available Entries"));

var _ref5 = _jsx(__WEBPACK_IMPORTED_MODULE_1_antd__["Breadcrumb"].Item, {}, void 0, "Settings");

var _ref6 = _jsx(__WEBPACK_IMPORTED_MODULE_4__Dictionaries__["a" /* Settings_Domains */], {});

var _ref7 = _jsx(__WEBPACK_IMPORTED_MODULE_4__Dictionaries__["c" /* Settings_Standards */], {});

var _ref8 = _jsx(__WEBPACK_IMPORTED_MODULE_4__Dictionaries__["d" /* Settings_Targets */], {});

var _ref9 = _jsx(__WEBPACK_IMPORTED_MODULE_4__Dictionaries__["b" /* Settings_Entries */], {});

var Settings =
/*#__PURE__*/
function (_Component) {
  _inherits(Settings, _Component);

  function Settings() {
    var _ref;

    var _temp, _this;

    _classCallCheck(this, Settings);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _possibleConstructorReturn(_this, (_temp = _this = _possibleConstructorReturn(this, (_ref = Settings.__proto__ || Object.getPrototypeOf(Settings)).call.apply(_ref, [this].concat(args))), Object.defineProperty(_assertThisInitialized(_this), "state", {
      configurable: true,
      enumerable: true,
      writable: true,
      value: {
        view: 'Default'
      }
    }), Object.defineProperty(_assertThisInitialized(_this), "setView", {
      configurable: true,
      enumerable: true,
      writable: true,
      value: function value(view) {
        return _this.setState({
          view: view
        });
      }
    }), Object.defineProperty(_assertThisInitialized(_this), "render", {
      configurable: true,
      enumerable: true,
      writable: true,
      value: function value() {
        var view = _this.state.view;
        return _jsx(__WEBPACK_IMPORTED_MODULE_0_react__["Fragment"], {}, void 0, _jsx(__WEBPACK_IMPORTED_MODULE_1_antd__["Layout"], {
          className: __WEBPACK_IMPORTED_MODULE_3_theme_style_scss___default.a.card
        }, void 0, _jsx(Sider, {
          width: 200,
          style: {
            background: '#fff'
          }
        }, void 0, _jsx(__WEBPACK_IMPORTED_MODULE_1_antd__["Menu"], {
          mode: "inline",
          defaultSelectedKeys: ['1'],
          defaultOpenKeys: ['sub1'],
          onClick: function onClick(_ref2) {
            var key = _ref2.key;
            return _this.setView(key);
          },
          style: {
            height: '100%',
            borderRight: 0
          },
          theme: "dark"
        }, void 0, _ref3, _ref4)), _jsx(__WEBPACK_IMPORTED_MODULE_1_antd__["Layout"], {
          style: {
            padding: '0 24px 24px'
          }
        }, void 0, _jsx(__WEBPACK_IMPORTED_MODULE_1_antd__["Breadcrumb"], {
          style: {
            margin: '16px 0'
          }
        }, void 0, _ref5, view ? _jsx(__WEBPACK_IMPORTED_MODULE_1_antd__["Breadcrumb"].Item, {}, void 0, view) : null), _jsx(Content, {
          style: {
            background: '#fff',
            padding: 24,
            margin: 0,
            minHeight: 280
          }
        }, void 0, {
          Domains: _ref6,
          Standards: _ref7,
          Targets: _ref8,
          Entries: _ref9 // Default: (
          //   <Fragment>
          //     Select a view
          //   </Fragment>
          //)

        }[view]))));
      }
    }), _temp));
  }

  return Settings;
}(__WEBPACK_IMPORTED_MODULE_0_react__["Component"]);

var Styled = __WEBPACK_IMPORTED_MODULE_2_isomorphic_style_loader_lib_withStyles___default()(__WEBPACK_IMPORTED_MODULE_3_theme_style_scss___default.a)(Settings);


/***/ }),
/* 80 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Domains__ = __webpack_require__(81);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__Domains__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__Standards__ = __webpack_require__(82);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "c", function() { return __WEBPACK_IMPORTED_MODULE_1__Standards__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__Targets__ = __webpack_require__(83);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "d", function() { return __WEBPACK_IMPORTED_MODULE_2__Targets__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__Entries__ = __webpack_require__(84);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "b", function() { return __WEBPACK_IMPORTED_MODULE_3__Entries__["a"]; });





/***/ }),
/* 81 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return withApollo; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_prop_types__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_react_apollo__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_react_apollo___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_react_apollo__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__graphql__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_antd__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_antd___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_antd__);
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

var REACT_ELEMENT_TYPE;

function _jsx(type, props, key, children) { if (!REACT_ELEMENT_TYPE) { REACT_ELEMENT_TYPE = typeof Symbol === "function" && Symbol.for && Symbol.for("react.element") || 0xeac7; } var defaultProps = type && type.defaultProps; var childrenLength = arguments.length - 3; if (!props && childrenLength !== 0) { props = {}; } if (props && defaultProps) { for (var propName in defaultProps) { if (props[propName] === void 0) { props[propName] = defaultProps[propName]; } } } else if (!props) { props = defaultProps || {}; } if (childrenLength === 1) { props.children = children; } else if (childrenLength > 1) { var childArray = new Array(childrenLength); for (var i = 0; i < childrenLength; i++) { childArray[i] = arguments[i + 3]; } props.children = childArray; } return { $$typeof: REACT_ELEMENT_TYPE, type: type, key: key === undefined ? null : '' + key, ref: null, props: props, _owner: null }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }


 // Data Layer


 // eslint-disable-line


var attributes = ['name', 'code'];
var columns = attributes.map(function (el) {
  return Object.assign({}, {
    title: el,
    dataIndex: el,
    rowKey: el
  });
});

var Settings_Domains =
/*#__PURE__*/
function (_Component) {
  _inherits(Settings_Domains, _Component);

  function Settings_Domains() {
    var _ref;

    var _temp, _this;

    _classCallCheck(this, Settings_Domains);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _possibleConstructorReturn(_this, (_temp = _this = _possibleConstructorReturn(this, (_ref = Settings_Domains.__proto__ || Object.getPrototypeOf(Settings_Domains)).call.apply(_ref, [this].concat(args))), Object.defineProperty(_assertThisInitialized(_this), "render", {
      configurable: true,
      enumerable: true,
      writable: true,
      value: function value() {
        var getAllDomains = _this.props.data.getAllDomains;
        return _jsx(__WEBPACK_IMPORTED_MODULE_0_react__["Fragment"], {}, void 0, getAllDomains ? _jsx(__WEBPACK_IMPORTED_MODULE_4_antd__["Table"], {
          dataSource: getAllDomains,
          columns: columns
        }) : 'loading');
      }
    }), _temp));
  }

  return Settings_Domains;
}(__WEBPACK_IMPORTED_MODULE_0_react__["Component"]);

var withApollo = Object(__WEBPACK_IMPORTED_MODULE_2_react_apollo__["compose"])(Object(__WEBPACK_IMPORTED_MODULE_2_react_apollo__["graphql"])(__WEBPACK_IMPORTED_MODULE_3__graphql__["b" /* queries */].domains_query, {
  options: {}
}))(Settings_Domains);


/***/ }),
/* 82 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return withApollo; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_prop_types__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_react_apollo__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_react_apollo___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_react_apollo__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__graphql__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_antd__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_antd___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_antd__);
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

var REACT_ELEMENT_TYPE;

function _jsx(type, props, key, children) { if (!REACT_ELEMENT_TYPE) { REACT_ELEMENT_TYPE = typeof Symbol === "function" && Symbol.for && Symbol.for("react.element") || 0xeac7; } var defaultProps = type && type.defaultProps; var childrenLength = arguments.length - 3; if (!props && childrenLength !== 0) { props = {}; } if (props && defaultProps) { for (var propName in defaultProps) { if (props[propName] === void 0) { props[propName] = defaultProps[propName]; } } } else if (!props) { props = defaultProps || {}; } if (childrenLength === 1) { props.children = children; } else if (childrenLength > 1) { var childArray = new Array(childrenLength); for (var i = 0; i < childrenLength; i++) { childArray[i] = arguments[i + 3]; } props.children = childArray; } return { $$typeof: REACT_ELEMENT_TYPE, type: type, key: key === undefined ? null : '' + key, ref: null, props: props, _owner: null }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }


 // Data Layer


 // eslint-disable-line


var attributes = ['name', 'code'];
var columns = attributes.map(function (el) {
  return Object.assign({}, {
    title: el,
    dataIndex: el,
    rowKey: el
  });
});

var Settings_Standards =
/*#__PURE__*/
function (_Component) {
  _inherits(Settings_Standards, _Component);

  function Settings_Standards() {
    var _ref;

    var _temp, _this;

    _classCallCheck(this, Settings_Standards);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _possibleConstructorReturn(_this, (_temp = _this = _possibleConstructorReturn(this, (_ref = Settings_Standards.__proto__ || Object.getPrototypeOf(Settings_Standards)).call.apply(_ref, [this].concat(args))), Object.defineProperty(_assertThisInitialized(_this), "render", {
      configurable: true,
      enumerable: true,
      writable: true,
      value: function value() {
        var getAllStandards = _this.props.data.getAllStandards;
        return _jsx(__WEBPACK_IMPORTED_MODULE_0_react__["Fragment"], {}, void 0, getAllStandards ? _jsx(__WEBPACK_IMPORTED_MODULE_4_antd__["Table"], {
          dataSource: getAllStandards,
          columns: columns
        }) : 'loading');
      }
    }), _temp));
  }

  return Settings_Standards;
}(__WEBPACK_IMPORTED_MODULE_0_react__["Component"]);

var withApollo = Object(__WEBPACK_IMPORTED_MODULE_2_react_apollo__["compose"])( // $FlowFixMe
Object(__WEBPACK_IMPORTED_MODULE_2_react_apollo__["graphql"])(__WEBPACK_IMPORTED_MODULE_3__graphql__["b" /* queries */].standards_query, {
  options: {}
}))(Settings_Standards);


/***/ }),
/* 83 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return withApollo; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_prop_types__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_react_apollo__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_react_apollo___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_react_apollo__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__graphql__ = __webpack_require__(7);
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

var REACT_ELEMENT_TYPE;

function _jsx(type, props, key, children) { if (!REACT_ELEMENT_TYPE) { REACT_ELEMENT_TYPE = typeof Symbol === "function" && Symbol.for && Symbol.for("react.element") || 0xeac7; } var defaultProps = type && type.defaultProps; var childrenLength = arguments.length - 3; if (!props && childrenLength !== 0) { props = {}; } if (props && defaultProps) { for (var propName in defaultProps) { if (props[propName] === void 0) { props[propName] = defaultProps[propName]; } } } else if (!props) { props = defaultProps || {}; } if (childrenLength === 1) { props.children = children; } else if (childrenLength > 1) { var childArray = new Array(childrenLength); for (var i = 0; i < childrenLength; i++) { childArray[i] = arguments[i + 3]; } props.children = childArray; } return { $$typeof: REACT_ELEMENT_TYPE, type: type, key: key === undefined ? null : '' + key, ref: null, props: props, _owner: null }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }


 // Data Layer


 // eslint-disable-line

var Settings_Targets =
/*#__PURE__*/
function (_Component) {
  _inherits(Settings_Targets, _Component);

  function Settings_Targets() {
    var _ref;

    var _temp, _this;

    _classCallCheck(this, Settings_Targets);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _possibleConstructorReturn(_this, (_temp = _this = _possibleConstructorReturn(this, (_ref = Settings_Targets.__proto__ || Object.getPrototypeOf(Settings_Targets)).call.apply(_ref, [this].concat(args))), Object.defineProperty(_assertThisInitialized(_this), "render", {
      configurable: true,
      enumerable: true,
      writable: true,
      value: function value() {
        var getAllTargets = _this.props.data.getAllTargets;
        return _jsx(__WEBPACK_IMPORTED_MODULE_0_react__["Fragment"], {}, void 0, getAllTargets ? JSON.stringify(getAllTargets) : 'loading');
      }
    }), _temp));
  }

  return Settings_Targets;
}(__WEBPACK_IMPORTED_MODULE_0_react__["Component"]);

var withApollo = Object(__WEBPACK_IMPORTED_MODULE_2_react_apollo__["compose"])( // $FlowFixMe
Object(__WEBPACK_IMPORTED_MODULE_2_react_apollo__["graphql"])(__WEBPACK_IMPORTED_MODULE_3__graphql__["b" /* queries */].targets_query, {
  options: {}
}))(Settings_Targets);


/***/ }),
/* 84 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return withApollo; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_prop_types__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_react_apollo__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_react_apollo___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_react_apollo__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__graphql__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_antd__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_antd___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_antd__);
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

var REACT_ELEMENT_TYPE;

function _jsx(type, props, key, children) { if (!REACT_ELEMENT_TYPE) { REACT_ELEMENT_TYPE = typeof Symbol === "function" && Symbol.for && Symbol.for("react.element") || 0xeac7; } var defaultProps = type && type.defaultProps; var childrenLength = arguments.length - 3; if (!props && childrenLength !== 0) { props = {}; } if (props && defaultProps) { for (var propName in defaultProps) { if (props[propName] === void 0) { props[propName] = defaultProps[propName]; } } } else if (!props) { props = defaultProps || {}; } if (childrenLength === 1) { props.children = children; } else if (childrenLength > 1) { var childArray = new Array(childrenLength); for (var i = 0; i < childrenLength; i++) { childArray[i] = arguments[i + 3]; } props.children = childArray; } return { $$typeof: REACT_ELEMENT_TYPE, type: type, key: key === undefined ? null : '' + key, ref: null, props: props, _owner: null }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }


 // Data Layer


 // eslint-disable-line


var attributes = ['name', 'code'];
var columns = attributes.map(function (el) {
  return Object.assign({}, {
    title: el,
    dataIndex: el,
    rowKey: el
  });
});

var Settings_Entries =
/*#__PURE__*/
function (_Component) {
  _inherits(Settings_Entries, _Component);

  function Settings_Entries() {
    var _ref;

    var _temp, _this;

    _classCallCheck(this, Settings_Entries);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _possibleConstructorReturn(_this, (_temp = _this = _possibleConstructorReturn(this, (_ref = Settings_Entries.__proto__ || Object.getPrototypeOf(Settings_Entries)).call.apply(_ref, [this].concat(args))), Object.defineProperty(_assertThisInitialized(_this), "render", {
      configurable: true,
      enumerable: true,
      writable: true,
      value: function value() {
        var getAllDomains = _this.props.data.getAllDomains;
        return _jsx(__WEBPACK_IMPORTED_MODULE_0_react__["Fragment"], {}, void 0, getAllDomains ? _jsx(__WEBPACK_IMPORTED_MODULE_4_antd__["Table"], {
          dataSource: getAllDomains,
          columns: columns
        }) : 'loading');
      }
    }), _temp));
  }

  return Settings_Entries;
}(__WEBPACK_IMPORTED_MODULE_0_react__["Component"]);

var withApollo = Object(__WEBPACK_IMPORTED_MODULE_2_react_apollo__["compose"])(Object(__WEBPACK_IMPORTED_MODULE_2_react_apollo__["graphql"])(__WEBPACK_IMPORTED_MODULE_3__graphql__["b" /* queries */].domains_query, {
  options: {}
}))(Settings_Entries);


/***/ }),
/* 85 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Styled; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_prop_types__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_antd__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_antd___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_antd__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_isomorphic_style_loader_lib_withStyles__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_isomorphic_style_loader_lib_withStyles___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_isomorphic_style_loader_lib_withStyles__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_theme_style_scss__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_theme_style_scss___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_theme_style_scss__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_react_redux__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_react_redux___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_react_redux__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_store_constants__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_semantic_ui_react__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_semantic_ui_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_semantic_ui_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__TableSelection__ = __webpack_require__(86);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ExportEntries__ = __webpack_require__(92);
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

var REACT_ELEMENT_TYPE;

function _jsx(type, props, key, children) { if (!REACT_ELEMENT_TYPE) { REACT_ELEMENT_TYPE = typeof Symbol === "function" && Symbol.for && Symbol.for("react.element") || 0xeac7; } var defaultProps = type && type.defaultProps; var childrenLength = arguments.length - 3; if (!props && childrenLength !== 0) { props = {}; } if (props && defaultProps) { for (var propName in defaultProps) { if (props[propName] === void 0) { props[propName] = defaultProps[propName]; } } } else if (!props) { props = defaultProps || {}; } if (childrenLength === 1) { props.children = children; } else if (childrenLength > 1) { var childArray = new Array(childrenLength); for (var i = 0; i < childrenLength; i++) { childArray[i] = arguments[i + 3]; } props.children = childArray; } return { $$typeof: REACT_ELEMENT_TYPE, type: type, key: key === undefined ? null : '' + key, ref: null, props: props, _owner: null }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }





 // redux







var _ref = _jsx(__WEBPACK_IMPORTED_MODULE_2_antd__["Breadcrumb"].Item, {}, void 0, _jsx(__WEBPACK_IMPORTED_MODULE_2_antd__["Icon"], {
  type: "user"
}), "\xA0My Selection");

var _ref2 = _jsx(__WEBPACK_IMPORTED_MODULE_2_antd__["Icon"], {
  type: "retweet"
});

var _ref3 = _jsx(__WEBPACK_IMPORTED_MODULE_8__TableSelection__["a" /* TableSelection */], {});

var _ref4 = _jsx(__WEBPACK_IMPORTED_MODULE_7_semantic_ui_react__["Divider"], {});

var _ref5 = _jsx(__WEBPACK_IMPORTED_MODULE_9__ExportEntries__["a" /* ExportEntries */], {});

var _ref6 = _jsx("br", {});

var UserSelection =
/*#__PURE__*/
function (_Component) {
  _inherits(UserSelection, _Component);

  function UserSelection(props) {
    var _this;

    _classCallCheck(this, UserSelection);

    _this = _possibleConstructorReturn(this, (UserSelection.__proto__ || Object.getPrototypeOf(UserSelection)).call(this, props));
    Object.defineProperty(_assertThisInitialized(_this), "resetEntriesSelection", {
      configurable: true,
      enumerable: true,
      writable: true,
      value: function value() {
        var resetSelection = _this.props.resetSelection;

        if (confirm('Do you really want to Reset of Metadata Entries?')) {
          resetSelection();
        }
      }
    });
    Object.defineProperty(_assertThisInitialized(_this), "render", {
      configurable: true,
      enumerable: true,
      writable: true,
      value: function value() {
        var user_entries = _this.props.user_entries;
        return user_entries && user_entries.length ? _jsx(__WEBPACK_IMPORTED_MODULE_2_antd__["Layout"], {
          className: __WEBPACK_IMPORTED_MODULE_4_theme_style_scss___default.a.user_selection,
          style: {
            padding: '0 24px 24px'
          }
        }, void 0, _jsx(__WEBPACK_IMPORTED_MODULE_2_antd__["Breadcrumb"], {
          style: {
            margin: '16px 0'
          }
        }, void 0, _ref, _jsx(__WEBPACK_IMPORTED_MODULE_2_antd__["Breadcrumb"].Item, {
          className: __WEBPACK_IMPORTED_MODULE_4_theme_style_scss___default.a.btn_breadcrumb,
          onClick: _this.resetEntriesSelection
        }, void 0, _ref2, "\xA0Reset selection")), _ref3, _ref4, _ref5, _ref6) : null;
      }
    });
    return _this;
  }

  return UserSelection;
}(__WEBPACK_IMPORTED_MODULE_0_react__["Component"]);

var mapStateToProps = function mapStateToProps(state) {
  return {
    // new_entry: { protocol: string } }) => ({
    // protocol: state.new_entry.protocol,
    user_entries: state.user_selection.user_entries
  };
}; // REDUX STORE


var mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return {
    // actions
    resetSelection: function resetSelection() {
      return dispatch({
        type: __WEBPACK_IMPORTED_MODULE_6_store_constants__["c" /* RESET_USER_SELECTION */]
      });
    }
  };
}; // MAPPING REDUX TO COMPONENT


var withRedux = Object(__WEBPACK_IMPORTED_MODULE_5_react_redux__["connect"])(mapStateToProps, mapDispatchToProps)(UserSelection);
var Styled = __WEBPACK_IMPORTED_MODULE_3_isomorphic_style_loader_lib_withStyles___default()(__WEBPACK_IMPORTED_MODULE_4_theme_style_scss___default.a)(withRedux);


/***/ }),
/* 86 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return withApollo; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_prop_types__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_react_redux__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_react_redux___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_react_redux__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_store_constants__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_antd__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_antd___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_antd__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__EditableCell__ = __webpack_require__(87);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__VersionControl__ = __webpack_require__(90);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__TargetZone__ = __webpack_require__(91);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_lodash__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_8_lodash__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_react_apollo__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_react_apollo___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_9_react_apollo__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__graphql__ = __webpack_require__(7);
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

var REACT_ELEMENT_TYPE;

function _jsx(type, props, key, children) { if (!REACT_ELEMENT_TYPE) { REACT_ELEMENT_TYPE = typeof Symbol === "function" && Symbol.for && Symbol.for("react.element") || 0xeac7; } var defaultProps = type && type.defaultProps; var childrenLength = arguments.length - 3; if (!props && childrenLength !== 0) { props = {}; } if (props && defaultProps) { for (var propName in defaultProps) { if (props[propName] === void 0) { props[propName] = defaultProps[propName]; } } } else if (!props) { props = defaultProps || {}; } if (childrenLength === 1) { props.children = children; } else if (childrenLength > 1) { var childArray = new Array(childrenLength); for (var i = 0; i < childrenLength; i++) { childArray[i] = arguments[i + 3]; } props.children = childArray; } return { $$typeof: REACT_ELEMENT_TYPE, type: type, key: key === undefined ? null : '' + key, ref: null, props: props, _owner: null }; }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }


 // redux







 // Data Layer


 // eslint-disable-line

var _ref = _jsx(__WEBPACK_IMPORTED_MODULE_4_antd__["Button"], {
  icon: "upload"
});

var _ref2 = _jsx(__WEBPACK_IMPORTED_MODULE_4_antd__["Button"], {
  icon: "delete"
});

var _ref3 = _jsx("span", {}, void 0, "Variables Column Fixed :");

var _ref4 = _jsx("br", {});

var _ref5 = _jsx("span", {}, void 0, "Actions Column Fixed :");

var _ref6 = _jsx("br", {});

var _ref7 = _jsx(__WEBPACK_IMPORTED_MODULE_6__VersionControl__["a" /* VersionControl */], {});

var _ref8 = _jsx(__WEBPACK_IMPORTED_MODULE_7__TargetZone__["a" /* TargetZone */], {});

var TableSelection =
/*#__PURE__*/
function (_Component) {
  _inherits(TableSelection, _Component);

  function TableSelection(props) {
    var _this;

    _classCallCheck(this, TableSelection);

    _this = _possibleConstructorReturn(this, (TableSelection.__proto__ || Object.getPrototypeOf(TableSelection)).call(this, props));
    Object.defineProperty(_assertThisInitialized(_this), "onChange", {
      configurable: true,
      enumerable: true,
      writable: true,
      value: function value(checked, switchSelected) {
        var fixedAction = checked ? 'right' : '';
        var fixedVariables = checked ? 'right' : '';

        _this.setState(_extends({}, switchSelected === 'action' ? {
          fixedAction: fixedAction
        } : {
          fixedVariables: fixedVariables
        }));
      }
    });
    Object.defineProperty(_assertThisInitialized(_this), "onCellChange", {
      configurable: true,
      enumerable: true,
      writable: true,
      value: function value(key, dataIndex) {
        var updateUserSelection = _this.props.updateUserSelection;
        return function (value) {
          var user_entries = _toConsumableArray(_this.props.user_entries);

          var target = user_entries.find(function (item) {
            return item.key === key;
          });

          if (target) {
            target[dataIndex] = value;
            updateUserSelection({
              user_entries: user_entries
            });
          }
        };
      }
    });
    Object.defineProperty(_assertThisInitialized(_this), "uploadRow", {
      configurable: true,
      enumerable: true,
      writable: true,
      value: function value(key) {
        var user_entries = _toConsumableArray(_this.props.user_entries);

        var entry = user_entries.find(function (item) {
          return item.key === key;
        });
        var _this$props = _this.props,
            commitNewEntryCreation = _this$props.commitNewEntryCreation,
            mutate = _this$props.mutate;
        var protocol = entry.protocol,
            domain = entry.domain,
            standard = entry.standard,
            sourcedata = entry.sourcedata,
            preprocessing = entry.preprocessing;
        var meta = {
          protocol: protocol,
          domain: domain,
          standard: standard,
          sourcedata: sourcedata,
          preprocessing: preprocessing // if (!(this.hasBeenAdded(entry))) {
          //   new Promise((resolve: any) => {
          //     resolve(addEntryToSelection({ entry }))
          //   }).then(()=> {
          //     message.success('A New Entry has been created')
          //   })
          // } else {
          //   message.error('Entry is already in your selection')
          // }

        };
        mutate({
          variables: {
            meta: meta
          } // refetchQueries: [{ query: queries.cooking_actions_query }]

        }).then(function (response) {
          var data = response.data;
          console.log('got data', data); // eslint-disable-line
        }).catch(function (error) {
          console.log('there was an error sending the query', error); // eslint-disable-line
        });
        new Promise(function (resolve) {
          resolve(commitNewEntryCreation());
        }).then(function () {
          __WEBPACK_IMPORTED_MODULE_4_antd__["message"].success('A New Entry has been persisted');
        });
      }
    });
    Object.defineProperty(_assertThisInitialized(_this), "deleteRow", {
      configurable: true,
      enumerable: true,
      writable: true,
      value: function value(key) {
        var updateUserSelection = _this.props.updateUserSelection;

        var user_entries = _toConsumableArray(_this.props.user_entries).filter(function (item) {
          return item.key !== key;
        });

        updateUserSelection({
          user_entries: user_entries
        });
      }
    });
    Object.defineProperty(_assertThisInitialized(_this), "render", {
      configurable: true,
      enumerable: true,
      writable: true,
      value: function value() {
        var user_entries = _this.props.user_entries;
        var _this$state = _this.state,
            fixedAction = _this$state.fixedAction,
            fixedVariables = _this$state.fixedVariables;
        var attributes = ['protocol', 'domain', 'standard'];
        var data = attributes.map(function (at) {
          return Object.assign({}, {
            title: __WEBPACK_IMPORTED_MODULE_8_lodash___default.a.capitalize(at),
            dataIndex: at,
            key: at,
            width: 250
          });
        });
        var sourcedata = [{
          title: 'Sourcedata',
          dataIndex: 'sourcedata',
          width: 200,
          render: function render(text, record) {
            return _jsx(__WEBPACK_IMPORTED_MODULE_5__EditableCell__["a" /* EditableCell */], {
              value: text,
              onChange: _this.onCellChange(record.key, 'sourcedata')
            });
          }
        }];
        var options = [{
          title: 'Options',
          dataIndex: 'options',
          width: 200,
          render: function render(text, record) {
            return _jsx(__WEBPACK_IMPORTED_MODULE_5__EditableCell__["a" /* EditableCell */], {
              value: text,
              onChange: _this.onCellChange(record.key, 'options')
            });
          }
        }];
        var exception = [{
          title: 'Exception',
          dataIndex: 'exception',
          width: 200,
          render: function render(text, record) {
            return _jsx(__WEBPACK_IMPORTED_MODULE_5__EditableCell__["a" /* EditableCell */], {
              value: text,
              onChange: _this.onCellChange(record.key, 'exception')
            });
          }
        }];
        var subset = [{
          title: 'Subset',
          dataIndex: 'subset',
          width: 250,
          render: function render(text, record) {
            return _jsx(__WEBPACK_IMPORTED_MODULE_5__EditableCell__["a" /* EditableCell */], {
              value: text,
              onChange: _this.onCellChange(record.key, 'subset')
            });
          }
        }];
        var variables = [{
          title: 'Variables',
          dataIndex: 'variables',
          fixed: fixedVariables,
          width: 200,
          render: function render(text, record) {
            return _jsx(__WEBPACK_IMPORTED_MODULE_0_react__["Fragment"], {}, void 0, _jsx(__WEBPACK_IMPORTED_MODULE_4_antd__["Button"], {
              icon: "phone",
              disabled: true,
              style: {
                float: 'left',
                display: 'inline'
              }
            }), _jsx(__WEBPACK_IMPORTED_MODULE_4_antd__["Button"], {
              icon: "info",
              disabled: true,
              style: {
                float: 'left',
                display: 'inline'
              }
            }), _jsx(__WEBPACK_IMPORTED_MODULE_5__EditableCell__["a" /* EditableCell */], {
              value: text,
              onChange: _this.onCellChange(record.key, 'variables'),
              style: {
                float: 'left',
                display: 'inline'
              }
            }));
          }
        }];
        var actions = [{
          title: 'Action',
          key: 'operation',
          fixed: fixedAction,
          width: 200,
          render: function render(record) {
            return _jsx(__WEBPACK_IMPORTED_MODULE_0_react__["Fragment"], {}, void 0, _jsx(__WEBPACK_IMPORTED_MODULE_4_antd__["Popconfirm"], {
              title: "Do you want to upload entry?",
              onConfirm: function onConfirm() {
                _this.uploadRow(record.key);
              }
            }, void 0, "\xA0", _ref), _jsx(__WEBPACK_IMPORTED_MODULE_4_antd__["Popconfirm"], {
              title: "Sure to delete?",
              onConfirm: function onConfirm() {
                _this.deleteRow(record.key);
              }
            }, void 0, "\xA0", _ref2));
          }
        }];

        var columns = _toConsumableArray(data).concat(sourcedata, options, exception, subset, variables, actions);

        return _jsx(__WEBPACK_IMPORTED_MODULE_0_react__["Fragment"], {}, void 0, _jsx("div", {
          style: {
            width: 400
          }
        }, void 0, _ref3, "\xA0", _jsx(__WEBPACK_IMPORTED_MODULE_4_antd__["Switch"], {
          defaultChecked: false,
          onChange: function onChange(checked) {
            return _this.onChange(checked, 'variables');
          }
        }), _ref4, _ref5, "\xA0", _jsx(__WEBPACK_IMPORTED_MODULE_4_antd__["Switch"], {
          defaultChecked: false,
          onChange: function onChange(checked) {
            return _this.onChange(checked, 'action');
          }
        })), _ref6, user_entries && user_entries.length > 0 ? _jsx(__WEBPACK_IMPORTED_MODULE_4_antd__["Table"], {
          dataSource: user_entries,
          columns: columns,
          bordered: true,
          size: "middle",
          scroll: {
            x: '130%'
          }
        }) : null, _ref7, _ref8);
      }
    });
    _this.state = {
      fixedAction: false,
      fixedVariables: false
    };
    return _this;
  }

  return TableSelection;
}(__WEBPACK_IMPORTED_MODULE_0_react__["Component"]);

var mapStateToProps = function mapStateToProps(state) {
  return {
    protocol: state.new_entry.protocol,
    user_entries: state.user_selection.user_entries
  };
}; // REDUX STORE


var mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return {
    // actions
    addEntryToSelection: function addEntryToSelection(_ref9) {
      var entry = _ref9.entry;
      return dispatch({
        type: __WEBPACK_IMPORTED_MODULE_3_store_constants__["a" /* ADD_ENTRY_TO_USER_SELECTION */],
        entry: entry
      });
    },
    updateUserSelection: function updateUserSelection(_ref10) {
      var user_entries = _ref10.user_entries;
      return dispatch({
        type: __WEBPACK_IMPORTED_MODULE_3_store_constants__["l" /* UPDATE_USER_SELECTION */],
        user_entries: user_entries
      });
    },
    commitNewEntryCreation: function commitNewEntryCreation() {
      return dispatch({
        type: __WEBPACK_IMPORTED_MODULE_3_store_constants__["b" /* COMMIT_CREATION_NEW_ENTRY */]
      });
    }
  };
}; // MAPPING REDUX TO COMPONENT


var withRedux = Object(__WEBPACK_IMPORTED_MODULE_2_react_redux__["connect"])(mapStateToProps, mapDispatchToProps)(TableSelection);
var withApollo = Object(__WEBPACK_IMPORTED_MODULE_9_react_apollo__["compose"])(Object(__WEBPACK_IMPORTED_MODULE_9_react_apollo__["graphql"])(__WEBPACK_IMPORTED_MODULE_10__graphql__["a" /* mutations */].new_entry_mm))(withRedux);


/***/ }),
/* 87 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Styled; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_antd__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_antd___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_antd__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_isomorphic_style_loader_lib_withStyles__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_isomorphic_style_loader_lib_withStyles___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_isomorphic_style_loader_lib_withStyles__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__style_scss__ = __webpack_require__(88);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__style_scss___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3__style_scss__);
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

var REACT_ELEMENT_TYPE;

function _jsx(type, props, key, children) { if (!REACT_ELEMENT_TYPE) { REACT_ELEMENT_TYPE = typeof Symbol === "function" && Symbol.for && Symbol.for("react.element") || 0xeac7; } var defaultProps = type && type.defaultProps; var childrenLength = arguments.length - 3; if (!props && childrenLength !== 0) { props = {}; } if (props && defaultProps) { for (var propName in defaultProps) { if (props[propName] === void 0) { props[propName] = defaultProps[propName]; } } } else if (!props) { props = defaultProps || {}; } if (childrenLength === 1) { props.children = children; } else if (childrenLength > 1) { var childArray = new Array(childrenLength); for (var i = 0; i < childrenLength; i++) { childArray[i] = arguments[i + 3]; } props.children = childArray; } return { $$typeof: REACT_ELEMENT_TYPE, type: type, key: key === undefined ? null : '' + key, ref: null, props: props, _owner: null }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }






var EditableCell =
/*#__PURE__*/
function (_React$Component) {
  _inherits(EditableCell, _React$Component);

  function EditableCell() {
    var _ref;

    var _temp, _this;

    _classCallCheck(this, EditableCell);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _possibleConstructorReturn(_this, (_temp = _this = _possibleConstructorReturn(this, (_ref = EditableCell.__proto__ || Object.getPrototypeOf(EditableCell)).call.apply(_ref, [this].concat(args))), Object.defineProperty(_assertThisInitialized(_this), "state", {
      configurable: true,
      enumerable: true,
      writable: true,
      value: {
        value: _this.props.value,
        editable: false
      }
    }), Object.defineProperty(_assertThisInitialized(_this), "handleChange", {
      configurable: true,
      enumerable: true,
      writable: true,
      value: function value(e) {
        var value = e.target.value;

        _this.setState({
          value: value
        });
      }
    }), Object.defineProperty(_assertThisInitialized(_this), "check", {
      configurable: true,
      enumerable: true,
      writable: true,
      value: function value() {
        _this.setState({
          editable: false
        });

        if (_this.props.onChange) {
          _this.props.onChange(_this.state.value);
        }
      }
    }), Object.defineProperty(_assertThisInitialized(_this), "edit", {
      configurable: true,
      enumerable: true,
      writable: true,
      value: function value() {
        _this.setState({
          editable: true
        });
      }
    }), _temp));
  }

  _createClass(EditableCell, [{
    key: "render",
    value: function render() {
      var _state = this.state,
          value = _state.value,
          editable = _state.editable;
      return _jsx("div", {
        className: "editable-cell"
      }, void 0, editable ? _jsx("div", {
        className: "editable-cell-input-wrapper"
      }, void 0, _jsx(__WEBPACK_IMPORTED_MODULE_1_antd__["Input"], {
        value: value,
        onChange: this.handleChange,
        onPressEnter: this.check
      }), _jsx(__WEBPACK_IMPORTED_MODULE_1_antd__["Icon"], {
        type: "check",
        className: "editable-cell-icon-check",
        onClick: this.check
      })) : _jsx("div", {
        className: "editable-cell-text-wrapper"
      }, void 0, value || ' ', _jsx(__WEBPACK_IMPORTED_MODULE_1_antd__["Button"], {
        icon: "edit",
        onClick: this.edit,
        style: {
          float: 'left',
          display: 'inline'
        }
      })));
    }
  }]);

  return EditableCell;
}(__WEBPACK_IMPORTED_MODULE_0_react___default.a.Component);

var Styled = __WEBPACK_IMPORTED_MODULE_2_isomorphic_style_loader_lib_withStyles___default()(__WEBPACK_IMPORTED_MODULE_3__style_scss___default.a)(EditableCell);


/***/ }),
/* 88 */
/***/ (function(module, exports, __webpack_require__) {


    var content = __webpack_require__(89);
    var insertCss = __webpack_require__(10);

    if (typeof content === 'string') {
      content = [[module.i, content, '']];
    }

    module.exports = content.locals || {};
    module.exports._getContent = function() { return content; };
    module.exports._getCss = function() { return content.toString(); };
    module.exports._insertCss = function(options) { return insertCss(content, options) };
    
    // Hot Module Replacement
    // https://webpack.github.io/docs/hot-module-replacement
    // Only activated in browser context
    if (false) {
      var removeCss = function() {};
      module.hot.accept("!!../../../../../node_modules/css-loader/index.js??ref--2-rules-2!../../../../../node_modules/postcss-loader/lib/index.js??ref--2-rules-3!./style.scss", function() {
        content = require("!!../../../../../node_modules/css-loader/index.js??ref--2-rules-2!../../../../../node_modules/postcss-loader/lib/index.js??ref--2-rules-3!./style.scss");

        if (typeof content === 'string') {
          content = [[module.id, content, '']];
        }

        removeCss = insertCss(content, { replace: true });
      });
      module.hot.dispose(function() { removeCss(); });
    }
  

/***/ }),
/* 89 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(9)(false);
// imports


// module
exports.push([module.i, ".aHI-R{position:relative}._3x2gD,.zy9ZP{padding-right:24px}._3x2gD{padding:5px 24px 5px 5px}._1N9qK,._2fKlL{position:absolute;right:0;width:20px;cursor:pointer}._2fKlL{line-height:18px;display:none}._1N9qK{line-height:28px}.aHI-R:hover ._2fKlL{display:inline-block}._1N9qK:hover,._2fKlL:hover{color:#108ee9}._11yDb{margin-bottom:8px}", ""]);

// exports
exports.locals = {
	"editable-cell": "aHI-R",
	"editable-cell-input-wrapper": "zy9ZP",
	"editable-cell-text-wrapper": "_3x2gD",
	"editable-cell-icon": "_2fKlL",
	"editable-cell-icon-check": "_1N9qK",
	"editable-add-btn": "_11yDb"
};

/***/ }),
/* 90 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return withRedux; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_prop_types__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_antd__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_antd___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_antd__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_react_redux__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_react_redux___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_react_redux__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_store_constants__ = __webpack_require__(5);
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

var REACT_ELEMENT_TYPE;

function _jsx(type, props, key, children) { if (!REACT_ELEMENT_TYPE) { REACT_ELEMENT_TYPE = typeof Symbol === "function" && Symbol.for && Symbol.for("react.element") || 0xeac7; } var defaultProps = type && type.defaultProps; var childrenLength = arguments.length - 3; if (!props && childrenLength !== 0) { props = {}; } if (props && defaultProps) { for (var propName in defaultProps) { if (props[propName] === void 0) { props[propName] = defaultProps[propName]; } } } else if (!props) { props = defaultProps || {}; } if (childrenLength === 1) { props.children = children; } else if (childrenLength > 1) { var childArray = new Array(childrenLength); for (var i = 0; i < childrenLength; i++) { childArray[i] = arguments[i + 3]; } props.children = childArray; } return { $$typeof: REACT_ELEMENT_TYPE, type: type, key: key === undefined ? null : '' + key, ref: null, props: props, _owner: null }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }



 // redux




var VersionControl =
/*#__PURE__*/
function (_Component) {
  _inherits(VersionControl, _Component);

  function VersionControl(props) {
    var _this;

    _classCallCheck(this, VersionControl);

    _this = _possibleConstructorReturn(this, (VersionControl.__proto__ || Object.getPrototypeOf(VersionControl)).call(this, props));
    Object.defineProperty(_assertThisInitialized(_this), "emitEmpty", {
      configurable: true,
      enumerable: true,
      writable: true,
      value: function value() {
        _this.VersionControlFileInput.focus();

        var updateVCS = _this.props.updateVCS;
        updateVCS({
          vcs: ''
        });
      }
    });
    Object.defineProperty(_assertThisInitialized(_this), "onChangeVersionControlFile", {
      configurable: true,
      enumerable: true,
      writable: true,
      value: function value(e) {
        var updateVCS = _this.props.updateVCS;
        updateVCS({
          vcs: e.target.value
        });
      }
    });
    Object.defineProperty(_assertThisInitialized(_this), "render", {
      configurable: true,
      enumerable: true,
      writable: true,
      value: function value() {
        var vcs = _this.props.vcs;
        var suffix = vcs ? _jsx(__WEBPACK_IMPORTED_MODULE_2_antd__["Icon"], {
          type: "close-circle",
          onClick: _this.emitEmpty
        }) : null;
        return _jsx(__WEBPACK_IMPORTED_MODULE_0_react__["Fragment"], {}, void 0, _jsx("span", {
          style: {
            textDecoration: 'underline'
          }
        }, void 0, "Version Control File : "), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_2_antd__["Input"], {
          placeholder: "View Configuration Specifications",
          prefix: _jsx(__WEBPACK_IMPORTED_MODULE_2_antd__["Icon"], {
            type: "setting",
            style: {
              color: 'rgba(0,0,0,.25)'
            }
          }),
          suffix: suffix,
          value: vcs,
          onChange: _this.onChangeVersionControlFile,
          ref: function ref(node) {
            return _this.VersionControlFileInput = node;
          },
          style: {
            marginBottom: '16px',
            maxWidth: '350px'
          }
        }));
      }
    });
    return _this;
  }

  return VersionControl;
}(__WEBPACK_IMPORTED_MODULE_0_react__["Component"]);

var mapStateToProps = function mapStateToProps(state) {
  return {
    // new_entry: { protocol: string } }) => ({
    // protocol: state.new_entry.protocol,
    user_entries: state.user_selection.user_entries,
    vcs: state.user_selection.vcs
  };
}; // REDUX STORE


var mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return {
    // actions
    updateVCS: function updateVCS(_ref) {
      var vcs = _ref.vcs;
      return dispatch({
        type: __WEBPACK_IMPORTED_MODULE_4_store_constants__["f" /* UPDATE_OUTPUT_VCS */],
        vcs: vcs
      });
    }
  };
}; // MAPPING REDUX TO COMPONENT


var withRedux = Object(__WEBPACK_IMPORTED_MODULE_3_react_redux__["connect"])(mapStateToProps, mapDispatchToProps)(VersionControl);


/***/ }),
/* 91 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return withApollo; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_prop_types__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_semantic_ui_react__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_semantic_ui_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_semantic_ui_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_react_redux__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_react_redux___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_react_redux__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_store_constants__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_react_apollo__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_react_apollo___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_react_apollo__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__graphql__ = __webpack_require__(7);
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

var REACT_ELEMENT_TYPE;

function _jsx(type, props, key, children) { if (!REACT_ELEMENT_TYPE) { REACT_ELEMENT_TYPE = typeof Symbol === "function" && Symbol.for && Symbol.for("react.element") || 0xeac7; } var defaultProps = type && type.defaultProps; var childrenLength = arguments.length - 3; if (!props && childrenLength !== 0) { props = {}; } if (props && defaultProps) { for (var propName in defaultProps) { if (props[propName] === void 0) { props[propName] = defaultProps[propName]; } } } else if (!props) { props = defaultProps || {}; } if (childrenLength === 1) { props.children = children; } else if (childrenLength > 1) { var childArray = new Array(childrenLength); for (var i = 0; i < childrenLength; i++) { childArray[i] = arguments[i + 3]; } props.children = childArray; } return { $$typeof: REACT_ELEMENT_TYPE, type: type, key: key === undefined ? null : '' + key, ref: null, props: props, _owner: null }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }



 // redux


 // Data Layer


 // eslint-disable-line

var TargetZone =
/*#__PURE__*/
function (_Component) {
  _inherits(TargetZone, _Component);

  function TargetZone() {
    var _ref;

    var _temp, _this;

    _classCallCheck(this, TargetZone);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _possibleConstructorReturn(_this, (_temp = _this = _possibleConstructorReturn(this, (_ref = TargetZone.__proto__ || Object.getPrototypeOf(TargetZone)).call.apply(_ref, [this].concat(args))), Object.defineProperty(_assertThisInitialized(_this), "render", {
      configurable: true,
      enumerable: true,
      writable: true,
      value: function value() {
        var _this$props = _this.props,
            getAllTargets = _this$props.data.getAllTargets,
            target = _this$props.target;
        var options = getAllTargets ? [{
          key: 'empty',
          text: 'Select a Target',
          value: ''
        }].concat(getAllTargets.map(function (el) {
          return Object.assign({}, {
            key: el,
            text: el,
            value: el
          });
        })) : [];
        return _jsx(__WEBPACK_IMPORTED_MODULE_0_react__["Fragment"], {}, void 0, _jsx("span", {
          style: {
            textDecoration: 'underline'
          }
        }, void 0, " Target: "), _jsx("div", {
          style: {
            maxWidth: '300px'
          }
        }, void 0, _jsx(__WEBPACK_IMPORTED_MODULE_2_semantic_ui_react__["Dropdown"], {
          upward: true,
          placeholder: "Select a Target",
          search: true,
          selection: true,
          options: options,
          onChange: function onChange(e, _ref2) {
            var value = _ref2.value;
            return _this.props.updateTarget({
              target: value
            });
          },
          value: target
        })));
      }
    }), _temp));
  }

  return TargetZone;
}(__WEBPACK_IMPORTED_MODULE_0_react__["Component"]);

var mapStateToProps = function mapStateToProps(state) {
  return {
    // new_entry: { protocol: string } }) => ({
    // protocol: state.new_entry.protocol,
    user_entries: state.user_selection.user_entries,
    target: state.user_selection.target
  };
}; // REDUX STORE


var mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return {
    // actions
    updateTarget: function updateTarget(_ref3) {
      var target = _ref3.target;
      return dispatch({
        type: __WEBPACK_IMPORTED_MODULE_4_store_constants__["e" /* UPDATE_OUTPUT_TARGET */],
        target: target
      });
    }
  };
}; // MAPPING REDUX TO COMPONENT


var withRedux = Object(__WEBPACK_IMPORTED_MODULE_3_react_redux__["connect"])(mapStateToProps, mapDispatchToProps)(TargetZone);
var withApollo = Object(__WEBPACK_IMPORTED_MODULE_5_react_apollo__["compose"])(Object(__WEBPACK_IMPORTED_MODULE_5_react_apollo__["graphql"])(__WEBPACK_IMPORTED_MODULE_6__graphql__["b" /* queries */].targets_query))(withRedux);


/***/ }),
/* 92 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return withRedux; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_antd__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_antd___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_antd__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_react_redux__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_react_redux___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_react_redux__);
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

var REACT_ELEMENT_TYPE;

function _jsx(type, props, key, children) { if (!REACT_ELEMENT_TYPE) { REACT_ELEMENT_TYPE = typeof Symbol === "function" && Symbol.for && Symbol.for("react.element") || 0xeac7; } var defaultProps = type && type.defaultProps; var childrenLength = arguments.length - 3; if (!props && childrenLength !== 0) { props = {}; } if (props && defaultProps) { for (var propName in defaultProps) { if (props[propName] === void 0) { props[propName] = defaultProps[propName]; } } } else if (!props) { props = defaultProps || {}; } if (childrenLength === 1) { props.children = children; } else if (childrenLength > 1) { var childArray = new Array(childrenLength); for (var i = 0; i < childrenLength; i++) { childArray[i] = arguments[i + 3]; } props.children = childArray; } return { $$typeof: REACT_ELEMENT_TYPE, type: type, key: key === undefined ? null : '' + key, ref: null, props: props, _owner: null }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }


 // redux



var ExportEntries =
/*#__PURE__*/
function (_Component) {
  _inherits(ExportEntries, _Component);

  function ExportEntries() {
    var _ref;

    var _temp, _this;

    _classCallCheck(this, ExportEntries);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _possibleConstructorReturn(_this, (_temp = _this = _possibleConstructorReturn(this, (_ref = ExportEntries.__proto__ || Object.getPrototypeOf(ExportEntries)).call.apply(_ref, [this].concat(args))), Object.defineProperty(_assertThisInitialized(_this), "downloadCSV", {
      configurable: true,
      enumerable: true,
      writable: true,
      value: function value() {
        var _this$props = _this.props,
            user_entries = _this$props.user_entries,
            vcs = _this$props.vcs,
            target = _this$props.target; // console.log(user_entries)
        // console.log(vcs)
        // console.log(target)

        var separator = ';';
        var linebreak = '\n';
        var csvFileMM = ''; //Initialization

        var vcsRow = {
          type: 'VCS',
          sourcedata: vcs
        };
        var targetRow = {
          type: 'TARGET',
          standard: target
        };
        csvFileMM += 'TYPE' + separator;
        csvFileMM += 'STANDARD' + separator;
        csvFileMM += 'PROTOCOL' + separator;
        csvFileMM += 'DOMAIN' + separator;
        csvFileMM += 'OPTIONS' + separator;
        csvFileMM += 'EXCEPTION' + separator;
        csvFileMM += 'SOURCEDATA' + separator;
        csvFileMM += 'SUBSET' + separator;
        csvFileMM += 'VARIABLES' + separator;
        csvFileMM += linebreak; //VCS

        if (vcsRow.sourcedata !== undefined && vcsRow.sourcedata.length > 0) {
          csvFileMM += vcsRow.type + separator;
          csvFileMM += vcsRow.standard !== undefined ? vcsRow.standard + separator : separator;
          csvFileMM += vcsRow.protocol !== undefined ? vcsRow.protocol + separator : separator;
          csvFileMM += vcsRow.options !== undefined ? vcsRow.options + separator : separator;
          csvFileMM += vcsRow.exception !== undefined ? vcsRow.exception + separator : separator;
          csvFileMM += vcsRow.domain !== undefined ? vcsRow.domain + separator : separator;
          csvFileMM += vcsRow.sourcedata !== undefined ? vcsRow.sourcedata + separator : separator;
          csvFileMM += vcsRow.subset !== undefined ? vcsRow.subset + separator : separator;
          csvFileMM += vcsRow.variables !== undefined ? vcsRow.variables + separator : separator;
          csvFileMM += linebreak;
        } //METADATA


        for (var i = 0; i < user_entries.length; i++) {
          var variablesCSV = '';

          if (Array.isArray(user_entries[i].variables)) {
            user_entries[i].variables.map(function (variable) {
              return variablesCSV += variable + ' ';
            });
          } else {
            variablesCSV = user_entries[i].variables;
          }

          variablesCSV = variablesCSV ? variablesCSV.trim() : '';
          csvFileMM += 'DATA' + separator;
          csvFileMM += user_entries[i].standard !== undefined ? user_entries[i].standard + separator : separator;
          csvFileMM += user_entries[i].protocol !== undefined ? user_entries[i].protocol + separator : separator;
          csvFileMM += user_entries[i].options !== undefined ? user_entries[i].options + separator : separator;
          csvFileMM += user_entries[i].exception !== undefined ? user_entries[i].exception + separator : separator;
          csvFileMM += user_entries[i].domain !== undefined ? user_entries[i].domain + separator : separator;
          csvFileMM += user_entries[i].sourcedata !== undefined ? user_entries[i].sourcedata + separator : separator;
          csvFileMM += user_entries[i].subset !== undefined ? user_entries[i].subset + separator : separator;
          csvFileMM += user_entries[i].variables !== undefined ? variablesCSV + separator : separator;
          csvFileMM += linebreak;
        } //


        var valueVariables = '';
        var separatorVar = ' ';

        if (targetRow.variables !== undefined) {
          if (Array.isArray(targetRow.variables)) {
            for (var j = 0; j < targetRow.variables.length; j++) {
              valueVariables += j !== targetRow.variables.length - 1 ? targetRow.variables[j] + separatorVar : targetRow.variables[j];
            }
          } else {
            valueVariables += targetRow.variables;
          }
        } //console.log(valueVariables)
        //TARGET


        if (targetRow.standard !== undefined && targetRow.standard.length > 0) {
          csvFileMM += targetRow.type + separator;
          csvFileMM += targetRow.standard !== undefined ? targetRow.standard + separator : separator;
          csvFileMM += targetRow.protocol !== undefined ? targetRow.protocol + separator : separator;
          csvFileMM += targetRow.options !== undefined ? targetRow.options + separator : separator;
          csvFileMM += targetRow.exception !== undefined ? targetRow.exception + separator : separator;
          csvFileMM += targetRow.domain !== undefined ? targetRow.domain + separator : separator;
          csvFileMM += targetRow.sourcedata !== undefined ? targetRow.sourcedata + separator : separator;
          csvFileMM += targetRow.subset !== undefined ? targetRow.subset + separator : separator;
          csvFileMM += targetRow.variables !== undefined ? valueVariables + separator : separator;
          csvFileMM += linebreak;
        }

        _this.download(csvFileMM, 'mm-' + Date.now() + '.txt', 'text/csv');
      }
    }), Object.defineProperty(_assertThisInitialized(_this), "download", {
      configurable: true,
      enumerable: true,
      writable: true,
      value: function value(content, fileName, mimeType) {
        var a = document.createElement('a');
        mimeType = mimeType || 'application/octet-stream';

        if (navigator.msSaveBlob) {
          // IE10
          return navigator.msSaveBlob(new Blob([content], {
            type: mimeType
          }), fileName);
        } else if ('download' in a) {
          //html5 A[download]
          a.href = 'data:' + mimeType + ',' + encodeURIComponent(content);
          a.setAttribute('download', fileName);
          document.body.appendChild(a);
          setTimeout(function () {
            a.click();
            document.body.removeChild(a);
          }, 66);
          return true;
        } else {
          //do iframe dataURL download (old ch+FF):
          var f = document.createElement('iframe');
          document.body.appendChild(f);
          f.src = 'data:' + mimeType + ',' + encodeURIComponent(content);
          setTimeout(function () {
            document.body.removeChild(f);
          }, 333);
          return true;
        }
      }
    }), Object.defineProperty(_assertThisInitialized(_this), "render", {
      configurable: true,
      enumerable: true,
      writable: true,
      value: function value() {
        return _jsx(__WEBPACK_IMPORTED_MODULE_0_react__["Fragment"], {}, void 0, _jsx("div", {
          style: {
            maxWidth: '500'
          }
        }, void 0, _jsx(__WEBPACK_IMPORTED_MODULE_1_antd__["Button"], {
          primary: "true",
          icon: "download",
          onClick: _this.downloadCSV
        }, void 0, "Download")));
      }
    }), _temp));
  }

  return ExportEntries;
}(__WEBPACK_IMPORTED_MODULE_0_react__["Component"]);

var mapStateToProps = function mapStateToProps(state) {
  return {
    user_entries: state.user_selection.user_entries,
    vcs: state.user_selection.vcs,
    target: state.user_selection.target
  };
}; // REDUX STORE
// const mapDispatchToProps = (dispatch: Function) => {
//   return {
//     // actions
//     updateVCS: ({ vcs }: { vcs: string }) => dispatch({ type : UPDATE_OUTPUT_VCS, vcs })
//   }
// }
// MAPPING REDUX TO COMPONENT


var withRedux = Object(__WEBPACK_IMPORTED_MODULE_2_react_redux__["connect"])(mapStateToProps, null)(ExportEntries);


/***/ }),
/* 93 */
/***/ (function(module, exports) {

module.exports = require("./assets.json");

/***/ }),
/* 94 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (immutable) */ __webpack_exports__["a"] = configureStore;
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_redux__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_redux___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_redux__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__reducers__ = __webpack_require__(95);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__logger__ = __webpack_require__(98);
/* global process __DEV__ */



function configureStore(initialState) {
  var middleware = [];
  var enhancer;

  if (false) {
    middleware.push(createLogger()); // https://github.com/zalmoxisus/redux-devtools-extension#redux-devtools-extension

    var devToolsExtension = function devToolsExtension(f) {
      return f;
    };

    if (process.env.BROWSER && window.devToolsExtension) {
      devToolsExtension = window.devToolsExtension();
    }

    enhancer = compose(applyMiddleware.apply(void 0, middleware), devToolsExtension);
  } else {
    enhancer = __WEBPACK_IMPORTED_MODULE_0_redux__["applyMiddleware"].apply(void 0, middleware);
  }

  var rootReducer = Object(__WEBPACK_IMPORTED_MODULE_1__reducers__["a" /* default */])(); // See https://github.com/reactjs/redux/releases/tag/v3.1.0

  var store = Object(__WEBPACK_IMPORTED_MODULE_0_redux__["createStore"])(rootReducer, initialState, enhancer); // Hot reload reducers (requires Webpack or Browserify HMR to be enabled)

  if (false) {
    module.hot.accept('./reducers', function () {
      return (// eslint-disable-next-line global-require
        store.replaceReducer(require('./reducers').default)
      );
    });
  }

  return store;
}

/***/ }),
/* 95 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (immutable) */ __webpack_exports__["a"] = createRootReducer;
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_redux__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_redux___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_redux__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__newEntry___ = __webpack_require__(96);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__userSelection___ = __webpack_require__(97);
 // import user from './user'
// import runtime from './runtime'



function createRootReducer() {
  return Object(__WEBPACK_IMPORTED_MODULE_0_redux__["combineReducers"])({
    // user,
    // runtime,
    new_entry: __WEBPACK_IMPORTED_MODULE_1__newEntry___["a" /* new_entry */],
    user_selection: __WEBPACK_IMPORTED_MODULE_2__userSelection___["a" /* user_selection */]
  });
}

/***/ }),
/* 96 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return update; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_store_constants__ = __webpack_require__(5);
function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }


// const REFRESH_STATE = {
//   refresh_form: 0
// }
var INIT_FIELDS = {
  protocol: '',
  preprocessing: false,
  sourcedata: '',
  domain: '',
  standard: '',
  yp_sourcedata: []
};

var INIT_STATE = _extends({}, INIT_FIELDS);

var update = function update() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : INIT_STATE;
  var action = arguments.length > 1 ? arguments[1] : undefined;
  var type = action.type;

  if (type === __WEBPACK_IMPORTED_MODULE_0_store_constants__["h" /* UPDATE_PROTOCOL_NEW_ENTRY */]) {
    var protocol = action.protocol;
    return _extends({}, state, {
      protocol: protocol
    });
  } else if (type === __WEBPACK_IMPORTED_MODULE_0_store_constants__["g" /* UPDATE_PREPROCESSING_NEW_ENTRY */]) {
    var preprocessing = action.preprocessing;
    return _extends({}, state, {
      preprocessing: preprocessing
    }, {
      sourcedata: preprocessing === true ? '__PRE__' : state.sourcedata
    });
  } else if (type === __WEBPACK_IMPORTED_MODULE_0_store_constants__["j" /* UPDATE_SOURCEDATA_NEW_ENTRY */]) {
    var sourcedata = action.sourcedata;
    return _extends({}, state, {
      sourcedata: sourcedata
    });
  } else if (type === __WEBPACK_IMPORTED_MODULE_0_store_constants__["d" /* UPDATE_DOMAIN_NEW_ENTRY */]) {
    var domain = action.domain;
    return _extends({}, state, {
      domain: domain
    });
  } else if (type === __WEBPACK_IMPORTED_MODULE_0_store_constants__["k" /* UPDATE_STANDARD_NEW_ENTRY */]) {
    var standard = action.standard;
    return _extends({}, state, {
      standard: standard
    });
  } else if (type === __WEBPACK_IMPORTED_MODULE_0_store_constants__["b" /* COMMIT_CREATION_NEW_ENTRY */]) {
    return _extends({}, state, INIT_FIELDS);
  } else if (type === __WEBPACK_IMPORTED_MODULE_0_store_constants__["i" /* UPDATE_SOURCEDATA_FROM_API_NEW_ENTRY */]) {
    var yp_sourcedata = action.yp_sourcedata;
    return _extends({}, state, {
      yp_sourcedata: yp_sourcedata
    });
  }

  return state;
};



/***/ }),
/* 97 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return update; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_store_constants__ = __webpack_require__(5);
function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }


var REFRESH_STATE = {
  refresh_form: 0
};
var INIT_FIELDS = {
  user_entries: [],
  target: '',
  vcs: ''
};

var INIT_STATE = _extends({}, INIT_FIELDS, REFRESH_STATE);

var update = function update() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : INIT_STATE;
  var action = arguments.length > 1 ? arguments[1] : undefined;
  var type = action.type;

  if (type === __WEBPACK_IMPORTED_MODULE_0_store_constants__["a" /* ADD_ENTRY_TO_USER_SELECTION */]) {
    var entry = action.entry;
    var user_entries = state.user_entries;

    var entry_with_key = _extends({
      key: user_entries.length
    }, entry);

    return _extends({}, state, {
      user_entries: _toConsumableArray(user_entries).concat([entry_with_key]),
      refresh_form: state.refresh_form + 1
    });
  } else if (type === __WEBPACK_IMPORTED_MODULE_0_store_constants__["l" /* UPDATE_USER_SELECTION */]) {
    var _user_entries = action.user_entries;
    return _extends({}, state, {
      user_entries: _user_entries
    });
  } else if (type === __WEBPACK_IMPORTED_MODULE_0_store_constants__["c" /* RESET_USER_SELECTION */]) {
    return _extends({}, state, {
      user_entries: []
    });
  } else if (type === __WEBPACK_IMPORTED_MODULE_0_store_constants__["e" /* UPDATE_OUTPUT_TARGET */]) {
    var target = action.target;
    return _extends({}, state, {
      target: target
    });
  } else if (type === __WEBPACK_IMPORTED_MODULE_0_store_constants__["f" /* UPDATE_OUTPUT_VCS */]) {
    var vcs = action.vcs;
    return _extends({}, state, {
      vcs: vcs
    });
  }

  return state;
};



/***/ }),
/* 98 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export createLogger */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_util__ = __webpack_require__(99);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_util___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_util__);


var inspectObject = function inspectObject(object) {
  return Object(__WEBPACK_IMPORTED_MODULE_0_util__["inspect"])(object, {
    colors: true
  });
};

var singleLine = function singleLine(str) {
  return str.replace(/\s+/g, ' ');
};

var actionFormatters = {
  // This is used at feature/apollo branch, but it can help you when implementing Apollo
  APOLLO_QUERY_INIT: function APOLLO_QUERY_INIT(a) {
    return "queryId:".concat(a.queryId, " variables:").concat(inspectObject(a.variables), "\n   ").concat(singleLine(a.queryString));
  },
  APOLLO_QUERY_RESULT: function APOLLO_QUERY_RESULT(a) {
    return "queryId:".concat(a.queryId, "\n   ").concat(singleLine(inspectObject(a.result)));
  },
  APOLLO_QUERY_STOP: function APOLLO_QUERY_STOP(a) {
    return "queryId:".concat(a.queryId);
  } // Server side redux action logger

};

var createLogger = function createLogger() {
  // eslint-disable-next-line no-unused-vars
  return function (store) {
    return function (next) {
      return function (action) {
        var formattedPayload = '';
        var actionFormatter = actionFormatters[action.type];

        if (typeof actionFormatter === 'function') {
          formattedPayload = actionFormatter(action);
        } else if (action.toString !== Object.prototype.toString) {
          formattedPayload = action.toString();
        } else if (typeof action.payload !== 'undefined') {
          formattedPayload = inspectObject(action.payload);
        } else {
          formattedPayload = inspectObject(action);
        }

        console.log(" * ".concat(action.type, ": ").concat(formattedPayload)); // eslint-disable-line no-console

        return next(action);
      };
    };
  };
};



/***/ }),
/* 99 */
/***/ (function(module, exports) {

module.exports = require("util");

/***/ }),
/* 100 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return schema; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_graphql_tools__ = __webpack_require__(101);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_graphql_tools___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_graphql_tools__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__resolvers__ = __webpack_require__(102);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__schema__ = __webpack_require__(113);



var resolvers = {
  Query: __WEBPACK_IMPORTED_MODULE_1__resolvers__["b" /* Query */],
  Mutation: __WEBPACK_IMPORTED_MODULE_1__resolvers__["a" /* Mutation */]
};
var schema = Object(__WEBPACK_IMPORTED_MODULE_0_graphql_tools__["makeExecutableSchema"])({
  typeDefs: __WEBPACK_IMPORTED_MODULE_2__schema__["a" /* default */],
  resolvers: resolvers
});


/***/ }),
/* 101 */
/***/ (function(module, exports) {

module.exports = require("graphql-tools");

/***/ }),
/* 102 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__queries__ = __webpack_require__(103);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__mutations__ = __webpack_require__(111);
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return __WEBPACK_IMPORTED_MODULE_0__queries__["a"]; });
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_1__mutations__["a"]; });




/***/ }),
/* 103 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__getAllDomains__ = __webpack_require__(104);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__getAllStandards__ = __webpack_require__(105);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__getAllTargets__ = __webpack_require__(106);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__getYPProtocols__ = __webpack_require__(107);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__getYPSourcedata__ = __webpack_require__(109);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__getAllEntries__ = __webpack_require__(110);






/* harmony default export */ __webpack_exports__["a"] = ({
  getAllDomains: __WEBPACK_IMPORTED_MODULE_0__getAllDomains__["a" /* getAllDomains */],
  getAllStandards: __WEBPACK_IMPORTED_MODULE_1__getAllStandards__["a" /* getAllStandards */],
  getAllTargets: __WEBPACK_IMPORTED_MODULE_2__getAllTargets__["a" /* getAllTargets */],
  getYPProtocols: __WEBPACK_IMPORTED_MODULE_3__getYPProtocols__["a" /* getYPProtocols */],
  getYPSourcedata: __WEBPACK_IMPORTED_MODULE_4__getYPSourcedata__["a" /* getYPSourcedata */],
  getAllEntries: __WEBPACK_IMPORTED_MODULE_5__getAllEntries__["a" /* getAllEntries */] // async getAllEntriesSettings (root, args, context) {
  //   const entries = await MongoClient.connect('mongodb://' + configMM.dbHost + ':' + configMM.dbPort + '/' + configMM.dbParam , {
  //     promiseLibrary: require('bluebird')
  //   }).then(function (db) {
  //     let query = {}
  //     return db
  //       .collection(configMM.dbCollMMEntries)
  //       .find(query)
  //       .toArray()
  //       .finally(db.close.bind(db))
  //   })
  //     .then(function (data) {
  //       return data
  //     })
  //     .catch(function (err) {
  //       if (err) {
  //         console.error('ERROR', err)
  //       }
  //     })
  //
  //   return entries
  //
  // },
  // async getAllDomainsSettings (root, { flag }, context) {
  //   let query = flag !== undefined ? { flag: flag } : {}
  //
  //   const entries = await MongoClient.connect('mongodb://' + configMM.dbHost + ':' + configMM.dbPort + '/' + configMM.dbParam ,{
  //     promiseLibrary: require('bluebird')
  //   }).then(function(db) {
  //
  //     // To apply
  //     // let query = {'protocol': {$regex: '.*' + pattern.toUpperCase() + '.*'}, 'flag': 'APPROVED'};
  //
  //     //console.log(db);
  //
  //     return db
  //       .collection(configMM.dbCollMMDomains)
  //       .find(query)
  //       .toArray()
  //       .finally(db.close.bind(db))
  //
  //     // .then(function(e, data) {
  //     //   return data
  //     // })
  //     ////.finally(db.close.bind(db))
  //   })
  //     .then(function(data) {
  //       return data
  //     })
  //     .catch(function(err) {
  //       console.error('ERROR', err);
  //     })
  //
  //   return entries
  // },
  // async getAllStandardsSettings (root, {flag}, context) {
  //
  //   let query = flag !== undefined ? {flag: flag} : {}
  //
  //   const entries = await MongoClient.connect('mongodb://' + configMM.dbHost + ':' + configMM.dbPort + '/' + configMM.dbParam ,{
  //     promiseLibrary: require('bluebird')
  //   }).then(function(db) {
  //     return db
  //       .collection(dbCollMMStandards)
  //       .find(query)
  //       .toArray()
  //       .finally(db.close.bind(db))
  //   })
  //     .then(function(data) {
  //       return data
  //     })
  //     .catch(function(err) {
  //       console.error('ERROR', err);
  //     });
  //   return entries;
  // },
  // async getAllTargetsSettings (root, {args}, context) {
  //   const entries = await MongoClient.connect('mongodb://' + configMM.dbHost + ':' + configMM.dbPort + '/' + configMM.dbParam ,{
  //     promiseLibrary: require('bluebird')
  //   }).then(function(db) {
  //     return db
  //       .collection(dbCollMMTargets)
  //       .find()
  //       .toArray()
  //       .finally(db.close.bind(db))
  //   })
  //     .then(function(data) {
  //       return data
  //     })
  //     .catch(function(err) {
  //       console.error('ERROR', err)
  //     });
  //   return entries
  // },
  // async getAllDefaultVariablesSettings (root, { flag }, context) {
  //   let query = flag !== undefined ? { flag: flag } : {}
  //   const entries = await MongoClient.connect('mongodb://' + configMM.dbHost + ':' + configMM.dbPort + '/' + configMM.dbParam ,{
  //     promiseLibrary: require('bluebird')
  //   }).then(function (db) {
  //     return db
  //       .collection(dbCollMMDefaultVariables)
  //       .find(query)
  //       .toArray()
  //       .finally(db.close.bind(db))
  //   })
  //     .then(function (data) {
  //       return data
  //     })
  //     .catch(function (err) {
  //       console.error('ERROR', err)
  //     })
  //   return entries
  // },
  // async getAllNotifications (root, { protocol, domain, standard, entryId }, context) {
  //   let query = {}
  //
  //   if (protocol !== undefined) {
  //     query.protocol = protocol
  //   }
  //
  //   if (domain !== undefined) {
  //     query.domain = domain
  //   }
  //
  //   if (standard !== undefined) {
  //     query.standard = standard
  //   }
  //
  //   if (entryId !== undefined) {
  //     query.entryId = entryId
  //   }
  //
  //   const entries = await MongoClient.connect('mongodb://' + configMM.dbHost + ':' + configMM.dbPort + '/' + configMM.dbParam, {
  //     promiseLibrary: require('bluebird')
  //   }).then(function (db) {
  //     return db
  //       .collection(dbCollMMCommitLogMessage)
  //       .find(query)
  //       .toArray()
  //       .finally(db.close.bind(db))
  //   })
  //     .then(function (data) {
  //       return data
  //     })
  //     .catch(function (err) {
  //       console.error('ERROR', err)
  //     })
  //   return entries
  // }

});

/***/ }),
/* 104 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return getAllDomains; });
var getAllDomains = function getAllDomains() {
  return [{
    code: 'AE',
    name: 'Adverse Events'
  }, {
    code: 'CM',
    name: 'Concomitant Medications'
  }, {
    code: 'DM',
    name: 'Demographics'
  }, {
    code: 'EG',
    name: 'ECG Test Results'
  }, {
    code: 'EX',
    name: 'Exposure'
  }, {
    code: 'LB',
    name: 'Laboratory Test Results'
  }, {
    code: 'PK',
    name: 'PK Concentrations and Timepoints'
  }, {
    code: 'QS',
    name: 'Questionnaires'
  }, {
    code: 'SV',
    name: 'Subject Visits'
  }, {
    code: 'VS',
    name: 'Vital Signs'
  }];
};



/***/ }),
/* 105 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return getAllStandards; });
var getAllStandards = function getAllStandards() {
  return [{
    code: 'NOVDDR',
    name: 'NovDD Raw data standard'
  }, {
    code: 'NOVDDA',
    name: 'NovDD Analysis data standard'
  }, {
    code: 'SDTMM',
    name: 'NCDS SDTM Minus'
  }, {
    code: 'SDTMP',
    name: 'NCDS SDTM Plus'
  }, {
    code: 'SDTM',
    name: 'CDISC SDTM'
  }, {
    code: 'ADAM',
    name: 'CDISC ADAM'
  }];
};



/***/ }),
/* 106 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return getAllTargets; });
var getAllTargets = function getAllTargets() {
  return ['PMX_GD', 'PMX_MD01', 'DSUR', 'SDTM_DM_POOL'];
};



/***/ }),
/* 107 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return getYPProtocols; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__protocols_json__ = __webpack_require__(108);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__protocols_json___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__protocols_json__);


var getYPProtocols = function getYPProtocols(root, _ref) {
  var pattern = _ref.pattern;
  return __WEBPACK_IMPORTED_MODULE_0__protocols_json___default.a.filter(function (protocol) {
    return protocol.match(new RegExp(pattern, 'gi'));
  });
};



/***/ }),
/* 108 */
/***/ (function(module, exports) {

module.exports = ["C99_PKC412_001","CA01353A253AL","CAAE581A1101","CAAE581A1102","CAAE581A1201","CAAE581A2101","CAAE581A2102","CAAE581A2103","CAAE581A2104","CAAE581A2105","CAAE581A2106","CAAE581A2108","CAAE581A2109","CAAE581A2110","CAAE581A2202","CAAE581A2203","CAAE581A2203E1","CAAE581A2203E2","CAAE581C2201","CAAE581C2201E1","CAAG561A0201","CAAG561A2101","CAAG561A2102","CAAG561A2105","CAAG561A2213","CABF656A0001","CABF656A0002","CABF656A0003","CABF656A0004","CABF656A0005","CABF656A0006","CABF656A1202","CABF656A2103","CABF656A2206","CABF656A2304","CABF656A2307","CABF656B2202","CABJ409A2201","CABJ879A2101","CABL001A1101","CABL001A2101","CABL001A2102","CABL001A2103","CABL001A2104","CABL001A2301","CABL001X2101","CABN912A2101","CABN912B2102","CABN912B2104","CABP688A2101","CABP688A2102","CABP688A2103","CABP688A2105","CACR000A2420AL","CACZ885A1101","CACZ885A2101","CACZ885A2102","CACZ885A2106","CACZ885A2108","CACZ885A2201","CACZ885A2201E1","CACZ885A2201E2","CACZ885A2202","CACZ885A2203","CACZ885A2204","CACZ885A2206","CACZ885A2207","CACZ885A2211","CACZ885A2212","CACZ885A2213","CACZ885APRE","CACZ885B2101","CACZ885B2204","CACZ885C2201","CACZ885D2201","CACZ885D2203","CACZ885D2204","CACZ885D2208","CACZ885D2304","CACZ885D2306","CACZ885D2307","CACZ885D2307E1","CACZ885D2308","CACZ885D2401","CACZ885D2402","CACZ885DCA01","CACZ885DTR01","CACZ885F2201","CACZ885G1301","CACZ885G2301","CACZ885G2301E1","CACZ885G2305","CACZ885G2306","CACZ885G2401","CACZ885G2402","CACZ885G2403","CACZ885GFR01","CACZ885H2104","CACZ885H2251","CACZ885H2251E1","CACZ885H2255","CACZ885H2356","CACZ885H2356E1","CACZ885H2356E2","CACZ885H2357","CACZ885H2357E1","CACZ885H2357E2","CACZ885H2357E3","CACZ885H2358","CACZ885H2361","CACZ885H2361E1","CACZ885H2402","CACZ885I2202","CACZ885I2202E1","CACZ885I2206","CACZ885I2207","CACZ885I2208","CACZ885I2302","CACZ885J2301","CACZ885J2302","CACZ885M2101","CACZ885M2201","CACZ885M2301","CACZ885M2301CSR","CACZ885M2301OLE","CACZ885M2301_BWH","CACZ885M2301_CSR","CACZ885N2301","CACZ885N2301E2","CACZ885X2201","CACZ885X2202","CACZ885X2205","CACZ885X2206","CAEB071A2104","CAEB071A2106","CAEB071A2107","CAEB071A2108","CAEB071A2109","CAEB071A2110","CAEB071A2111","CAEB071A2112","CAEB071A2113","CAEB071A2114","CAEB071A2115","CAEB071A2117","CAEB071A2119","CAEB071A2120","CAEB071A2122","CAEB071A2125","CAEB071A2130","CAEB071A2201","CAEB071A2203","CAEB071A2203E1","CAEB071A2206","CAEB071A2206E1","CAEB071A2207","CAEB071A2210","CAEB071A2211","CAEB071A2214","CAEB071A2215","CAEB071A2216","CAEB071A2306","CAEB071B2101","CAEB071B2201","CAEB071C2201","CAEE788A2101","CAEE788A2101B","CAEE788A2102","CAEE788A2103","CAEE788A2106","CAEE788A2201","CAEP924A2101","CAEP924A2102","CAEP924A2103","CAEW334A2101","CAFG495A2101","CAFG495A2102","CAFQ056A1103","CAFQ056A1104","CAFQ056A2101","CAFQ056A2102","CAFQ056A2103","CAFQ056A2104","CAFQ056A2106","CAFQ056A2108","CAFQ056A2109","CAFQ056A2110","CAFQ056A2111","CAFQ056A2113","CAFQ056A2115","CAFQ056A2119","CAFQ056A2120","CAFQ056A2121","CAFQ056A2123","CAFQ056A2124","CAFQ056A2125","CAFQ056A2132","CAFQ056A2133","CAFQ056A2134","CAFQ056A2144","CAFQ056A2145","CAFQ056A2166","CAFQ056A2167","CAFQ056A2168","CAFQ056A2171","CAFQ056A2203","CAFQ056A2203_DCF","CAFQ056A2204","CAFQ056A2204_DCF","CAFQ056A2206","CAFQ056A2206_DCF","CAFQ056A2207","CAFQ056A2208","CAFQ056A2208_DCF","CAFQ056A2209","CAFQ056A2212","CAFQ056A2216","CAFQ056A2216_DCF","CAFQ056A2217","CAFQ056A2217_DCF","CAFQ056A2222","CAFQ056A2222_DMC","CAFQ056A2222_TST","CAFQ056A2223","CAFQ056A2223_IA","CAFQ056A2225","CAFQ056A2299","CAFQ056A2299_DMC","CAFQ056B2131","CAFQ056B2154","CAFQ056B2212_DCF","CAFQ056B2214","CAFQ056B2214_DCF","CAFQ056B2252","CAFQ056B2278","CAFQ056B2278_DCF","CAFQ056B2279","CAFQ056B2279_DCF","CAFU057A2201","CAGC393A2101","CAGO178A001DHPK","CAGO178A002DHPK","CAGO178A003DHPK","CAGO178A2101","CAGO178A2102","CAGO178A2103","CAGO178A2104","CAGO178A2105","CAGO178A2106","CAGO178A2109","CAGO178A2301","CAGO178A2301E1","CAGO178A2302","CAGO178A2302E1","CAGO178A2303","CAGO178A2303E1","CAGO178A2304","CAGO178A2305","CAGO178A2305E1","CAGO178ACL1001","CAGO178ACL1002","CAGO178ACL1003","CAGO178ACL1004","CAGO178ACL1005","CAGO178ACL1019","CAGO178ACL1020","CAGO178ACL1028","CAGO178ACL1031","CAGO178ACL1032","CAGO178ACL1033","CAGO178ACL1034","CAGO178ACL1039","CAGO178ACL1049","CAGO178ACL1053","CAGO178ACL1054","CAGO178ACL1058","CAGO178ACL2006","CAGO178ACL2007","CAGO178ACL2008","CAGO178ACL2009","CAGO178ACL2010","CAGO178ACL2011","CAGO178ACL2012","CAGO178ACL2013","CAGO178ACL2014","CAGO178ACL2015","CAGO178ACL2017","CAGO178ACL2018","CAGO178ACL2040","CAGO178ACL3021","CAGO178ACL3022","CAGO178ACL3023","CAGO178ACL3024","CAGO178ACL3025","CAGO178ACL3026","CAGO178ACL3027","CAGO178ACL3029","CAGO178ACL3030","CAGO178ACL3035","CAGO178ACL3036","CAGO178ACL3038","CAGO178ACL3041","CAGO178ACL3042","CAGO178ACL3043","CAGO178ACL3045","CAGO178ACL3046","CAGO178ACL3048","CAGO178ACL3052","CAGO178ACL3056","CAGO178ACL3063","CAGO178AIAE001","CAGO178AIAS001","CAGO178AIAS002","CAGO178AIAS003","CAGO178AIAS004","CAGO178APKH004","CAGO178APKH007","CAGO178APKH008","CAGO178APKH009","CAGO178APKH010","CAGO178APKH011","CAGO178APKH012","CAGO178APKH013","CAGO178APKH014","CAGO178APKH015","CAGO178APKH017","CAGO178APKH018","CAGO178APKH019","CAGO178APKH020","CAGO178APKH066","CAGO178C2102","CAGO178C2103","CAGO178C2104","CAGO178C2301","CAGO178C2302","CAGO178C2390","CAGO178C2399","CAGO178CCL0509","CAGO178CCL2005","CAGO178CCL2009","CAHT956A2101","CAHT956A2102","CAHT956A2105","CAIN457A1101","CAIN457A1302","CAIN457A2101","CAIN457A2102","CAIN457A2103","CAIN457A2104","CAIN457A2105","CAIN457A2106","CAIN457A2107","CAIN457A2110","CAIN457A2202","CAIN457A2202E1","CAIN457A2204","CAIN457A2206","CAIN457A2206E1","CAIN457A2208","CAIN457A2208C4","CAIN457A2209","CAIN457A2209E1","CAIN457A2211","CAIN457A2211E1","CAIN457A2212","CAIN457A2220","CAIN457A2223","CAIN457A2224","CAIN457A2225","CAIN457A2225C2","CAIN457A2227","CAIN457A2228","CAIN457A2302","CAIN457A2302E1","CAIN457A2302TST","CAIN457A2303","CAIN457A2304","CAIN457A2304E1","CAIN457A2307","CAIN457A2308","CAIN457A2309","CAIN457A2310","CAIN457A2312","CAIN457A2313","CAIN457A2314","CAIN457A2317","CAIN457A2318","CAIN457A2322","CAIN457A2323","CAIN457A2326","CAIN457A2401","CAIN457A3301","CAIN457A3302","CAIN457A3404","CAIN457ADE02","CAIN457ADE03","CAIN457ADE04","CAIN457ADE06","CAIN457ADE08","CAIN457ADMC","CAIN457AFR01","CAIN457AGB0","CAIN457AGB01","CAIN457AIT01","CAIN457AIT02","CAIN457AJP01","CAIN457APRE","CAIN457AUS01","CAIN457AUS02","CAIN457AUS07","CAIN457B2201","CAIN457B2201E1","CAIN457B2203","CAIN457C2301","CAIN457C2301E1","CAIN457C2302","CAIN457C2302E1","CAIN457C2303","CAIN457C2303E1","CAIN457C2399","CAIN457D2204","CAIN457F1301","CAIN457F2201","CAIN457F2206","CAIN457F2208","CAIN457F2302","CAIN457F2304","CAIN457F2305","CAIN457F2305E1","CAIN457F2306","CAIN457F2306E1","CAIN457F2308","CAIN457F2309","CAIN457F2309E1","CAIN457F2310","CAIN457F2311","CAIN457F2312","CAIN457F2314","CAIN457F2318","CAIN457F2320","CAIN457F2336","CAIN457F2342","CAIN457F2354","CAIN457F2366","CAIN457F3301","CAIN457F3302","CAIN457FDE03","CAIN457FDMC","CAIN457FUS01","CAKU517A2101","CAKU517A2102","CAKU517A2103","CAMN107A1101","CAMN107A1101E1","CAMN107A1101E2","CAMN107A1201","CAMN107A2101","CAMN107A2101E1","CAMN107A2101E2","CAMN107A2101E3","CAMN107A2101E4","CAMN107A2101E5","CAMN107A2101E6","CAMN107A2101E7","CAMN107A2101E8","CAMN107A2101E9","CAMN107A2102","CAMN107A2103","CAMN107A2103E1","CAMN107A2104","CAMN107A2106","CAMN107A2108","CAMN107A2109","CAMN107A2110","CAMN107A2112","CAMN107A2113","CAMN107A2115","CAMN107A2116","CAMN107A2117","CAMN107A2119","CAMN107A2120","CAMN107A2120_DCF","CAMN107A2121","CAMN107A2122","CAMN107A2123","CAMN107A2127","CAMN107A2128","CAMN107A2129","CAMN107A2130","CAMN107A2131","CAMN107A2132","CAMN107A2201","CAMN107A2201E1","CAMN107A2203","CAMN107A2203_DCF","CAMN107A2302","CAMN107A2303","CAMN107A2303E1","CAMN107A2404","CAMN107A2405","CAMN107A2408","CAMN107A2409","CAMN107A2410","CAMN107AAU04","CAMN107ADE10","CAMN107ADE20","CAMN107AIC05","CAMN107AIL01","CAMN107ARU01","CAMN107ARU02","CAMN107AUS09","CAMN107AUS17","CAMN107AUS20","CAMN107AUS28","CAMN107AUS37","CAMN107B2301","CAMN107C2101","CAMN107C2102","CAMN107C2103","CAMN107C2104","CAMN107C2105","CAMN107C2106","CAMN107C2107","CAMN107C2108","CAMN107C2111","CAMN107C2118","CAMN107CIC03","CAMN107D1201","CAMN107DBR01","CAMN107DDE05","CAMN107E2401","CAMN107ECN02","CAMN107ECN02E1","CAMN107EIC01","CAMN107FJP01","CAMN107G2301","CAMN107I2201","CAMN107X2201","CAMN107X2201E1","CAMN107Y2101","CAMN107YDE19","CAMP3970101","CAMP3970102","CAMP397A2103","CAMP397A2104","CAMP397A2105","CAMP397A2107","CAMP397A2202","CAPL180A2201","CAPL180A2207","CAPL180A2210A","CAPL180A2210B","CAPP018A0001","CAPP018A0002","CAPP018A0003","CAQS7042201","CAQS7042202","CAQW051A1101","CAQW051A2101","CAQW051A2102","CAQW051A2104","CAQW051A2202","CAQW051A2204","CAQW051A2205","CAQW051A2207","CAQW051A2209","CAQW051A2210","CARD233AP01","CARD233AP02","CARD233AP03","CARD233AP04","CARD233AP05","CARD233AP06","CARD233AP07","CARD233AP09","CARD233AP10","CARD233AP11","CARD233AP12","CARD233AP13","CARD233AP14","CARD233AP15","CARD233AP18","CARD233AP19","CARD233AP27","CARD233AP28","CARD233AP30","CARD233AP32","CARD233AP90","CARD233APBMET","CARD233APME","CARD233APNDA","CARD233DPB10B","CARD233DPB11","CARD233DPB14","CARD233DPBC4","CARD233DPBC7","CARD233DPBC9","CARD233DPCM1","CARD233DPMY1","CARD233GB01","CASA404A1101","CASA404A1102","CASA404A2101","CASA404A2102","CASA404A2103","CASA404A2104","CASA404A2105","CASA404A2106","CASA404A2107","CASA404A2108","CASA404A2109","CASA404A2110","CASA404A2111","CASA404A2112","CASA404A2113","CASA404A2114","CASA404A2115","CASA404A2201","CASA404A2202","CASA404A2203","CASA404A2204","CASA404A2205","CASA404A2301","CASA404A2302","CASA404A2303","CASA404D2201","CASA404D2202","CASD7320101","CASM9810301E1","CASM9810304","CASM9810306","CASM9810315","CASM9810316","CASM9810317","CASM9811301","CASM9811302","CASM9811303","CASM981A2102","CASM981A2201","CASM981A2205","CASM981A2206","CASM981A2308","CASM981B005","CASM981B202","CASM981B2102","CASM981B2204","CASM981B2316","CASM981B2317","CASM981B305","CASM981B306","CASM981B307","CASM981B308","CASM981B313","CASM981B315","CASM981BW222","CASM981BW223","CASM981C005","CASM981C0315E1","CASM981C1201","CASM981C1202","CASM981C1301","CASM981C1302","CASM981C1303","CASM981C1304","CASM981C201","CASM981C201E1","CASM981C202","CASM981C202E1","CASM981C2301","CASM981C2302","CASM981C2306","CASM981C2307","CASM981C2308","CASM981C2311","CASM981C2313","CASM981C2314","CASM981C2315","CASM981C2316","CASM981C2401","CASM981C2402","CASM981C2405","CASM981C2405E1","CASM981C2406","CASM981C2420","CASM981C2434","CASM981C2434E1","CASM981C2435","CASM981C2436","CASM981C2439","CASM981C2440","CASM981C2442","CASM981C2445","CASM981C2446","CASM981CBD01","CASM981CCA01","CASM981CDE01","CASM981CDE04","CASM981CDE06","CASM981CDE07","CASM981CDE10","CASM981CDE11","CASM981CDE18","CASM981CDE22","CASM981CES01","CASM981CGB01","CASM981CJP01","CASM981CRU01","CASM981CUS01","CASM981CUS02","CASM981CUS03","CASM981CUS04","CASM981CUS05","CASM981CUS08","CASM981CUS09","CASM981CUS09E1","CASM981CVE01","CASM981D2201","CASM981E2202","CASM981E2203","CASM981E2204","CASM981E2205","CASM981F2201","CASM981G2201","CASM981G2202","CASM981H2201","CASM981H2201E1","CASM981M2301","CASM981N2101","CASM981N2203S","CASM981N2301","CASM981W108","CASM981W116","CASM981W121","CASM981W122","CASM981W124","CASM981W201","CASM981W202","CASM981W203","CASM981W204","CASM981W205","CASM981W206","CASM981W206E1","CASM981W301","CASM981W332","CASM981W333","CASQ8710105","CATF936A2201","CATI355A2102","CATI355A2103","CATI355A2105","CAUB246A2301","CAUB246A2302","CAUB246ANTP003","CAUY922A1101","CAUY922A2101","CAUY922A2101B","CAUY922A2103","CAUY922A2105","CAUY922A2109","CAUY922A2202","CAUY922A2205","CAUY922A2206","CAUY922A2207","CAXT914A2202","CBAF312A1101","CBAF312A1101DCF","CBAF312A2101","CBAF312A2101DCF","CBAF312A2102","CBAF312A2102DCF","CBAF312A2104","CBAF312A2104DCF","CBAF312A2105","CBAF312A2105DCF","CBAF312A2107","CBAF312A2107DCF","CBAF312A2108","CBAF312A2108DCF","CBAF312A2110","CBAF312A2110DCF","CBAF312A2111","CBAF312A2111DCF","CBAF312A2116","CBAF312A2116DCF","CBAF312A2118","CBAF312A2118DCF","CBAF312A2119","CBAF312A2119DCF","CBAF312A2121","CBAF312A2121DCF","CBAF312A2122","CBAF312A2122DCF","CBAF312A2124","CBAF312A2125","CBAF312A2126","CBAF312A2128","CBAF312A2128DCF","CBAF312A2129","CBAF312A2129DCF","CBAF312A2130","CBAF312A2130DCF","CBAF312A2201","CBAF312A2201E1","CBAF312A2201_DMC","CBAF312A2201_INT","CBAF312A2202","CBAF312A2202DCF","CBAF312A2304","CBAF312A2304B","CBAF312A2304EP","CBAF312X2205","CBAF312X2205DCF","CBAF312X2206","CBAF312X2206DCF","CBAF312X2207","CBAG972A2103","CBAH824A2101","CBAH824A2102","CBBZ098A1101","CBBZ098A2101","CBBZ098A2102","CBCT194A2101","CBCT194A2101C2","CBCT194A2101C3","CBCT194A2102","CBCT197A1101","CBCT197A2101","CBCT197A2201","CBCT197A2202","CBCT197A2205","CBEZ235A1101","CBEZ235A2101","CBEZ235A2110","CBEZ235A2118","CBEZ235A2118_DCF","CBEZ235A2119","CBEZ235B2101","CBEZ235B2201","CBEZ235B2203","CBEZ235C2101","CBEZ235C2108","CBEZ235C2201","CBEZ235D2101","CBEZ235F2201","CBEZ235Y2201","CBEZ235Z2401","CBEZ235Z2402","CBEZ235ZIC01","CBFH772A2201","CBFH772A2203","CBGG492A1101","CBGG492A2101","CBGG492A2102","CBGG492A2105","CBGG492A2106","CBGG492A2107","CBGG492A2108","CBGG492A2112","CBGG492A2115","CBGG492A2116","CBGG492A2117","CBGG492A2119","CBGG492A2202","CBGG492A2203","CBGG492A2204","CBGG492A2207","CBGG492A2210","CBGG492A2211","CBGG492A2212","CBGG492A2214","CBGG492A2215","CBGG492A2216","CBGJ398X1101","CBGJ398X2101","CBGJ398X2102","CBGJ398X2103","CBGJ398X2104C","CBGJ398X2105","CBGJ398X2106","CBGJ398X2201","CBGJ398X2204","CBGJ398X2205","CBGS649A2101","CBGS649A2102","CBGS649A2103","CBGS649A2104","CBGS649A2105","CBGS649A2202","CBGS649A2204","CBGT226A1101","CBGT226A2101","CBHQ880A2101","CBHQ880A2102","CBHQ880A2102A","CBHQ880A2102B","CBHQ880A2203","CBHQ880A2204","CBHQ880APRE","CBJB221A2101","CBKM120B2101","CBKM120C2102","CBKM120C2103","CBKM120C2104","CBKM120C2106","CBKM120C2108","CBKM120C2110","CBKM120C2111","CBKM120C2112","CBKM120C2113","CBKM120C2114","CBKM120C2201","CBKM120D2101","CBKM120D2201","CBKM120D2204","CBKM120D2205","CBKM120E2101","CBKM120E2102","CBKM120F2201","CBKM120F2202","CBKM120F2203","CBKM120F2301","CBKM120F2302","CBKM120F2303","CBKM120H2201","CBKM120H2201_DCF","CBKM120X1101","CBKM120X1101_DCF","CBKM120X2101","CBKM120X2101_DCF","CBKM120X2107","CBKM120XUS11T","CBKM120Z2102","CBKM120Z2402","CBKM120ZUS40","CBPD9522308","CBPD952A1401","CBPD952A2201","CBPD952A2205","CBPD952A2206","CBPD952A2208","CBPD952A2209","CBPD952A2301","CBPD952A2302","CBPD952A2303","CBPD952A2304","CBPD952A2305","CBPD952A2306","CBPD952A2307","CBPD952A2308","CBPD952A2309","CBPD952A2401","CBPD952B2301","CBPD952B2401","CBPD952C2301","CBPD952E2201","CBPD952E2202","CBPR277X2101","CBPS804A2101","CBPS804A2201","CBPS804A2202","CBPS804A2203","CBPS804A2204","CBQS481A2101","CBUW078X2101","CBVS857X2101","CBVS857X2201","CBVS857X2202","CBYL719A2103","CBYL719A2103_DCF","CBYL719A2104","CBYL719A2105","CBYL719A2105_DCF","CBYL719A2109","CBYL719A2201","CBYL719X1101","CBYL719X2101","CBYL719X2101_DCF","CBYL719X2102","CBYL719X2103","CBYL719X2104","CBYL719X2105","CBYL719X2105J","CBYL719X2107","CBYL719X2107_DCF","CBYL719X2201","CBYL719X2402","CBYL719Z2101","CBYL719Z2102","CBYM338B2203","CBYM338B2203E1","CBYM338B2302","CBYM338BCMED","CBYM338D2201","CBYM338E2202","CBYM338E2202E1","CBYM338X2101","CBYM338X2102","CBYM338X2104","CBYM338X2105","CBYM338X2106","CBYM338X2107","CBYM338X2108","CBYM338X2109","CBYM338X2110","CBYM338X2201","CBYM338X2202","CBYM338X2204","CBYM338X2205","CBYM338X2205E1","CBYM338X2206","CBYM338X2207","CBYM338X2210","CBYM338X2211","CBYM338XPRE","CBZF961X2101","CBZF961X2102","CBZF961X2201","CCAD106A2101","CCAD106A2201","CCAD106A2201E1","CCAD106A2202","CCAD106A2202E1","CCAD106A2203","CCAD106APRE1","CCAF178A1101","CCAF178A1101_EDC","CCAT458M2101","CCAT458M2402","CCBC134A2101","CCBC134A2103","CCBC134A2402","CCBC134A2403","CCBC134A2404","CCBC134AFR01","CCBC134AIT01","CCBD050A2101","CCBD050A2102","CCBD050A2105","CCBD050A2106","CCBD050A2303","CCBD050A2306","CCBD050A2410","CCBD050A2412","CCDZ173X2101","CCDZ173X2102","CCDZ173X2104","CCDZ173X2201","CCDZ173X2201E1","CCDZ173X2202","CCDZ173X2203","CCFG920X2101","CCFG920X2102","CCFQ175X2101","CCFZ533X1101","CCFZ533X2101","CCFZ533X2201","CCFZ533X2201P1","CCFZ533X2202","CCFZ533X2203","CCFZ533X2204","CCFZ533X2205","CCGF166X2201","CCGF166X2202","CCGM097X2101","CCHI621A1101","CCHI621A1102","CCHI621A2402","CCHI621AB152","CCHI621ADE01","CCHI621ADE02","CCHI621ADE04","CCHI621AFR01","CCHI621AFR02","CCHI621AFR05","CCHI621AIS","CCHI621AIT05","CCHI621AIT06","CCHI621A_NONJP","CCHI621B201","CCHI621B201E1","CCHI621B352","CCHI621B352E1","CCHI621C102","CCHI621C304","CCHI621CS01","CCHI621IN08","CCHI621IN10","CCHI621IN11","CCHI621IN12","CCHI621IN13","CCHI621US04","CCHI621US05","CCHI621US06","CCHN438X2101","CCHN438X2102","CCIB002AM01","CCIB002AM02","CCIB002AM03","CCIB002AM04","CCIB002AM05","CCIB002AM06","CCIB002AM07","CCIB002AM100","CCIB002AM102","CCIB002AM103","CCIB002AM104","CCIB002AM14","CCIB002AM15","CCIB002AM16","CCIB002AM18","CCIB002AM19","CCIB002AM31","CCIB002FUS06","CCIB002FUS08","CCIB002FUS11","CCIB002FUS12","CCIB002FUS19","CCIB002G0104","CCIB002G2301","CCIB002H2301","CCIB002H2303","CCIB002H2304","CCIB002I2301","CCIB002K2301","CCIB002K2302","CCIB824EUS01","CCJM112X2101","CCJM112X2202","CCJM112X2203","CCLG561X2101A","CCLG561X2201A","CCLR325X2101","CCLR325X2202","CCLR457X2101","CCMK389X2101","CCNP392X2101","CCNP520X1101","CCNP520X2101","CCNP520X2102","CCNP520X2103","CCNP520X2105","CCOA5660003","CCOA5660004","CCOA5660005","CCOA5660006","CCOA5660007","CCOA5660008","CCOA5660009","CCOA5660010","CCOA5660011","CCOA5660012","CCOA5660014","CCOA5660020","CCOA5660022","CCOA5660023","CCOA5660024","CCOA5660025","CCOA5660026","CCOA5660027","CCOA5660028","CCOA5662302","CCOA566A030","CCOA566A1101","CCOA566A2101","CCOA566A2102","CCOA566A2301","CCOA566A2401","CCOA566A2401E1","CCOA566A2403","CCOA566A2404","CCOA566A2407","CCOA566A2412","CCOA566A2417","CCOA566ABD01","CCOA566ABM01","CCOA566ABM02","CCOA566ABR01","CCOA566AIC04","CCOA566AUGA01","CCOA566B2101","CCOA566B2102","CCOA566B2104","CCOA566B2106","CCOA566B2303","CCOA566B2306","CCOA566B2401","CCOA566B2401E1","CCOM9981101","CCOM998A1201","CCOM998A1202","CCOM998A1203","CCOM998A1204","CCOM998A2201","CCOM998A2404","CCOM998AC352","CCOM998AC352E1","CCOM998AIS_POOLED","CCOM998AUS01","CCOM998IA01","CCOM998IA04","CCOM998IB01","CCOM998OR33","CCOM998OR44","CCOM998OR52","CCOM998OR63","CCOM998US01","CCOX1890101","CCOX1890102","CCOX1890103","CCOX1890104","CCOX1890105","CCOX1890107","CCOX1890108","CCOX1890109","CCOX1890110","CCOX1890111","CCOX1890112","CCOX1890112E1","CCOX1890114","CCOX1890115","CCOX1890117","CCOX1890121","CCOX1890122","CCOX1890123","CCOX1890126","CCOX1890128","CCOX1890129","CCOX1890130","CCOX1890131","CCOX1890132","CCOX1890134","CCOX1892302","CCOX1892312","CCOX189A0106","CCOX189A0135","CCOX189A0136","CCOX189A0137","CCOX189A1101","CCOX189A1102","CCOX189A1103","CCOX189A1201","CCOX189A1202","CCOX189A2301","CCOX189A2303","CCOX189A2307","CCOX189A2311","CCOX189A2313","CCOX189A2314","CCOX189A2315","CCOX189A2316","CCOX189A2319","CCOX189A2320","CCOX189A2326","CCOX189A2329","CCOX189A2330","CCOX189A2331","CCOX189A2332","CCOX189A2335","CCOX189A2335E1","CCOX189A2336","CCOX189A2349","CCOX189A2350","CCOX189A2351","CCOX189A2353","CCOX189A2357","CCOX189A2358","CCOX189A2360","CCOX189A2360E1","CCOX189A2361","CCOX189A2361E1","CCOX189A2362","CCOX189A2364","CCOX189A2365","CCOX189A2367","CCOX189A2369","CCOX189A2425","CCOX189A2426","CCOX189A2427","CCOX189A2428","CCOX189A2474","CCOX189A2475","CCOX189AADJCRO","CCOX189AGB02","CCOX189C2101","CCOX189C2102","CCOX189D2103","CCRM001A2201","CCRV778A2101","CCRV789A2201","CCSJ117X2101","CCSJ148X2101","CCSJ148X2201","CCSJ148X2202","CCTESTCOGNIZANT","CCTESTCOGNIZANT2","CCTESTCRT","CCTESTJYR","CCTESTNONMEM","CCTESTNONMEMVI","CCTESTR31234","CCTESTR32101J","CCTESTR3CODEARC","CCTESTR3PROTOCOL","CCTESTSTUDY1","CCTESTSTUDY10","CCTESTSTUDY11","CCTESTSTUDY13","CCTESTSTUDY14","CCTESTSTUDY15","CCTESTSTUDY16","CCTESTSTUDY1E1","CCTESTSTUDY2","CCTESTSTUDY20","CCTESTSTUDY21","CCTESTSTUDY25","CCTESTSTUDY3","CCTESTSTUDY4","CCTESTSTUDY5","CCTESTSTUDY6","CCTESTSTUDY7","CCTESTSTUDY9","CCTL019A2201","CCTL019A2205B","CCTL019B2001X","CCTL019B2101J","CCTL019B2101J_DCF","CCTL019B2102J","CCTL019B2202","CCTL019B2203J","CCTL019B2204","CCTL019B2205J","CCTL019B2206","CCTL019B2207J","CCTL019B2301","CDAP-00-01","CDAP-00-02","CDAP-00-03","CDAP-00-04","CDAP-4CELL-05-02","CDAP-4PSW-03-03","CDAP-4REN-03-06","CDAP-ADT-04-02","CDAP-BAC-98-03","CDAP-CAP-00-05","CDAP-CAP-00-08","CDAP-DI-01-01","CDAP-DIW-01-08","CDAP-EAP-02-01","CDAP-GER-01-11","CDAP-HEP-00-09","CDAP-IE-01-02","CDAP-MRDI-01-03","CDAP-MRDI-01-09","CDAP-OBSE-01-07","CDAP-QTNC-01-06","CDAP-REN-02-03","CDAP-REN-07-01","CDAP-RRC-98-04","CDAP-SST-98-01","CDAP-SST-9801-B","CDAP-SST-99-01","CDAP-STAT-01-10","CDAP-VRE-00-07","CDAR328A0101","CDAR328A0102","CDAR328A0103","CDAR328A0201","CDAR328A0202","CDAR328A0203","CDAR328A0204","CDAR328A0205","CDAR328A0207","CDAR328A0208","CDAR328A0209","CDAR328A0210","CDAR328A0211","CDAR328A0212","CDAR328A0213","CDAR328A0214","CDAR328A0215","CDAR328A0216","CDAR328A0217","CDAR328A0218","CDAR328A0219","CDAR328A0220","CDAR328A0221","CDAR328A0222","CDAR328A0224","CDAR328A0225","CDAR328A0226","CDAR328A0227","CDAR328A0301","CDAR328A0302","CDAR328A0303","CDAR328A0304","CDAR328A0305","CDAR328A0305A","CDAR328A0305C","CDAR328A0307","CDAR328A0308","CDAR328A0310","CDAR328A0311","CDAR328A0312","CDAR328A0313","CDAR328A0315","CDAR328A0316","CDAR328A0317","CDAR328A0350","CDAR328A0351","CDAR328A0352","CDAR328A0353","CDAR328A0356","CDAR328A0359","CDAR328A0360","CDAR328A0454","CDAR328A0455","CDAR328A0456","CDAR328A0501","CDAR328A0502","CDAR328A0503","CDAR328A0504","CDAR328A0601A","CDAR328A0601B","CDAR328A0666","CDAR328A0670","CDAR328A0676","CDAR328A0684","CDAR328A0701","CDAR328A1001","CDAR328A1002","CDAR328A1004","CDAR328A1005","CDAR328A1007","CDAR328A1008","CDAR328A1009","CDAR328A1010","CDAR328A1011","CDAR328A1012","CDAR328A1013","CDAR328A1014","CDAR328A1014A","CDAR328A1014C","CDAR328A1014D","CDAR328A1014E","CDAR328A1014F","CDAR328A1015","CDAR328A1016","CDAR328A1017","CDAR328A1018","CDAR328A1019","CDAR328A1021","CDAR328A1022","CDAR328A1023","CDAR328A1024","CDAR328A1025","CDAR328A1026","CDAR328A1027","CDAR328A1028","CDAR328A1030","CDAR328A1031","CDAR328A1032","CDAR328A1033","CDAR328A1035","CDAR328A1036","CDAR328A1038","CDAR328A1040","CDAR328A1041","CDAR328A1042","CDAR328A1042A","CDAR328A1042B","CDAR328A1042C","CDAR328A1044","CDAR328A1045","CDAR328A1047","CDAR328A1049","CDAR328A2101","CDAR328A2301","CDAR328A2302","CDAR328A2303","CDAR328A2304","CDAR328A2401","CDAR328A2403","CDAR328A2404","CDAR328A2409","CDAR328A2410","CDAR328A2413","CDAR328A2414","CDAR328AUS01","CDAR328B2101","CDAR328B2201","CDAR328B2301","CDEB025A2101","CDEB025A2102","CDEB025A2103","CDEB025A2105","CDEB025A2106","CDEB025A2107","CDEB025A2108","CDEB025A2109","CDEB025A2110","CDEB025A2112","CDEB025A2114","CDEB025A2115","CDEB025A2116","CDEB025A2119","CDEB025A2210","CDEB025A2211","CDEB025A2212","CDEB025A2222","CDEB025A2233","CDEB025A2301","CDEB025A2306","CDEB025A2307","CDEB025A2312","CDEB025A2313","CDEBIO025101","CDEBIO025102","CDEBIO025103","CDEBIO025105","CDEBIO025106","CDEBIO025107","CDEBIO025111","CDEBIO025113","CDEBIO025114","CDEBIO025116","CDEBIO025HCV118","CDEBIO025HCV119","CDEBIO025HCV203","CDEBIO025HCV205","CDEBIO025HCV207","CDEBIO025HCV209","CDJN6080102","CDJN6080103","CDJN6080105","CDJN6080114","CDJN6080116","CDJN6080120","CDJN6080121","CDJN6080126","CDJN6080127","CDJN6080201","CDJN6080302","CDJN6080304","CDJN6080306","CDJN6080354","CDJN6080356","CDJN6082301","CDJN6082302","CDJN6082414","CDJN608A0113","CDJN608A2102","CDJN608A2302","CDJN608A2308","CDJN608A2308E1","CDJN608A2401","CDJN608A2404","CDJN608A2413","CDJN608ACN06","CDJN608ACN07","CDJN608AGR01","CDJN608AIA01","CDJN608AIA04","CDJN608AIA05","CDJN608AIA06","CDJN608AIA07","CDJN608AIT05","CDJN608ASG01","CDJN608AUS07","CDJN608AUS08","CDJN608AUS13","CDJN608B2302","CDJN608B2302CSR","CDJN608W364","CDMN608A2201","CDNK3330101","CDNK3330102","CDNK3330103","CDNK3330104","CDNK333B2103","CDNK333B2201","CDNK333B2202","CDNK333C2101","CDNK333C2201","CDNPH0010011","CDNPH0010022","CDNPH0010192","CDNPH0110022","CDPP7280102","CDPP7280114","CDPP728W101","CDPP728W102","CEDP239X2101","CEDP239X2201","CEGF816X2101","CEGF816X2102","CEGF816X2201C","CELC200A2101","CELC200A2102","CELC200A2103","CELC200A2104","CELC200A2105","CELC200A2106","CELC200A2107","CELC200A2109","CELC200A2305","CELC200A2401","CELC200A2406","CELC200AES03","CELC200AUS01","CELC200AUS02","CELC200AUS02E1","CELC200AUS11","CELC200AUS13","CELC200AUS14","CELC200AUS15","CELC200B1101","CELC200B1102","CENA7130401","CENA7130504","CENA7130513","CENA713B2310","CENA713B2311","CENA713B2311E1","CENA713B2312","CENA713B2314","CENA713B2315","CENA713B2423","CENA713B304","CENA713BCN05","CENA713BES02","CENA713BES02E","CENA713BIA03","CENA713BIA05","CENA713BIA05E","CENA713BIA07","CENA713BIA09","CENA713BINT03","CENA713BUS10","CENA713BUS18","CENA713BUS25","CENA713BUS32","CENA713C152","CENA713CIA05","CENA713CN05","CENA713D1101","CENA713D1201","CENA713D1301","CENA713D1301E1","CENA713D1301E1COF1","CENA713D1301E1COF2","CENA713D1303","CENA713D1403","CENA713D2101","CENA713D2102","CENA713D2320","CENA713D2320E1","CENA713D2331","CENA713D2332","CENA713D2333","CENA713D2334","CENA713D2335","CENA713D2338","CENA713D2340","CENA713D2344","CENA713D2405","CENA713D2406","CENA713D2409","CENA713DDE15","CENA713DDE18","CENA713DES07","CENA713DFR08","CENA713DJP02","CENA713DTW04","CENA713DUS38","CENA713DUS38E1","CENA713DUS44","CENA713DUS44E1","CENA713D_MAX_RECAP","CENA713E2101","CENA713IA05","CENA713W155","CENA713W159","CENA713W160","CEPO9060101","CEPO9060102","CEPO9062201","CEPO9062202","CEPO9062203","CEPO9062203E","CEPO9062205","CEPO906A1101","CEPO906A1102","CEPO906A1103","CEPO906A2103","CEPO906A2103E1","CEPO906A2104","CEPO906A2105","CEPO906A2108","CEPO906A2110","CEPO906A2112","CEPO906A2113","CEPO906A2114","CEPO906A2117","CEPO906A2120","CEPO906A2121","CEPO906A2122","CEPO906A2123","CEPO906A2203E2","CEPO906A2204","CEPO906A2207","CEPO906A2208","CEPO906A2209","CEPO906A2210","CEPO906A2211","CEPO906A2212","CEPO906A2213","CEPO906A2227","CEPO906A2229","CEPO906A2303","CEPO906A2321","CERL0800101","CERL0800102","CERL0800104","CERL0800105","CERL0800106","CERL0800109","CERL080A2101","CERL080A2301","CERL080A2302","CERL080A2305","CERL080A2401","CERL080A2404","CERL080A2405","CERL080A2405AP01","CERL080A2405AU01","CERL080A2405DE01","CERL080A2405DE02","CERL080A2405ES01","CERL080A2405ES03","CERL080A2405FR01","CERL080A2405IN01","CERL080A2405IT01","CERL080A2405IT02","CERL080A2405IT03","CERL080A2405LA01","CERL080A2405NL01","CERL080A2405US01","CERL080A2407","CERL080A2408","CERL080A2409","CERL080A2410","CERL080A2416","CERL080A2419","CERL080A2420","CERL080A2423","CERL080A2424","CERL080ABR02","CERL080ADE08","CERL080ADE09","CERL080ADE10","CERL080ADE12","CERL080ADE27","CERL080AES08","CERL080AFR03","CERL080AFR04","CERL080AFR05","CERL080AGB03","CERL080AIT01","CERL080AIT02","CERL080AIT09","CERL080AUS02","CERL080AUS40","CERL080AUS51","CERL080AUS67","CERL080AUS90","CERL080B107","CERL080B2101","CERL080B301","CERL080B302","CESG3880001","CESG3880004","CESG3880007","CESG3880008","CESG3880015","CESG3880015E","CESG388DE01","CESG388SPCC2","CESG388SPCC3","CESG388SPCC4","CEST158DE01","CEST274ADE01","CFAD286A2103","CFAM810A2101","CFAM810A2102","CFAM810A2103","CFAM810A2202","CFAM810A2203","CFAM810A2204","CFAM810A2205","CFAM810A2206","CFAM810A2207","CFAM810A2208","CFAM810A2212","CFAM810A2213","CFAM810A2216","CFAM810A2217","CFAM810A2308","CFAM810A2309","CFAM810A2310","CFAM810A2401","CFAM810A2402","CFAM810A2403","CFAM810A2404","CFAM810A2412","CFAM810AUS07","CFAM810B2301","CFAM810B2303","CFAM810B2304","CFAM810B2305","CFEM3450003","CFEM3450008","CFEM3450010","CFEM3450012","CFEM3450015","CFEM3450017","CFEM3450024","CFEM34500245","CFEM3450025","CFEM3450025E1","CFEM3450025S","CFEM3450026","CFEM3450029","CFEM3450036","CFEM3450038","CFEM3450500","CFEM345AR01","CFEM345AR02","CFEM345AR02C","CFEM345AR02E","CFEM345AR04","CFEM345AR06","CFEM345AR07","CFEM345AR26","CFEM345AR27","CFEM345ARBC2","CFEM345ARBC3","CFEM345ARES1","CFEM345ARET1","CFEM345ARPK1","CFEM345ARPS1","CFEM345ARST1","CFEM345ARST1E","CFEM345D2405","CFEM345D2406","CFEM345D2407","CFEM345D2411","CFEM345DDE09","CFEM345DUS59","CFEM345EGB07","CFEM345F1201","CFEM345F1203","CFEM345MA17","CFG920X2101","CFOR2580001","CFOR2580002","CFOR2580003","CFOR2580004","CFOR2580005","CFOR2580014","CFOR2580016","CFOR2580040","CFOR2580041","CFOR2580045","CFOR2580046","CFOR2580049","CFOR2580054","CFOR2580056","CFOR2580058","CFOR2580061","CFOR2580062","CFOR2580069","CFOR2580071","CFOR2580072","CFOR2580073","CFOR2580102","CFOR2580103","CFOR2580402","CFOR2580601","CFOR2580602","CFOR2580603","CFOR2580605","CFOR2580701","CFOR2580702","CFOR258D2201","CFOR258D2301","CFOR258D2307","CFOR258D2308","CFOR258D2402","CFOR258D2412","CFOR258D2416","CFOR258DFORD2","CFOR258DFORINT02","CFOR258DFORINT03","CFOR258DIA02","CFOR258DIA05","CFOR258DMTA02","CFOR258DPDA2","CFOR258DPDF2","CFOR258DPFL2","CFOR258DPME1","CFOR258DPME2","CFOR258DPNA1","CFOR258DPNA2","CFOR258DPON1","CFOR258DPON2","CFOR258DPPD1","CFOR258DPPD2","CFOR258DPPD2A","CFOR258DPPD2F","CFOR258DPPD3","CFOR258DPPD5","CFOR258DPPD6","CFOR258DPRD1","CFOR258DPRD2","CFOR258DPRD3","CFOR258DPRD3F","CFOR258DPSP2","CFOR258DPSP4","CFOR258DUS06","CFOR258F0604","CFOR258F2301","CFOR258F2302","CFOR258F2303","CFOR258F2304","CFOR258F2305","CFOR258F2306","CFOR258F2308","CFOR258F2309","CFOR258F2402","CFOR258FOIT2","CFOR258FOOD1","CFOR258FORF01","CFOR258FORF03","CFOR258FORI01","CFOR258FOS01","CFOR258FOS02","CFOR258FOSA1","CFOR258FOUK2","CFOR258H2101","CFOR258H2104","CFOR258H2201","CFOR258I2201","CFOR258I2202","CFOR258IA03","CFOR258IN02","CFOR258INT02","CFOR258INT03","CFOR258META","CFOR258MTA03","CFOR258MTA22","CFOR258S01","CFOR258S02","CFOR258SBR01","CFOR258SDPCU1","CFOR258SDPDF1","CFOR258SDPDF3","CFOR258SDPDF4","CFOR258SMTA02","CFOR258US02A","CFOR358D2301","CFTY7200107","CFTY7200112","CFTY7200118","CFTY720A0106","CFTY720A0108","CFTY720A0114","CFTY720A0115","CFTY720A0116","CFTY720A0119","CFTY720A0121","CFTY720A0121E","CFTY720A0121E1","CFTY720A0121E2","CFTY720A0124","CFTY720A0124E","CFTY720A0124E1","CFTY720A0125","CFTY720A0125E","CFTY720A0125E1","CFTY720A2202","CFTY720A2202E","CFTY720A2202E1","CFTY720A2204","CFTY720A2213","CFTY720A2213E1","CFTY720A2215","CFTY720A2216","CFTY720A2217","CFTY720A2218","CFTY720A2218E","CFTY720A2218E1","CFTY720A2302","CFTY720A2302E","CFTY720A2302E1","CFTY720A2304","CFTY720A2305","CFTY720A2306","CFTY720A2307","CFTY720A2307E","CFTY720A2309","CFTY720A2311","CFTY720B0102","CFTY720B101","CFTY720B102","CFTY720B201","CFTY720B201E","CFTY720D1201","CFTY720D1201E1","CFTY720D1201_DMC","CFTY720D1401","CFTY720D2101","CFTY720D2102","CFTY720D2105","CFTY720D2106","CFTY720D2107","CFTY720D2108","CFTY720D2109","CFTY720D2110","CFTY720D2113","CFTY720D2114","CFTY720D2115","CFTY720D2117","CFTY720D2118","CFTY720D2121","CFTY720D2122","CFTY720D2201","CFTY720D2201E1","CFTY720D2201E1_DMC","CFTY720D2201E2","CFTY720D2205","CFTY720D2301","CFTY720D2301E1","CFTY720D2301_DMC","CFTY720D2302","CFTY720D2302E1","CFTY720D2302_DMC","CFTY720D2306","CFTY720D2306E1","CFTY720D2306_DMC","CFTY720D2309","CFTY720D2309E1","CFTY720D2309_DMC","CFTY720D2311","CFTY720D2311E1","CFTY720D2311_DMC","CFTY720D2312","CFTY720D2316","CFTY720D2319","CFTY720D2320","CFTY720D2324","CFTY720D2325","CFTY720D2399","CFTY720D2399E1","CFTY720D2399P2","CFTY720D2402","CFTY720D2402B","CFTY720D2403","CFTY720D2404","CFTY720D2405","CFTY720D2406","CFTY720D2409","CFTY720DCRS","CFTY720DDE01E1","CFTY720DDE02","CFTY720DDE06","CFTY720DDE07","CFTY720DDE17","CFTY720DES03","CFTY720DFI01","CFTY720DFR03","CFTY720DGB04","CFTY720DGB05","CFTY720DIT01","CFTY720DIT02","CFTY720DIT03","CFTY720DIT07","CFTY720DIT08","CFTY720DRU01","CFTY720DTR01","CFTY720DTR05","CFTY720DTW02","CFTY720DUS01","CFTY720DUS09","CFTY720DUS31","CFTY720D_MAX_TH01","CFTY720D_MAX_TH_01","CFTY720I2201","CGET980AUS01","CGLOBDATCLN","CGLOBSAFMON","CGPN013A2301","CGPN013A2301A1","CGPN013A2301A2","CGPN013A2301A3","CGPN013A2301A4","CGPN013A2302","CGPN017A2301","CGSO769BIA02","CHCD122A2101","CHCD122A2102","CHCD122A2103","CHCD122A2104","CHCD122APRE","CHDM201X1102","CHDM201X2101","CHDM201X2102","CHDM201X2103C","CHGH191A2101","CHSC835X2201","CHSC835X2202","CHSC835X2203","CHSP990A1101","CHSP990A2101","CHTE4002201","CHTF9192201","CHTF9192202","CHTF9192203","CHTF9192204","CHTF9192205","CHTF919A0201","CHTF919A0202","CHTF919A0204","CHTF919A0205","CHTF919A0206","CHTF919A0207","CHTF919A0209","CHTF919A0210","CHTF919A0226","CHTF919A0251","CHTF919A0252","CHTF919A0253","CHTF919A0254","CHTF919A0301","CHTF919A0301E1","CHTF919A0304","CHTF919A0305","CHTF919A0307","CHTF919A0307E1","CHTF919A0351","CHTF919A0355","CHTF919A0357","CHTF919A0358","CHTF919A2101","CHTF919A2102","CHTF919A2103","CHTF919A2201","CHTF919A2201E1","CHTF919A2302","CHTF919A2306","CHTF919A2307","CHTF919A2308","CHTF919A2310","CHTF919A2311","CHTF919A2408","CHTF919A2410","CHTF919A2417","CHTF919A2422","CHTF919A2433","CHTF919ADE01","CHTF919ADE01E1","CHTF919ADE02","CHTF919AFI01","CHTF919AIA01","CHTF919AIA03","CHTF919AIA03E1","CHTF919AIA04","CHTF919AIA04E1","CHTF919AIA06","CHTF919AIA07","CHTF919AIA08","CHTF919AIA10","CHTF919AIA12","CHTF919AIC01","CHTF919ASC","CHTF919ASG01","CHTF919AUS09","CHTF919AUS10","CHTF919AUS11","CHTF919AUS33","CHTF919B0201","CHTF919B0202","CHTF919B2201","CHTF919B2203","CHTF919B2204","CHTF919B2205","CHTF919B2207","CHTF919BUS09","CHTF919BUS10","CHTF919BUS11","CHTF919BUS12","CHTF919BUS26","CHTF919BUS27","CHTF919BUS30","CHTF919D2206","CHTF919D2207","CHTF919D2209","CHTF919D2211","CHTF919D2301","CHTF919D2301E1","CHTF919D2302","CHTF919D2302E1","CHTF919D2305","CHTF919DUS45","CHTF919DUS46","CHTF919E2101","CHTF919E2301","CHTF919E2301E1","CHTF919E2302","CHTF919E2305","CHTF919E2306","CHTF919E2308","CHTF919E2309","CHTF919E2406","CHTF919EUS42","CHTF919EUS49","CHTF919EUS51","CHTF919F2301","CHTF919F2305","CHTF919F2306","CHTF919G2203","CHTF919GB01","CHTF919GUS62","CHTF919N2201","CHTF919N2201E1","CHTF919N2301","CHTF919N2302","CHV_PKC412A001","CICL6700101","CICL6700104","CICL670A0105","CICL670A0105E1","CICL670A0105E2","CICL670A0105F","CICL670A0106","CICL670A0106E1","CICL670A0107","CICL670A0107E1","CICL670A0108","CICL670A0108E1","CICL670A0109","CICL670A0109E1","CICL670A0112","CICL670A0112E1","CICL670A0115","CICL670A0117","CICL670A1101","CICL670A1201","CICL670A2101","CICL670A2102","CICL670A2120","CICL670A2121","CICL670A2122","CICL670A2123","CICL670A2125","CICL670A2126","CICL670A2127","CICL670A2128","CICL670A2129","CICL670A2130","CICL670A2131","CICL670A2201","CICL670A2202","CICL670A2202E1","CICL670A2203","CICL670A2204","CICL670A2204E1","CICL670A2205","CICL670A2206","CICL670A2206E1","CICL670A2209","CICL670A2209E1","CICL670A2214","CICL670A2301","CICL670A2302","CICL670A2402","CICL670A2402E1","CICL670A2409","CICL670A2409E1","CICL670A2409E2","CICL670A2411","CICL670A2417","CICL670A2420","CICL670A2421","CICL670AAU05","CICL670ACN01","CICL670ACN03","CICL670ADE07T","CICL670AIC04","CICL670AIC06","CICL670AIT07","CICL670AIT14","CICL670ATR01","CICL670AUS02","CICL670AUS03","CICL670AUS03E1","CICL670AUS04","CICL670AUS04E1","CICL670AUS22","CICL670AUS32","CICL670AUS38","CICL670AUS47","CICL670AUS49","CICL670E2209","CICL670E2419","CICL670E2422","CICL670F1102","CICL670F2101","CICL670F2102","CICL670F2103","CICL670F2104","CICL670F2105","CICL670F2106","CICL670F2201","CICL670F2202","CICL670F2202_DCF","CICL670F2203","CICL670F2429","CIDD001A2101","CIDD001A2201","CIDD001D2401","CIDD001D2402","CIDH305X2101","CIDH305X2102","CIGE0250006","CIGE0250006E1","CIGE0250007","CIGE0250008","CIGE0250008E1","CIGE0250009","CIGE0250009E1","CIGE0250010","CIGE0250010E1","CIGE0250010E1FU","CIGE0250011","CIGE0250011E1","CIGE0250011E2","CIGE0250011E3","CIGE0250011FU","CIGE0250112","CIGE0250113","CIGE0250114","CIGE0250D01","CIGE0251101","CIGE0252303","CIGE025A0011E1","CIGE025A1101","CIGE025A1301","CIGE025A1303","CIGE025A1304","CIGE025A1305","CIGE025A1306","CIGE025A1307","CIGE025A2102","CIGE025A2203","CIGE025A2204","CIGE025A2204E1","CIGE025A2206","CIGE025A2208","CIGE025A2210","CIGE025A2303","CIGE025A2304","CIGE025A2304FU","CIGE025A2306","CIGE025A2306FU","CIGE025A2313","CIGE025A2416","CIGE025A2425","CIGE025A2428","CIGE025A2432","CIGE025A2433","CIGE025A2437","CIGE025AAR02","CIGE025ABE08","CIGE025ABR01","CIGE025ADE03","CIGE025ADE03EXT7","CIGE025ADE03EXT8","CIGE025ADE05","CIGE025AFR01","CIGE025AFR01E","CIGE025AFR02","CIGE025AFR05","CIGE025AIS","CIGE025ALTO","CIGE025ALTOE1","CIGE025ALTOE2","CIGE025AMX02","CIGE025APRE","CIGE025AQ0572G","CIGE025AQ0619G","CIGE025AQ0626G","CIGE025AQ0637G","CIGE025AQ0709G","CIGE025AQ0716G","CIGE025AQ0723G","CIGE025AQ2736G","CIGE025AQ2948G","CIGE025AQ2982G","CIGE025AQ3623G","CIGE025AQ3662G","CIGE025AQ4229N","CIGE025AQ4465G","CIGE025AQ4577G","CIGE025AQ4777N","CIGE025ATENOR","CIGE025AUS16","CIGE025AUS23","CIGE025AUS33","CIGE025A_CTD","CIGE025A_Q2948G","CIGE025A_QO624G","CIGE025A_QO630G","CIGE025A_QO634G","CIGE025A_QO673G","CIGE025A_QO694G","CIGE025B1301","CIGE025B1301E1","CIGE025BUS26","CIGE025BUS28","CIGE025C2101","CIGE025C2303","CIGE025C2304","CIGE025C_ICATA","CIGE025C_PROSE","CIGE025C_Q4160G","CIGE025E2201","CIGE025E2305","CIGE025E2306","CIGE025E3401","CIGE025ECA01","CIGE025EDE16","CIGE025EFR02","CIGE025EQ4881G","CIGE025EQ4882G","CIGE025EQ4883G","CIGE025EVE01","CIGE025IA04","CIGE025IA04E1","CIGE025IA04E2","CIGE025IA05","CIGE025IA05FU","CIGE0260101","CIGG013A1101J","CIGG013A2301J","CIGG014A2101J","CIGG019A2101J","CILO5220105","CILO5220107","CILO5220108","CILO5220109","CILO5220110","CILO5220112","CILO522A0109","CILO522A2001","CILO522A2001E1","CILO522A2301","CILO522A2328","CILO522A3000","CILO522A3000E1","CILO522A3001","CILO522A3001E1","CILO522A3002","CILO522A3002E1","CILO522A3003","CILO522A3003E1","CILO522A3004","CILO522A3004E1","CILO522A3005","CILO522A3005E1","CILO522AVP3101","CILO522AVP3101E1","CILO522B0210","CILO522D2301","CILO522D2302","CILO522D2401","CILO522D2402","CILO522DUS01","CILO522E2101","CIMDRTEST","CIMM1250102","CINC280X1101","CINC280X2101","CINC280X2101T","CINC280X2102","CINC280X2102_DCF","CINC280X2103","CINC280X2103_DCF","CINC280X2104","CINC280X2105C","CINC280X2106","CINC280X2106_DCF","CINC280X2107","CINC280X2107_DCF","CINC280X2108","CINC280X2201","CINC280X2202","CINC280X2203","CINC280X2204","CINC280X2205","CINC424A1101","CINC424A1102","CINC424A1131","CINC424A1132","CINC424A1133","CINC424A1134","CINC424A1135","CINC424A1136","CINC424A1137","CINC424A1138","CINC424A1139","CINC424A1142","CINC424A2101","CINC424A2102","CINC424A2103","CINC424A2104","CINC424A2105","CINC424A2106","CINC424A2201","CINC424A2202","CINC424A2251","CINC424A2254","CINC424A2255","CINC424A2256","CINC424A2351","CINC424A2352","CINC424A2353","CINC424A2401","CINC424A2401_DCF","CINC424A2407","CINC424A2411","CINC424A2X01B","CINC424AGB02","CINC424AIC01T","CINC424AJP01","CINC424B2001X","CINC424B2301","CINC424B2357","CINC424B2401","CINC424B2401_DCF","CINC424BIC04","CINC424X2101","CINC424X2201","CKAE609A2109","CKAE609A2201","CKAE609A2202","CKAE609X2101","CKAE609X2109","CKAE609X2110","CKAE609X2201","CKAE609X2202","CKAF156X2101","CKAF156X2201","CKAF156X2202","CKCO912P101","CKCO912P102","CKCO912P103","CKCO912P111","CKRP203A2101","CKRP203A2102","CKRP203A2103","CKRP203A2105","CKRP203A2201","CKRP203A2202","CKRP203C101","CLAB6870101","CLAB6870102","CLAF2370101","CLAF2370102","CLAF2370102A1","CLAF2372105","CLAF2372202","CLAF237A-MAX-GUARD","CLAF237A0101","CLAF237A1101","CLAF237A1102","CLAF237A1103","CLAF237A1201","CLAF237A1202","CLAF237A1203","CLAF237A1204","CLAF237A1301","CLAF237A1302","CLAF237A1303","CLAF237A1303E1","CLAF237A1304","CLAF237A1308","CLAF237A1405","CLAF237A2101","CLAF237A2103","CLAF237A2104","CLAF237A2107","CLAF237A2109","CLAF237A2110","CLAF237A2111","CLAF237A2113","CLAF237A2115","CLAF237A2117","CLAF237A2201","CLAF237A2203","CLAF237A2203E1","CLAF237A2204","CLAF237A2204E1","CLAF237A2205","CLAF237A2205E1","CLAF237A2206","CLAF237A2207","CLAF237A2208","CLAF237A2213","CLAF237A2214","CLAF237A2215","CLAF237A2216","CLAF237A2217","CLAF237A2220","CLAF237A2221","CLAF237A2224","CLAF237A2301","CLAF237A2301E1","CLAF237A2303","CLAF237A2303E1","CLAF237A2304","CLAF237A2304E1","CLAF237A2305","CLAF237A2305E1","CLAF237A2307","CLAF237A2307E1","CLAF237A2308","CLAF237A2309","CLAF237A2309E1","CLAF237A2310","CLAF237A23103","CLAF237A23104","CLAF237A2311","CLAF237A23118","CLAF237A23119","CLAF237A2311E1","CLAF237A23135","CLAF237A23137","CLAF237A23137E1","CLAF237A23138","CLAF237A23138E1","CLAF237A23140","CLAF237A23150","CLAF237A23151","CLAF237A23152","CLAF237A23154","CLAF237A23155","CLAF237A23156","CLAF237A2323","CLAF237A2325","CLAF237A2327","CLAF237A2327E1","CLAF237A2329","CLAF237A2329E1","CLAF237A2331","CLAF237A2332","CLAF237A2333","CLAF237A2334","CLAF237A2335","CLAF237A2336","CLAF237A2337","CLAF237A2338","CLAF237A2343","CLAF237A2344","CLAF237A2345","CLAF237A2346","CLAF237A2347","CLAF237A2347A","CLAF237A2349","CLAF237A2351","CLAF237A2352","CLAF237A2353","CLAF237A2354","CLAF237A2355","CLAF237A2357","CLAF237A2366","CLAF237A2367","CLAF237A2368","CLAF237A2378","CLAF237A2379","CLAF237A2381","CLAF237A2384","CLAF237A2386","CLAF237A2387","CLAF237A2388","CLAF237A2389","CLAF237A2395","CLAF237A2398","CLAF237A2401","CLAF237A2403","CLAF237A2411","CLAF237A3119","CLAF237A3421","CLAF237A9101","CLAF237A9105","CLAF237ACO01","CLAF237ADE07","CLAF237ADE08","CLAF237AFR07","CLAF237AGB03","CLAF237AHK01","CLAF237APRE","CLAF237ATW03","CLAF237A_MAX_VIRTUE","CLAF237B2101","CLAF237B2201","CLAF237B2202","CLAF237B2222","CLAF237B2224","CLAF237B2302","CLAG078A2101","CLAG078A2103","CLAQ824A2101","CLAQ824A2102","CLAQ824A2107","CLAV694A0101","CLBH589A1101","CLBH589A2101","CLBH589A2102","CLBH589B1101","CLBH589B1201","CLBH589B2101","CLBH589B2102","CLBH589B2105","CLBH589B2108","CLBH589B2109","CLBH589B2110","CLBH589B2111","CLBH589B2116","CLBH589B2201","CLBH589B2202","CLBH589B2203","CLBH589B2206","CLBH589B2207","CLBH589B2211","CLBH589B2212","CLBH589B2213","CLBH589B2402B","CLBH589BDE04","CLBH589BUS58","CLBH589C2114","CLBH589C2204","CLBH589C2205","CLBH589C2208","CLBH589C2211","CLBH589D1201","CLBH589D2001X","CLBH589D2219","CLBH589D2222","CLBH589D2308","CLBH589D2408","CLBH589DUS106","CLBH589DUS71","CLBH589E2214","CLBH589E2301","CLBH589G2101","CLBH589H1101","CLBH589H2101","CLBH589X2101","CLBH589X2105","CLBH589X2106","CLBH589X2201","CLBM415A2101","CLBM415A2102","CLBM415A2103","CLBM415A2105","CLBM642A2101","CLBM642A2102","CLBM642A2103","CLBM642A2106","CLBM642A2108","CLBM642A2113","CLBM642A2201","CLBM642A2204","CLBM642B2101","CLBM642C2101","CLBQ707A1101","CLBQ707A1103","CLBQ707A1401","CLBQ707A1402","CLBQ707A2005","CLBQ707A2008","CLBQ707A2101","CLBQ707A2105","CLBQ707A2105E1","CLBQ707A4003","CLBY135A2101","CLBY135APRE","CLCF369A2101","CLCI699A2101","CLCI699A2101DCF","CLCI699A2102","CLCI699A2102DCF","CLCI699A2201","CLCI699A2206","CLCI699A2215","CLCI699A2216","CLCI699C1101","CLCI699C1201","CLCI699C2101","CLCI699C2101DCF","CLCI699C2102","CLCI699C2103","CLCI699C2103DCF","CLCI699C2104","CLCI699C2104DCF","CLCI699C2105","CLCI699C2105_DCF","CLCI699C2108","CLCI699C2201","CLCI699C2201A1","CLCI699C2201D","CLCI699C2201D_DCF","CLCI699C2201_DCF","CLCI699C2301","CLCI699C2301_DCF","CLCI699C2302","CLCL161A1102","CLCL161A2101","CLCL161A2104","CLCL161A2105","CLCL161A2201","CLCQ908A1101","CLCQ908A2101","CLCQ908A2102","CLCQ908A2103","CLCQ908A2201","CLCQ908A2203","CLCQ908A2204","CLCQ908A2205","CLCQ908A2207","CLCQ908A2212","CLCQ908A2213","CLCQ908A2214","CLCQ908A2216","CLCQ908B2101","CLCQ908B2102","CLCQ908B2103","CLCQ908B2104","CLCQ908B2107","CLCQ908B2108","CLCQ908B2109","CLCQ908B2110","CLCQ908B2111","CLCQ908B2113","CLCQ908B2116","CLCQ908B2117","CLCQ908B2302","CLCQ908B2305","CLCQ908C2201","CLCQ908C2202","CLCZ696A1101","CLCZ696A1304","CLCZ696A1305","CLCZ696A1306","CLCZ696A2101","CLCZ696A2102","CLCZ696A2103","CLCZ696A2117","CLCZ696A2119","CLCZ696A2120","CLCZ696A2124","CLCZ696A2126","CLCZ696A2201","CLCZ696A2203","CLCZ696A2204","CLCZ696A2205","CLCZ696A2216","CLCZ696A2219","CLCZ696A2219E1","CLCZ696A2222","CLCZ696A2223","CLCZ696A2224","CLCZ696A2315","CLCZ696A2316","CLCZ696A2318","CLCZ696A2319","CLCZ696A2320","CLCZ696A2320E1","CLCZ696B1301","CLCZ696B2008","CLCZ696B2105","CLCZ696B2107","CLCZ696B2109","CLCZ696B2111","CLCZ696B2112","CLCZ696B2113","CLCZ696B2114","CLCZ696B2115","CLCZ696B2116","CLCZ696B2122","CLCZ696B2123","CLCZ696B2125","CLCZ696B2126","CLCZ696B2128","CLCZ696B2130","CLCZ696B2132","CLCZ696B2203","CLCZ696B2207","CLCZ696B2208","CLCZ696B2214","CLCZ696B2223","CLCZ696B2225","CLCZ696B2228","CLCZ696B2229","CLCZ696B2314","CLCZ696B2317","CLCZ696B2319","CLCZ696B2320","CLCZ696B2401","CLCZ696BAR02","CLCZ696BCA02","CLCZ696BDE01","CLCZ696BIE01","CLCZ696D2301","CLCZ696D2302","CLDA215X2101","CLDE225A1102","CLDE225A2107","CLDE225A2108","CLDE225A2110","CLDE225A2112","CLDE225A2113","CLDE225A2114","CLDE225A2117","CLDE225A2118","CLDE225A2120","CLDE225A2201","CLDE225B2101","CLDE225B2105","CLDE225B2203","CLDE225B2204","CLDE225B2209","CLDE225B2209_DCF","CLDE225B2301","CLDE225B2307","CLDE225C2301","CLDE225X1101","CLDE225X1101_DCF","CLDE225X2101","CLDE225X2101_DCF","CLDE225X2103","CLDE225X2104","CLDE225X2104_DCF","CLDE225X2114","CLDE225X2116","CLDE225X2203","CLDE225XUS20","CLDJ824A2103","CLDK378A1201","CLDK378A2101","CLDK378A2103","CLDK378A2104","CLDK378A2105","CLDK378A2106","CLDK378A2107","CLDK378A2108","CLDK378A2109","CLDK378A2110","CLDK378A2112","CLDK378A2113","CLDK378A2116C","CLDK378A2120C","CLDK378A2121","CLDK378A2122","CLDK378A2201","CLDK378A2202","CLDK378A2203","CLDK378A2205","CLDK378A2301","CLDK378A2303","CLDK378A2402","CLDK378A2407","CLDK378A2X01B","CLDK378B2208","CLDK378X1101","CLDK378X2101","CLDK378X2101_DCF","CLDK378X2102","CLDK378X2103","CLDK378X2103_DCF","CLDT600A2104","CLDT600A2202","CLDT600A2301","CLDT600A2303","CLDT600A2303A1","CLDT600A2304","CLDT600A2306","CLDT600A2404","CLDT600A2405","CLDT600A2406","CLDT600A2407","CLDT600A2409","CLDT600A2410","CLDT600A2414","CLDT600ACN03","CLDT600ACN04","CLDT600ACN04E1","CLDT600AEP01","CLDT600AES01","CLDT600AKR02","CLDT600AUS03","CLDT600AUS06","CLED243X2101","CLEE011A2101","CLEE011A2101_DCF","CLEE011A2102","CLEE011A2102_DCF","CLEE011A2103","CLEE011A2103_DCF","CLEE011A2106","CLEE011A2106_DCF","CLEE011A2109","CLEE011A2109_DCF","CLEE011A2111","CLEE011A2111_DCF","CLEE011A2112C","CLEE011A2115C","CLEE011A2116","CLEE011A2117","CLEE011A2201","CLEE011A2206","CLEE011A2301","CLEE011A2404","CLEE011AUS42","CLEE011X1101","CLEE011X2101","CLEE011X2101_DCF","CLEE011X2102","CLEE011X2105","CLEE011X2106","CLEE011X2106_DCF","CLEE011X2107","CLEE011X2107_DCF","CLEE011X2108","CLEE011X2108_DCF","CLEE011X2110C","CLEE011X2201","CLEE011X2X01B","CLEE011XUS03","CLEE011XUS29","CLEH819A2201","CLEQ506X2101","CLEX123ABA001","CLEX123ABA004","CLEX123ABA016","CLEX123ABA030","CLEX123ABA063","CLEX123ABA1753P","CLEX123ABA1855P","CLEX123ABA451","CLEX123ABA5060P","CLEX123ABAF01","CLEX123ABAF02","CLEX123J1201","CLEX123J1202","CLEX123J1203","CLEX123J1301","CLEX123JCLSE01_POOLED","CLEX123JIS_POOLED","CLEZ763A2201","CLEZ763X1101","CLEZ763X2201","CLFA102X1101","CLFA102X2101","CLFA102X2102","CLFF269X2101","CLFF269X2105","CLFF269X2201","CLFF571X2101","CLFF571X2201","CLFF571X2201C2","CLFG316A2101","CLFG316A2102","CLFG316A2201","CLFG316A2202","CLFG316A2203","CLFG316A2203PB","CLFG316A2204","CLFG316X2201","CLFG316X2202","CLFX453X2101","CLFX453X2201","CLFX453X2202","CLGC759X2101","CLGC759X2102","CLGH447X1101","CLGH447X2101","CLGH447X2102","CLGH447X2103C","CLGH447X2104C","CLGK974X2101","CLGK974X2102C","CLGT209X1101","CLGT209X2101","CLGT209X2105","CLGX818B2201","CLGX818X2101","CLGX818X2102","CLGX818X2103","CLGX818X2109","CLGY544X2101","CLHA510X2201A","CLHA510X2202A","CLHT344A2101","CLHW090X2101","CLHW090X2102","CLHW090X2202","CLIC477D2201","CLIC477D2203","CLIC477D2204","CLIC477D2205","CLIC477D2206","CLIC477D2301","CLIC477D2301E1","CLIC477D2302","CLIC477D2302E1","CLIC477D2303","CLIC477D2303E1","CLIC477D2306","CLIC477D2310","CLIC477D2311","CLIK066A2201","CLIK066A2202","CLIK066X1101","CLIK066X2101","CLIK066X2201","CLIK066X2204","CLIK066X2205","CLINXPORT","CLINXPORT_OQ","CLINXPORT_PQ","CLIS025A2301","CLIS025A2302","CLIS0500201","CLIS0500202","CLIS0500301","CLIS0500302","CLIS0500303","CLIS0500304","CLIS050C0301","CLIS050C301E1","CLIV1212201","CLIV1212202","CLIV1212301","CLJE704X2101","CLJM716X1101","CLJM716X2101","CLJM716X2102","CLJM716X2103","CLJM716X2104","CLJN295X2101","CLJN452X1101","CLJN452X2101","CLJN452X2102","CLJN452X2201","CLJN452X2202","CLLF580X2101","CLLG783X2101","CLLG783X2201","CLMB763X2101","CLMB763X2201","CLME636X2101A","CLME636X2201A","CLME636X2202","CLME636X2202A","CLMF237A1101","CLMF237A1102","CLMF237A1301","CLMF237A1303","CLMF237A2101","CLMF237A2103","CLMF237A2106","CLMF237A2107","CLMF237A2301","CLMF237A2302","CLMF237A2302S1","CLMF237A2303","CLMF237A2304","CLMF237A2307","CLMF237A2309","CLMF237ADE02","CLMI070X2201","CLML134X2101","CLML134X2103","CLML134X2201","CLOP628X2101","CLO_OR1_SCH_PH2X_01","CLO_OR1_SCH_PH2X_01_E","CLTB0190038","CLTB0190101","CMCS110A2101","CMCS110X2101","CMCS110X2201","CMEK162A2101J","CMEK162A2102","CMEK162A2103","CMEK162A2104","CMEK162A2105","CMEK162A2106","CMEK162A2109","CMEK162A2110","CMEK162A2301","CMEK162A2301_DCF","CMEK162AUS11","CMEK162B2301","CMEK162X1101","CMEK162X1101_DCF","CMEK162X2101","CMEK162X2102","CMEK162X2103","CMEK162X2104","CMEK162X2106","CMEK162X2108","CMEK162X2109","CMEK162X2110","CMEK162X2111","CMEK162X2112J","CMEK162X2112J_CDISC","CMEK162X2114","CMEK162X2116","CMEK162X2201","CMEK162X2201_DCF","CMEK162X2203","CMEK162Y2201","CMEN0010201","CMEN0010201E1","CMEN0010305","CMEN0010305E1","CMEN0010307","CMEN0010451","CMEN0010452","CMFF258A4073","CMFF258A4139","CMFF258A4334","CMFF258A4431","CMFF258A4703","CMFF258A4705","CMFF258C2201","CMFF258C2204","CMID001A2102","CMIO2902301","CMONOCLOAB","CMSP7710001","CMSP7710002","CMSP7710002E1","CMSP7710003","CMSP7710003E1","CMSP7710004","CMSP7710004E1","CMSP7710005","CMSP7710005E1","CMYC123A2201","CMYC123A2202","CMYC123A2301","CMYC123A2302","CMYC123A2303","CMYC123A2304","CNAB1160001","CNDD094B0151","CNIC002A2201","CNIC002A_NICQB01","CNIC002A_NICQB02","CNIC002A_NICQB03","CNIC002A_NICQB04","CNIM811B2101","CNIM811B2102","CNIM811B2202","CNKP6080102","CNKP6080103","CNKP6080106","CNKP6080107","CNKP6080108","CNKP6080109","CNKP6080109E1","CNKP6080110","CNKP6080110E1","CNKP6080111","CNKP6080111E1","CNKP6080112","CNKP6080112E1","CNKP6080131","CNKP6080132","CNKP6080141","CNKP6080152","CNKP6080152E1","CNKP6080153","CNKP6080153E1","CNKP6080154","CNKP6080154E1","CNKP6080155","CNKP6080155E1","CNKP6080156","CNKP6080157","CNKP6080158","CNKP6080301","CNKP6080302","CNKP6080303","CNKP6080304","CNKP6080306","CNKP6080307","CNKP6080308","CNKP6080309","CNKP6080310","CNKP6080311","CNKP6080312","CNKP6080314","CNKP6080315","CNKP6080316","CNKP6080317","CNKS104A0101","CNKS104A0119","CNKS104A0202","CNKS104A0203","CNKS104A2113","CNKS104A2114","CNKS104A2115","CNKS104A2204","CNKS104A2205","CNKS104A2205E1","CNMC283A0001","CNMC283A0002","CNMC283A0003","CNMC283A0004","CNMC283A0005","CNMC283A0006","CNMC283A0007","CNMC283A0008","CNVA237A1302","CNVA237A1302COF","CNVA237A2103","CNVA237A2104","CNVA237A2105","CNVA237A2107","CNVA237A2108","CNVA237A2109","CNVA237A2110","CNVA237A2205","CNVA237A2206","CNVA237A2207","CNVA237A2208","CNVA237A2303","CNVA237A2304","CNVA237A2306","CNVA237A2307","CNVA237A2309","CNVA237A2310","CNVA237A2311","CNVA237A2313","CNVA237A2314","CNVA237A2316","CNVA237A2317","CNVA237A2318","CNVA237A2319","CNVA237A2320","CNVA237A2403","CNVA237A3401","CNVA237ACH01","CNVA237ADE02","CNVA237APAD237001","CNVA237APAD237002","CNVA237APAD237003","CNVA237APAD237004","CNVA237APAD237005","CNVA237B2301","CNYO1422301","CNYO1422302","COEB071X2101","COEB071X2102","COEB071X2103","COLO4002414","COLO400A2412","COLO400A2416","COLO400A2419","COLO400A2421","COLO400A2425","COLO400A2426","COLO400A2430","COLO400ADE01","COLO400AIA01","COLO400AIA02","COLO400AIT03","COLO400ANL07","COLO400ANO01","COLO400ATW04","COLO400AUS05","COLO400B1303","COLO400BS16","COLO400CIT04","COLO400CS2008","COLO400CS651","COLO400CS652","COLO400CS653","COLO400CS654","COLO400D1301","COLO400D1302","COLO400D1303","COLO400D1304","COLO400DIS_POOLED","COLO400DOL10085","COLO400DOL8095","COLO400DOLR302","COLO400DSIM79","COLO400DSIM80","COLO400DSIMAD543345","COLO400DSIMSF04","COLO400IN05","COLO400IN06","COLO400IN07","COLO400N351","COLOPATADINE","COMB157G1301","COMB157G2301","COMB157G2302","COMS112831GSK","COMS115102GSK","CPCTNDS001","CPCTNDS004","CPCTNDS005","CPCTNDS006","CPCTNDS007","CPCTNDS008","CPCTNDS009","CPCTNDS011","CPCTNDS012","CPCTNDS014","CPCTNDS018","CPCTNDS019","CPCTNDS2347","CPJMR0012101","CPJMR0012103","CPJMR0012104","CPJMR0012105","CPJMR0012106","CPJMR0012107","CPJMR0012201","CPJMR0022103","CPJMR0022105","CPJMR0022106","CPJMR0022107","CPJMR0022108","CPJMR0022109","CPJMR0032101","CPJMR0032103","CPJMR0032105","CPJMR0032106","CPJMR0032107","CPJMR0032108","CPJMR0052101","CPJMR0052102","CPJMR0052104","CPJMR0052107","CPJMR0052109","CPJMR0052203","CPJMR0052208","CPJMR0062101","CPJMR0062108","CPJMR0062109","CPJMR0062112","CPJMR0062113","CPJMR0062201","CPJMR0062203","CPJMR0092103","CPJMR0092105","CPJMR0092202","CPKC4120002","CPKC4120004","CPKC4120006","CPKC4120011","CPKC4120012","CPKC4120013","CPKC4120014","CPKC4122101","CPKC4122201","CPKC412A1101","CPKC412A2001X","CPKC412A2103","CPKC412A2104","CPKC412A2104E1","CPKC412A2104E2","CPKC412A2106","CPKC412A2107","CPKC412A2108","CPKC412A2109","CPKC412A2110","CPKC412A2111","CPKC412A2112","CPKC412A2113","CPKC412A2114","CPKC412A2116","CPKC412A2117","CPKC412A2120","CPKC412A2207","CPKC412A2211","CPKC412A2213","CPKC412A2218","CPKC412A2220","CPKC412A2222","CPKC412A2301","CPKC412A2408","CPKC412AUS23","CPKC412AUS56X","CPKC412D2201","CPKI1660101","CPKI1660102","CPKI166A0105","CPKI166A2107","CPLATFRM2101","CPLATFRM2102","CPLATFRM2104","CPLATFRM2105","CPLATFRM2106","CPLATFRM2107","CPLATFRM2201","CPLTNDS101","CPMX622B201","CPNT1002201","CPRL002A2201","CPRO400A2201","CPRT128A04101","CPRT128A05102","CPRT128A06104","CPRT128A06105","CPRT128A06106","CPRT128A06107","CPRT128A06108","CPRT128A06109","CPRT128A06110","CPRT128A06111","CPRT128A07112","CPRT128A07113","CPRT128A07114","CPRT128A07116","CPRT128A08118","CPRT128A09120","CPRT128A11INV102","CPRT128A2101","CPRT128A2102","CPRT128A2104","CPRT128A2105","CPRT128A2106","CPRT128A2107","CPRT128A2108","CPRT128A2110","CPRT128A2115","CPRT128A2116","CPRT128A2118","CPRT128A2120","CPRT128A2123","CPRT128A2126","CPRT128A2201","CPRT128A2202","CPRT128A2301","CPRT128A2302","CPSCB3510351","CPTH134A2101","CPTH134A2102","CPTH134A2105","CPTK7870101","CPTK7870102","CPTK7870103","CPTK7870105","CPTK7870106","CPTK7870107","CPTK7870108","CPTK7870109","CPTK7870112","CPTK7870113","CPTK7870114","CPTK7870116","CPTK7870117","CPTK7870118","CPTK7870130","CPTK7870133","CPTK7870133E1","CPTK7870134","CPTK7870135","CPTK7870135E1","CPTK7870136","CPTK7870144","CPTK787A2115","CPTK787A2116","CPTK787A2118","CPTK787A2120","CPTK787A2121","CPTK787E2201","CPTK796A1101","CPTK796A2101","CPTK796A2102","CPTK796A2103","CPTK796A2104","CPTK796A2201","CPTK796A2202","CPTK796A2203","CPTK796A2301","CPTK796A2302","CPTK796A2303","CPTK796A2304","CPTK796A2305","CPTK796A2306","CPTZ601A0002","CPTZ601A0003","CPTZ601A2102","CPTZ601A2103","CPTZ601A2104","CPTZ601A2105","CPTZ601A2201","CPTZ601A2301","CPTZ601A2302","CPTZ601A2303","CPTZ601A2304","CPTZ601A2305","CPTZ601A2306","CPTZ601A2307","CPTZ601A2308","CPZ-601-02","CPZ-601-03","CQAB149A1101","CQAB149A1201","CQAB149A1202","CQAB149A2101","CQAB149A2103","CQAB149A2105","CQAB149A2106","CQAB149A2201","CQAB149A2205","CQAB149A2206","CQAB149A2208","CQAB149A2210","CQAB149A2211","CQAB149A2214","CQAB149A2215","CQAB149A2216","CQAB149A2217","CQAB149A2218","CQAB149A2219","CQAB149A2221","CQAB149A2222","CQAB149A2223","CQAB149A2224","CQAB149A2228","CQAB149A2307","CQAB149A2311","CQAB149A2318","CQAB149B1201","CQAB149B1202","CQAB149B1302","CQAB149B1303","CQAB149B1303COF","CQAB149B2101","CQAB149B2102","CQAB149B2103","CQAB149B2106","CQAB149B2107","CQAB149B2108","CQAB149B2201","CQAB149B2202","CQAB149B2205","CQAB149B2205E1","CQAB149B2208","CQAB149B2211","CQAB149B2212","CQAB149B2213","CQAB149B2214","CQAB149B2215","CQAB149B2216","CQAB149B2217","CQAB149B2218","CQAB149B2219","CQAB149B2220","CQAB149B2222","CQAB149B2223","CQAB149B2305","CQAB149B2307","CQAB149B2311","CQAB149B2312","CQAB149B2317","CQAB149B2318","CQAB149B2319","CQAB149B2331","CQAB149B2333","CQAB149B2334","CQAB149B2335S","CQAB149B2335SE","CQAB149B2336","CQAB149B2337","CQAB149B2338","CQAB149B2339","CQAB149B2340","CQAB149B2341","CQAB149B2346","CQAB149B2348","CQAB149B2349","CQAB149B2350","CQAB149B2351","CQAB149B2353","CQAB149B2354","CQAB149B2355","CQAB149B2356","CQAB149B2357","CQAB149B2358","CQAB149B2401","CQAB149B2425","CQAB149B2433","CQAB149BAR01","CQAB149BBE02","CQAB149BBR02","CQAB149BCN01","CQAB149BDE01","CQAB149BHK02","CQAB149BIL01","CQAB149BIT01","CQAB149BKR01","CQAB149BUS01","CQAB149BUS02","CQAB149B_MAX_INFLOW","CQAB149C2101","CQAB149C2202","CQAB149D2301","CQAD171A2101","CQAD171A2102","CQAD706A2101","CQAD706A2103","CQAE397A2101","CQAE397A2202","CQAF805A2101","CQAF805A2102","CQAK423A2101","CQAL964B2101","CQAL964B2201","CQAN746A2101","CQAP642A2101","CQAT370A2101","CQAT370A2102","CQAT370A2103","CQAU145A2101","CQAU145A2201","CQAV680A2101","CQAV680A2102","CQAV680A2102E1","CQAV680A2201","CQAV680A2201E1","CQAV680A2202","CQAV680A2204","CQAW039A1101","CQAW039A1101DCF","CQAW039A2101","CQAW039A2101DCF","CQAW039A2102","CQAW039A2102DCF","CQAW039A2104","CQAW039A2107","CQAW039A2108","CQAW039A2111","CQAW039A2113","CQAW039A2113DCF","CQAW039A2114","CQAW039A2116","CQAW039A2120","CQAW039A2121","CQAW039A2125","CQAW039A2201","CQAW039A2206","CQAW039A2208","CQAW039A2212","CQAW039A2214","CQAW039A2307","CQAW039A2314","CQAW039A2315","CQAW039A2316","CQAW039A2317","CQAW039A2319","CQAW039X2201","CQAX028A2101","CQAX028A2102","CQAX028A2103","CQAX028A2201","CQAX576A1101","CQAX576A2101","CQAX576A2103","CQAX576A2104","CQAX576A2106","CQAX576A2107","CQAX576A2201","CQAX576A2202","CQAX576A2203","CQAX576A2205","CQAX576A2206","CQAX576A2207","CQAX576A2209","CQAX576APRE","CQAX935A2101","CQAZ275A2101","CQBK271X2101","CQBM076X2101","CQBM076X2203","CQBM076X2203P2","CQBW251X2101","CQBW251X2102","CQBW251X2201","CQBW276X2101","CQBW276X2201","CQBX258X2101","CQBX258X2201","CQCC374X2101","CQCC374X2201","CQCF281X2101","CQCF281X2101P2","CQGE031A1101","CQGE031A2102","CQGE031A2103","CQGE031A2208","CQGE031A2208E1","CQGE031B2101","CQGE031B2201","CQGE031B2201E1","CQGE031B2203","CQGE031B2204","CQGE031X2201","CQGE031X2202","CQMF149A2101","CQMF149A2102","CQMF149A2201","CQMF149A2202","CQMF149A2203","CQMF149A2204","CQMF149A2206","CQMF149A2210","CQMF149A2212","CQMF149B2201","CQMF149E1101","CQMF149E2101","CQMF149E2102","CQMF149E2201","CQMF149E2203","CQMF149F2202","CQTI571A2102","CQTI571A2102E1","CQTI571A2301","CQTI571A2301E1","CQTI571A2314","CQTI571AJP01","CQVA149A1101","CQVA149A1301","CQVA149A1301COF","CQVA149A2101","CQVA149A2103","CQVA149A2104","CQVA149A2105","CQVA149A2106","CQVA149A2107","CQVA149A2109","CQVA149A2202","CQVA149A2203","CQVA149A2204","CQVA149A2206","CQVA149A2210","CQVA149A2301","CQVA149A2302","CQVA149A2303","CQVA149A2304","CQVA149A2305","CQVA149A2306","CQVA149A2307","CQVA149A2308","CQVA149A2313","CQVA149A2316","CQVA149A2318","CQVA149A2322","CQVA149A2325","CQVA149A2326","CQVA149A2327","CQVA149A2328","CQVA149A2331","CQVA149A2334","CQVA149A2335","CQVA149A2336","CQVA149A2337","CQVA149A2339","CQVA149A2340","CQVA149A2341","CQVA149A2349","CQVA149A2350","CQVA149A2405","CQVA149A3401","CQVA149A3405","CQVA149ACA01","CQVA149ADE01","CQVA149ADE03","CQVA149ADE04","CQVA149ADE05","CQVA149AKR01","CQVA149ANO01","CRAD0010101","CRAD0012201","CRAD001A1101","CRAD001A1202","CRAD001A1202E1","CRAD001A1202E1COF","CRAD001A2203","CRAD001A2301","CRAD001A2302","CRAD001A2303","CRAD001A2304","CRAD001A2306","CRAD001A2306E1","CRAD001A2307","CRAD001A2307E1","CRAD001A2308","CRAD001A2309","CRAD001A2310","CRAD001A2310M24","CRAD001A2314","CRAD001A2401","CRAD001A2403","CRAD001A2405","CRAD001A2407","CRAD001A2408","CRAD001A2409","CRAD001A2410","CRAD001A2411","CRAD001A2412","CRAD001A2413","CRAD001A2415","CRAD001A2418","CRAD001A2419","CRAD001A2420","CRAD001A2421","CRAD001A2423","CRAD001A2424","CRAD001A2426","CRAD001A2429","CRAD001A2429M24","CRAD001A2433","CRAD001ADE02","CRAD001ADE12","CRAD001ADE13","CRAD001ADE14","CRAD001ADE19","CRAD001ADE36","CRAD001ADE44","CRAD001AES07","CRAD001AFR06","CRAD001AFR10","CRAD001AIA01","CRAD001AIA06","CRAD001AIA11","CRAD001AIC01","CRAD001AIL03","CRAD001AIT01","CRAD001AIT02","CRAD001AIT02E1","CRAD001AIT12","CRAD001AIT16","CRAD001AIT25","CRAD001ANO02","CRAD001ASE01","CRAD001AUS08","CRAD001AUS09","CRAD001AUS92","CRAD001B151","CRAD001B152","CRAD001B156","CRAD001B157","CRAD001B158","CRAD001B159","CRAD001B201","CRAD001B251","CRAD001B253","CRAD001B257","CRAD001B258","CRAD001B351","CRAD001C1101","CRAD001C1104","CRAD001C1201","CRAD001C1X01B","CRAD001C2101","CRAD001C2102","CRAD001C2104","CRAD001C2106","CRAD001C2107","CRAD001C2108","CRAD001C2111","CRAD001C2112","CRAD001C2114","CRAD001C2116","CRAD001C2118","CRAD001C2119","CRAD001C2120","CRAD001C2121","CRAD001C2206","CRAD001C2207","CRAD001C2222","CRAD001C2235","CRAD001C2240","CRAD001C2241","CRAD001C2242","CRAD001C2324","CRAD001C2325","CRAD001C2410","CRAD001C2485","CRAD001C2X01B","CRAD001CDE16","CRAD001D2201","CRAD001F2201","CRAD001G2101","CRAD001H2301","CRAD001H2304","CRAD001H2304E1","CRAD001H2304M24","CRAD001H2304OLD","CRAD001H2305","CRAD001H2307","CRAD001H2307E1","CRAD001H2307M24","CRAD001H2401","CRAD001HDE10","CRAD001HES01","CRAD001HFR02","CRAD001HIT33","CRAD001HIT34","CRAD001IA06","CRAD001J2101","CRAD001J2101B","CRAD001J2101E1","CRAD001J2102","CRAD001J2102E1","CRAD001J2301","CRAD001JDE49","CRAD001JUS226","CRAD001K24133","CRAD001K24133E1","CRAD001L2101","CRAD001L2201","CRAD001L2202","CRAD001L2401","CRAD001L2404","CRAD001LDE43","CRAD001LFR08","CRAD001M2301","CRAD001M2301E1","CRAD001M2302","CRAD001M2302E1","CRAD001M2304","CRAD001M2305","CRAD001M2401","CRAD001M2X02B","CRAD001MIC02","CRAD001MIC03","CRAD001MIC03RP1","CRAD001MIC03RP2","CRAD001MIC03RP3","CRAD001MIC03RP4","CRAD001MIC03RP5","CRAD001MIC03RP6","CRAD001MIL04T","CRAD001MJP04","CRAD001N2201","CRAD001N2202","CRAD001N2301","CRAD001NUS65","CRAD001O2101","CRAD001O2301","CRAD001OHK02","CRAD001PCN31","CRAD001PGB12","CRAD001R2301","CRAD001T2302","CRAD001W102","CRAD001W105","CRAD001W107","CRAD001W2301","CRAD001W301","CRAD001W302","CRAD001W303","CRAD001X2101","CRAD001X2102","CRAD001X2103","CRAD001X2104","CRAD001X2105","CRAD001X2106","CRAD001X2109","CRAD001X2110","CRAD001X2111","CRAD001X2112","CRAD001X2113","CRAD001X2114","CRAD001X2115","CRAD001X2201","CRAD001X2202","CRAD001Y2201","CRAD001Y2202","CRAD001Y2301","CRAD001Y24135","CRAD001YGB01","CRAD001YGB11","CRAD001YIC04","CRAD001ZDE12","CRAD002X2202","CRAD002X2202E1","CRAF265A2101","CRAF265A2105","CRFB0022101","CRFB0022102","CRFB0022201","CRFB0022202","CRFB0022203","CRFB0022301","CRFB0022302","CRFB0022303","CRFB0022304","CRFB0022305","CRFB002A1201","CRFB002A2203","CRFB002A2301","CRFB002A2302","CRFB002A2303","CRFB002A2304","CRFB002A2401","CRFB002A2402","CRFB002A2405","CRFB002A2406","CRFB002A2407","CRFB002A2411","CRFB002A2412","CRFB002A2412_24M","CRFB002A2413","CRFB002AAU15","CRFB002ACN07","CRFB002ADE23","CRFB002ADE27","CRFB002AGB10","CRFB002AGB18","CRFB002AGB19","CRFB002AIT02","CRFB002AMAXKR01","CRFB002ANCT00454389","CRFB002ATR01","CRFB002A_MAXAU_01","CRFB002A_MAX_AU_01","CRFB002A_MAX_IL01","CRFB002A_MAX_IL_01","CRFB002A_MAX_UNVEIL","CRFB002B2201","CRFB002C2201","CRFB002D2201","CRFB002D2301","CRFB002D2301E1","CRFB002D2303","CRFB002D2303_CHINA","CRFB002D2304","CRFB002D2305","CRFB002D2404","CRFB002DCA05","CRFB002DCL01","CRFB002DDE13","CRFB002DDE26","CRFB002DDRCR","CRFB002DES01","CRFB002DFR11","CRFB002DGB14","CRFB002E2301","CRFB002E2302","CRFB002E2303","CRFB002E2401","CRFB002E2402","CRFB002EDE17","CRFB002EDE18","CRFB002EDE20","CRFB002EFVF4165G","CRFB002EFVF4166G","CRFB002F2301","CRFB002F2302","CRFB002F2401","CRFB002FIT01","CRFB002G2301","CRFB002G2301_12M","CRFB002G2302","CRFB002G2302_12M","CRFB002GFR02","CRID124E2103","CRIT1240001","CRIT1240002","CRIT1240003","CRIT1240004","CRIT1240006","CRIT1240007","CRIT1240007E1","CRIT1240009","CRIT124D2201","CRIT124D2201E1","CRIT124D2302","CRIT124D2302E1","CRIT124DAU01","CRIT124DDE01","CRIT124DDE02","CRIT124DDE04","CRIT124DUS02","CRIT124DUS05","CRIT124DUS06","CRIT124DUS07","CRIT124E2101","CRIT124E2102","CRIT124E2103","CRIT124E2301","CRIT124E2302","CRIT124E2302E01","CRIT124E2305","CRIT124E2401","CRIT124EC001","CRIT124EC991","CRIT124ECM01","CRIT124ECM02","CRIT124ECM03","CRIT124ECM04","CRIT124ECM05","CRIT124ECM06","CRIT124EUS08","CRIT124EUS09","CRIT124EUS12","CRIT124EUS13","CRIT124EUS19","CRIT124EUS21","CRIT124RI38","CRKI983A2101","CRKI983A2201","CRLX030A1201","CRLX030A2101","CRLX030A2102","CRLX030A2103","CRLX030A2201","CRLX030A2202","CRLX030A2203","CRLX030A2205","CRLX030A2208","CRLX030A2209","CRLX030A2210","CRLX030A2211","CRLX030A2301","CRLX030A2302","CRLX030A2303","CRLX030A3301","CRLX030AC002","CRLX030AC003","CRLX030AC004","CRLX030AC005","CRLX030AC006","CRLX030ACHF001","CRLX030ACHF002","CRLX030ACHF003","CRLX030ACHF003B","CRLX030ACR001","CRLX030AFMC001","CRLX030AIFC001","CRLX030AORTHO001","CRLX030APE001","CRLX030AR0006G","CRLX030AR0187G","CRLX030AR0279G","CRLX030AR0282G","CRLX030AR9401","CRLX030X2201","CRSC0212101","CRSC0212102","CRSC0212103","CRSC0212104","CRSC0212201","CRSC0212202","CRSC0212203","CRSC0212204","CRSC0212205","CRSC0212206","CRSC0212207","CRSC0212208","CRSC0212209","CRSC0212301","CRSC0212302","CRSC0212303","CRSC0212304","CRSC0212305","CRSC0212306","CRSC0212307","CRSC0212308","CRSC0212309","CRSC0212310","CRSC0212311","CRSC0212312","CRSC0212313","CRSC0212314","CRSC0212315","CRSC0212316","CRSC0212401","CRSC0212402","CRSV604A2101","CRSV604A2102","CRSV604A2103","CRSV604A2203","CRUF3310001","CRUF3310002","CRUF3310003","CRUF3310004","CRUF3310014","CRUF3310015","CRUF3310016","CRUF3310016E1","CRUF3310018","CRUF3310018E1","CRUF3310021","CRUF3310021E1","CRUF3310022","CRUF3310022E1","CRUF3310027","CRUF3310027E1","CRUF3310029","CRUF3310031","CRUF3310036","CRUF3310037","CRUF3310038","CRUF3310038E1","CRUF3310039","CRUF3310039E","CRUF3310039E1","CRUF3310101","CRUF3310102","CRUF3310104","CRUF3310105","CRUF3310201","CRUF331A2301","CRUF331A237","CRUF331AEET1","CRUF331AEET1E","CRUF331AEMD2","CRUF331AEPT2","CRUF331AIND","CSAA164A2103","CSAB378A2101","CSAB378A2102","CSAB378A2103","CSAB378A2106","CSAB378A2201","CSAB378A2202","CSAB953A2101","CSAB953B2101","CSAD448A2101","CSAD448B2101","CSAF312A2102","CSAF312A2103","CSAF312A2105","CSAF312A2202","CSAF312X2101","CSAF312X2201","CSAH100A2101","CSAH100A2102","CSAH100A2104","CSAH100A2105","CSAH100A2110","CSAH100A2111","CSAH100A2301","CSAH100A2302","CSAM4860001","CSAM4860002","CSAM4860003","CSAM4860004","CSAM4860006","CSAM4860007","CSAM4860008","CSAM4860009","CSAM4860010","CSAM4860011","CSAM4860012","CSAM4860013","CSAM4860014","CSAM4860015","CSAM4860021","CSAM4860022","CSAM486A009","CSAM486A013","CSAM486P01","CSBR759A2101","CSBR759A2102","CSBR759A2104","CSBR759A2106","CSBR759A2201","CSBR759A2202","CSBR759A2304","CSBR759A2305","CSCOPOLAMINE","CSCRPROD0001","CSCRPROD0002","CSCRPROD0003","CSCRPROD0004","CSCRPROD0005","CSCRPROD0006","CSCRPROD0007","CSCRPROD0008","CSCRPROD0009","CSCRPROD0010","CSFO3270303","CSFO327AIN06","CSFO327AIN07","CSFO327AIN08","CSFO327AIN09","CSFO327C2101","CSFO327C2201","CSFO327C2301","CSFO327C2302","CSFO327C2303","CSFO327EIN03","CSFO327EIN04","CSFO327I203","CSFO327I205","CSFO327I208","CSFO327I255","CSFO327I257","CSFO327K2401","CSFO327KUS13","CSFO327KUS14","CSFO327L2101","CSFO327L2102","CSFO327L2104","CSFO327L2302","CSFO327L2302E1","CSFO327L2303","CSFO327L2306","CSFO327M1101","CSFO327M2101","CSFO327M2102","CSFO327M2106","CSFO327M2302","CSFO327N2101","CSFO327N2102","CSFO327N2103","CSFO327N2201","CSFO327N2301","CSFO327N2302","CSFO327N2303","CSFO327T201","CSFO327T202","CSFO327W157","CSFO327W352","CSMC0210101","CSMC0210102","CSMC0210104","CSMC0210105","CSMC021A2103","CSMC021A2106","CSMC021A2107","CSMC021A2108","CSMC021A2109","CSMC021A2110","CSMC021A2111","CSMC021A2112","CSMC021A2207","CSMC021A2209","CSMC021A2210","CSMC021A2211","CSMC021A2212","CSMC021A2213","CSMC021A2303","CSMC021C2102","CSMC021C2103","CSMC021C2104","CSMC021C2208","CSMC021C2301","CSMC021C2302","CSMC021C2303","CSMC021C2307","CSMC0510108","CSMC0510211","CSMC0510310","CSMC0510311","CSMC0510311E1","CSMC0510312","CSMC0510312E1","CSMC0510314","CSMC0510320","CSMC0510321","CSMC051A2402","CSMC051A2406","CSMC051AIA01","CSMS9950802","CSMS9950804","CSMS9952408","CSMS9952409","CSMS995A2101","CSMS995A2403","CSMS995ADE05","CSMS995AFR08","CSMS995AHU02","CSMS995AIC04","CSMS995B1201","CSMS995B2401","CSMS995B2402","CSMS995B2403","CSMS995B2403E1","CSMS995B2406","CSMS995BIC03","CSMS995BIT12","CSMS995C0201E00","CSMS995C0201E01","CSMS995C0201E02","CSMS995C0201E03","CSMS995C0201E04","CSMS995C0202E00","CSMS995C0202E01","CSMS995C0202E02","CSMS995C0202E03","CSMS995E351","CSMS995E451","CSMS995H0802E","CSMS995H0804E","CSMS995K2101","CSMS995K272","CSMS995L2102","CSMS995L2103","CSMS995L2106","CSMS995M2101","CSMS995M2102","CSMS995M2103","CSMS995M2201","CSMS995M2301","CSMS995Z2101","CSMT487A103","CSMT487A2201","CSMT487A2202","CSMT487B102","CSMT487B151","CSOM230B2101","CSOM230B2102","CSOM230B2103","CSOM230B2106","CSOM230B2107","CSOM230B2108","CSOM230B2112","CSOM230B2113","CSOM230B2114","CSOM230B2116","CSOM230B2124","CSOM230B2125","CSOM230B2126","CSOM230B2127","CSOM230B2128","CSOM230B2201","CSOM230B2201E1","CSOM230B2201E2","CSOM230B2202","CSOM230B2202E1","CSOM230B2208","CSOM230B2208E1","CSOM230B2208E2","CSOM230B2216","CSOM230B2219","CSOM230B2301","CSOM230B2305","CSOM230B2305E1","CSOM230B2305E2","CSOM230B2305E3","CSOM230B2305E3_DCF","CSOM230B2406","CSOM230B2410","CSOM230B2411","CSOM230B2412","CSOM230BUS10T","CSOM230C1201","CSOM230C1202","CSOM230C2101","CSOM230C2110","CSOM230C2110E1","CSOM230C2110E2","CSOM230C2111","CSOM230C2112","CSOM230C2303","CSOM230C2305","CSOM230C2305E1","CSOM230C2305E2","CSOM230C2305E3","CSOM230C2305E4","CSOM230C2402","CSOM230C2402E1","CSOM230C2413","CSOM230CIC05","CSOM230CUS33","CSOM230D2101","CSOM230D2203","CSOM230DIC03","CSOM230F2102","CSOM230G1101","CSOM230G2304","CSOM230G2304E1","CSOM230G2304_DCF","CSOM230I2201","CSOM230K2101","CSOM230X2103C","CSOM230X2203","CSOM230X2404","CSOM230XDE04","CSPA100A1101","CSPA100A1103","CSPA100A1104","CSPA100A1301","CSPA100A1302","CSPA100A2101","CSPA100A2102","CSPA100A2103","CSPA100A2104","CSPA100A2108","CSPA100A2201","CSPA100A2301","CSPA100A2302","CSPA100A2303","CSPA100A2304","CSPA100A2305","CSPA100A2306","CSPA100A2307","CSPA100A2406","CSPA100AUS01","CSPA100AUS02","CSPH100A2101","CSPH100A2102","CSPH100A2103","CSPH100A2104","CSPH100A2105","CSPH100ADE01","CSPP100A-MAX-AHEAD","CSPP100A0011","CSPP100A0012","CSPP100A0013","CSPP100A0014","CSPP100A0015","CSPP100A0016","CSPP100A0018","CSPP100A0019","CSPP100A0021","CSPP100A0022","CSPP100A0023","CSPP100A0024","CSPP100A0025","CSPP100A0026","CSPP100A0027","CSPP100A0028","CSPP100A0029","CSPP100A1101","CSPP100A1102","CSPP100A1104","CSPP100A1201","CSPP100A1202","CSPP100A1301","CSPP100A1303","CSPP100A1304","CSPP100A2101","CSPP100A2102","CSPP100A2103","CSPP100A2105","CSPP100A2106","CSPP100A2108","CSPP100A2109","CSPP100A2110","CSPP100A2111","CSPP100A2112","CSPP100A2113","CSPP100A2114","CSPP100A2116","CSPP100A2118","CSPP100A2119","CSPP100A2201","CSPP100A2202","CSPP100A2203","CSPP100A2204","CSPP100A2205","CSPP100A2207","CSPP100A2208","CSPP100A2209","CSPP100A2210","CSPP100A2211","CSPP100A2212","CSPP100A2214","CSPP100A2216","CSPP100A2217","CSPP100A2218","CSPP100A2220","CSPP100A2221","CSPP100A2222","CSPP100A2223","CSPP100A2228","CSPP100A2229","CSPP100A2230","CSPP100A2231","CSPP100A2232","CSPP100A2233","CSPP100A2234","CSPP100A2235","CSPP100A2236","CSPP100A2238","CSPP100A2239","CSPP100A2240","CSPP100A2242","CSPP100A2243","CSPP100A2244","CSPP100A2252","CSPP100A2255","CSPP100A2256","CSPP100A2260","CSPP100A2262","CSPP100A2302","CSPP100A2302E1","CSPP100A2303","CSPP100A2304","CSPP100A2305","CSPP100A2306","CSPP100A2307","CSPP100A2308","CSPP100A2309","CSPP100A2310","CSPP100A2313","CSPP100A2316","CSPP100A2318","CSPP100A2319","CSPP100A2322","CSPP100A2323","CSPP100A2323E1","CSPP100A2324","CSPP100A2325","CSPP100A2327","CSPP100A2328","CSPP100A2329","CSPP100A2331","CSPP100A2332","CSPP100A2333","CSPP100A2334","CSPP100A2339","CSPP100A2340","CSPP100A2340E1","CSPP100A2343","CSPP100A2344","CSPP100A2347","CSPP100A2351","CSPP100A2353","CSPP100A2360","CSPP100A2365","CSPP100A2365E1","CSPP100A2365E2","CSPP100A2365_WDCS_TST","CSPP100A2366","CSPP100A2368","CSPP100A2370","CSPP100A2403","CSPP100A2404","CSPP100A2405","CSPP100A2406","CSPP100A2408","CSPP100A2409","CSPP100A2410","CSPP100A2411","CSPP100A2413","CSPP100AAT01","CSPP100ABE01","CSPP100ADE03","CSPP100AES02","CSPP100AFR01","CSPP100AGB01","CSPP100AIE01","CSPP100ARU01","CSPP100ATH01","CSPP100AUS02","CSPP100AUS03","CSPP100AUS07","CSPP100C2201","CSPP100E2337","CSPP100E2337A","CSPP100E2337B","CSPP100F2301","CSPP100G2301","CSPV100A2101","CSPV100A2102","CSPV100A2103","CSPV100A2104","CSPV100A2108","CSPV100A2110","CSPV100A2111","CSPV100A2112","CSPV100A2114","CSPV100A2225","CSPV100A2301","CSPV100A2301E1","CSPV100A2321","CSPV100AUS01","CSPV100AUS02","CSPV100AUS05","CSRA997A2101","CSTI5710001","CSTI5710102","CSTI5710103","CSTI5710106","CSTI5710106_CML91","CSTI5710108","CSTI5710109","CSTI5710110","CSTI5710113","CSTI5710114","CSTI5710115","CSTI5710118","CSTI5710119","CSTI5710201","CSTI5710202","CSTI5710205","CSTI571A0102E2","CSTI571A0109E2","CSTI571A0110E2","CSTI571A1101","CSTI571A1201","CSTI571A1203","CSTI571A2106","CSTI571A2107","CSTI571A2108","CSTI571A2110","CSTI571A2402","CSTI571A2403","CSTI571A2406","CSTI571AJP02","CSTI571AUS177","CSTI571B0203","CSTI571B1201","CSTI571B1202","CSTI571B2101","CSTI571B2102","CSTI571B2210","CSTI571B2222","CSTI571B2222E1","CSTI571B2223","CSTI571B2224","CSTI571B2225","CSTI571B2231","CSTI571B2404","CSTI571BDE15","CSTI571BDE59","CSTI571BFI03","CSTI571BIC08","CSTI571BIT06","CSTI571BIT15","CSTI571BIT16","CSTI571BJP07","CSTI571BUS227","CSTI571BUS282","CSTI571BUS89","CSTI571C2101","CSTI571D2101","CSTI571E2201","CSTI571E2203","CSTI571E2204","CSTI571E2205","CSTI571H2201","CSTI571H2202","CSTI571I0109","CSTI571I0114","CSTI571I1203","CSTI571I2201","CSTI571I2301","CSTI571ICN21","CSTI571K2301","CSTI571L2401","CSTI571O2101","CSTI571O2102","CSTI571O2103","CSTI571X2101","CSTI571X2102","CSTI571X2103","CSTIBDE40","CSYO380A2101","CTAP311X2101","CTAP311X2201","CTAS266X2101","CTBM100B2201","CTBM100B2202","CTBM100B2301","CTBM100BUS01","CTBM100C2301","CTBM100C2302","CTBM100C2303","CTBM100C2303E1","CTBM100C2303E2","CTBM100C2304","CTBM100C2401","CTBM100C2401E1","CTBM100C2403","CTBM100C2407","CTBM100C2411","CTBM100C2412","CTBM100CDE02","CTBM100CUS03","CTCH3460001","CTCH3460002","CTCH3460003","CTCH3460006","CTCH3460101","CTCH3460102","CTCH3460102E1","CTCH3460103","CTCH3460107","CTCH3462202","CTCH346A0005","CTCH346A1101","CTCH346A1102","CTCH346A2201","CTCH346A2202","CTCH346A2204","CTCH346A2211","CTCH346A2211E1","CTCH346B1201","CTFP561A0001","CTFP561A0002","CTFP561A0003","CTFP561A0004","CTFP561A0005","CTFP561A0006","CTFP561A0007","CTFP561A2101","CTFP561A2308","CTKA4570001","CTKA731A2101","CTKA731A2102","CTKA731A2201","CTKA731A2202","CTKI258A1101","CTKI258A1101_DCF","CTKI258A1201","CTKI258A2101","CTKI258A2101_DCF","CTKI258A2102","CTKI258A2102_DCF","CTKI258A2103","CTKI258A2103_DCF","CTKI258A2104","CTKI258A2104_DCF","CTKI258A2105","CTKI258A2105_DCF","CTKI258A2106","CTKI258A2106_DCF","CTKI258A2107","CTKI258A2107E","CTKI258A2107E_DCF","CTKI258A2107_DCF","CTKI258A2109","CTKI258A2112","CTKI258A2112_DCF","CTKI258A2116","CTKI258A2116_DCF","CTKI258A2119","CTKI258A2120","CTKI258A2124","CTKI258A2128","CTKI258A2201","CTKI258A2201_DCF","CTKI258A2202","CTKI258A2202_DCF","CTKI258A2204","CTKI258A2204_DCF","CTKI258A2208","CTKI258A2210","CTKI258A2211","CTKI258A2302","CTKI258A2X01B","CTKI258AIC02","CTKI258AUS26","CTKI258B2402","CTNDS_002_003","CTRAVATAN","CTRI4760001","CTRI4760002","CTRI4760003","CTRI4760004","CTRI4760006","CTRI4760009","CTRI4760010","CTRI4760011","CTRI4760011E1","CTRI4760013","CTRI4760021","CTRI4760022","CTRI4760025","CTRI4760025E1","CTRI4760026","CTRI4760026E1","CTRI4760027","CTRI4760028","CTRI4760028E1","CTRI4760029","CTRI4760030","CTRI4760030E1","CTRI4760031","CTRI4760032","CTRI4760033","CTRI4760033E1","CTRI4760034","CTRI4760036","CTRI4760201","CTRI47630157","CTRI476A2320","CTRI476B1101","CTRI476B1102","CTRI476B1301","CTRI476B1301E1","CTRI476BDE04","CTRI476BES04","CTRI476BRU02","CTRI476BUS36","CTRI476BUS37","CTRI476C2101","CTRI476E2337","CTRI476E2338","CTRI476E2338E01","CTRI476E2339","CTRI476E2339E01","CTRI476E2340","CTRI476E2340E01","CTRI476E2341","CTRI476E2341E01","CTRI476F2320","CTRI476F2321","CTRI476F2324","CTRI476FTRI01","CTRI476FTRI02","CTRI476FTRI02E","CTRI476G2301","CTRI476G2302","CTRI476G2303","CTRI476G2304","CTRI476G2305","CTRI476G2305E01","CTRI476MTEP1","CTRI476MTEP2","CTRI476NGB8813","CTRI476NGB90027","CTRI476NIOC02","CTRI476NIOC03","CTRI476NIOC04","CTRI476NIOC08","CTRI476NLOT03","CTRI476NPP","CTRI476OTE11","CTRI476OTE12","CTRI476OTE13","CTRI476OTE14","CTRI476OTE15","CTRI476OTE16","CTRI476OTE17","CTRI476OTE18","CTRI476OTE19","CTRI476OTE20","CTRI476OTE21","CTRI476OTE22","CTRI476OTE23","CTRI476OTE24","CTRI476OTE25","CTRI476OTE25E","CTRI476OTE26","CTRI476OTE27","CTRI476OTE28","CTRI476OTEP1","CTRI476OTEP2","CTRI476OTEP3","CTRI476OTEP5","CTRI476OTEP6","CTRI476OTEP7","CTRI476OTEP8","CTRI476OTEP9","CTRI476OTF01","CTRI476OTF01E","CTRI476OTF02","CTRI476OTF02E","CTRI476OTF04","CTRI476OTF04E","CTRI476OTF06","CTRI476OTF10","CTRI476OTF10E","CTRI476OTF11","CTRI476OTN1","CTRI476OTN2","CTRI476OTN3","CTRI476OTPE1","CTRI476OTPE1E","CTRI476OXDK01","CTRI476OXDK02","CTRI476TIGRE","CTRI476VP28","CTRI476VP55","CTRI4770001","CTRI4770002","CTRI4770003","CTRI4770004","CTRI4770006","CTRI4770007","CTST001A0001","CVAA489A-MAX-EXCITE","CVAA489A1101","CVAA489A1102","CVAA489A1104","CVAA489A1105","CVAA489A1107","CVAA489A1108","CVAA489A1301","CVAA489A1302","CVAA489A2101","CVAA489A2102","CVAA489A2104","CVAA489A2105","CVAA489A2201","CVAA489A2201E01","CVAA489A2302","CVAA489A2303","CVAA489A2305","CVAA489A2306","CVAA489A2307","CVAA489A2307E01","CVAA489A2308","CVAA489A2309","CVAA489A2310","CVAA489A2311","CVAA489A2313","CVAA489A2315","CVAA489A2316","CVAA489A2317","CVAA489A2318","CVAA489A2401","CVAA489A2402","CVAA489A2403","CVAA489A2404","CVAA489ACN02","CVAA489ADE01","CVAA489ADE02","CVAA489ADE03","CVAA489ADE06","CVAA489AFR01","CVAA489AFR02","CVAA489ATR04","CVAA489ATW01","CVAA489AUS01","CVAA489AUS02","CVAA489C2101","CVAA489C2102","CVAG624A2101","CVAH6310201","CVAH631A2302","CVAH631AT301","CVAH631AT302","CVAH631AT303","CVAH631B1101","CVAH631B1102","CVAH631B1105","CVAH631B1301","CVAH631B1303","CVAH631B1303E1","CVAH631B2101","CVAH631B2302","CVAH631B2401","CVAH631B2402","CVAH631B2403","CVAH631B2404","CVAH631B2405","CVAH631B2406","CVAH631B2406E1","CVAH631BDE06","CVAH631BDE13","CVAH631BSG02","CVAH631BTW02","CVAH631BUS04","CVAH631BUS05","CVAH631BUS06","CVAH631BUS07","CVAH631BUS08","CVAH631C2301","CVAH631C2301E1","CVAH631C2302","CVAH631C2304","CVAH631D2301","CVAH631DUS02","CVAK694A2102","CVAK694A2201","CVAK694A2205","CVAK694APRE","CVAL4890009","CVAL4890010","CVAL4890011","CVAL4890011E","CVAL4890012","CVAL4890017","CVAL4890019","CVAL4890020","CVAL4890021","CVAL4890022","CVAL4890023","CVAL4890027","CVAL4890028","CVAL4890028E","CVAL4890030","CVAL4890031","CVAL4890031E","CVAL4890033","CVAL4890037","CVAL4890038","CVAL4890039","CVAL4890047","CVAL4890048","CVAL4890050","CVAL4890051","CVAL4890102","CVAL4890103","CVAL4890104","CVAL4890105","CVAL4890106","CVAL4890107","CVAL4890107E1","CVAL4890109","CVAL4890110","CVAL4890111","CVAL4890201","CVAL4890207","CVAL4890405","CVAL4890405S1","CVAL4890601","CVAL4890602","CVAL4890603","CVAL4890604","CVAL489A1101","CVAL489A1102","CVAL489A2101","CVAL489A2102","CVAL489A2103","CVAL489A2104","CVAL489A2108","CVAL489A2301","CVAL489A2302","CVAL489A2304","CVAL489A2305","CVAL489A2307","CVAL489A2308","CVAL489A2403","CVAL489A2407","CVAL489A2408","CVAL489A2415","CVAL489A2416","CVAL489A2417","CVAL489A2418","CVAL489A2419","CVAL489A2420","CVAL489A2426","CVAL489AAU01","CVAL489ABR02","CVAL489AGB02","CVAL489AGB09","CVAL489AIT05","CVAL489AUS01","CVAL489AUS52","CVAL489AUS62","CVAL489AUS70","CVAL489B1101","CVAL489B2401","CVAL489B2402","CVAL489BUS70","CVAL489D416","CVAL489D417","CVAL489D419","CVAL489E0108","CVAL489E2301","CVAL489G2201","CVAL489G2202","CVAL489H2301","CVAL489H2301E1","CVAL489K1101","CVAL489K2101","CVAL489K2102","CVAL489K2302","CVAL489K2302E1","CVAL489K2303","CVAL489K2303E1","CVAL489K2305","CVAL489K2306","CVAL489US14","CVAS489A2101","CVAS489A2102","CVAS489A2301","CVAS489A2301E1","CVAS489A2303","CVAS489A2304","CVAS489A2305","CVAS489A2309","CVAS489A2310","CVAS489A2312","CVAS489A2316","CVAS489A2318","CVAS489A2403","CVAY736X2101","CVAY736X2201","CVAY736X2202","CVAY736X2203","CVAY736X2205","CVAY736X2207","CVAY736Y2101","CVAY736Y2102","CVEA489A2104","CVEA489A2105","CVEA489A2106","CVEA489A2301","CVEA489A2302","CVEA489A2305","CVEA489A2306","CVEA489A2310","CVEA489ABR01","CVEA489ADE01","CVEA489AUS01","CVID400W101","CVID400W102","CVIV274EM03","CVIV274EM04","CVIV274EM05","CVIV274EM06","CVIV274EM07","CVIV274EM13","CVIV274EM16","CVIV274EM35","CVIV274EM36","CVIV274EM37","CVIV274EM38","CVNP489A2101","CVNP489A2102","CVNP489A2103","CVNP489A2106","CVNP489A2203","CVOL4582201","CVOL4582202","CVOL4582203","CVOL4582204","CVOL4582205","CVOL4582206","CVOL4582301","CVOL4582302","CVOL4582303","CVOL4582304","CVOL4582305","CVOL458C2201","CVOL458C2202","CVOL458C2203","CVOL458C2204","CVOL458C2301","CVOL458C2302","CVOL458D2201","CVOL458D2202","CVOL458D2301","CVOPODUAL","CVOPOPE102","CXAA296A2101","CXBD173A2101","CXBD173A2102","CXBD173A2103","CXBD173A2105","CXBD173A2106","CXBD173A2107","CXBD173A2204","CXEH001C121","CXUO3200001","CXUO3200201","CXUO3200205","CXUO3200205E1","CXUO3200206","CXUO3200252","CXUO3200253","CXUO3200351E2","CXUO3200353E2","CXUO320B2102","CXUO320B2301","CXUO320B2302","CXUO320B2402","CXUO320B2406","CXUO320B2407","CXUO320B2408","CXUO320B2410","CXUO320B2411","CXUO320B351","CXUO320BCN01","CXUO320BIT02","CXUO320C908","CXUO320DE03","CXUO320EU01","CXUO320EU02","CXUO320EU02E1","CXUO320EU03","CXUO320F2301","CXUO320F354","CXUO320F355","CXUO320F356","CXUO320I2201","CXUO320IA01","CXUO320IEU03","CXUO320ZA01","CZAD511A2101","CZAD511A2102","CZAD511A2103","CZAD511A2202","CZAD511A2301","CZAD511A2302","CZAD511A2303","CZAD511A2304","CZAD511A2305","CZAD511A2306","CZAD511A2307","CZAD511A2308","CZAD511A2309","CZAD511A2310","CZAD511A2401","CZAD511A2402","CZAD511A2403","CZAD511A2404","CZAD511B2101","CZAD511B2201","CZOL4460001","CZOL4460002","CZOL4460003","CZOL4460003E1","CZOL4460003E2","CZOL4460004","CZOL4460005","CZOL4460006","CZOL4460007","CZOL4460007E1","CZOL4460007E2","CZOL4460007E3","CZOL4460010","CZOL4460010E","CZOL4460010E1","CZOL4460010E2","CZOL4460011","CZOL4460011E","CZOL4460011E1","CZOL4460011E2","CZOL4460017","CZOL4460018","CZOL4460029","CZOL4460031","CZOL4460035","CZOL4460035E1","CZOL4460035E2","CZOL4460036","CZOL4460037","CZOL4460038","CZOL4460039","CZOL4460039E","CZOL4460039E1","CZOL4460039E2","CZOL4460041","CZOL4460041E1","CZOL4460041E2","CZOL4460503","CZOL4460503E1","CZOL4460506","CZOL4460506E1","CZOL4460510","CZOL4460701","CZOL4460702","CZOL4460704","CZOL4460705","CZOL446CJHC1","CZOL446D1101","CZOL446D1201","CZOL446D2404","CZOL446DE01","CZOL446DJ001","CZOL446E1501","CZOL446E2104","CZOL446E2105","CZOL446E2352","CZOL446E2432","CZOL446EAIBL","CZOL446EAIBLB","CZOL446EDE02","CZOL446EDE06","CZOL446EDE15","CZOL446EDE28","CZOL446EIT01","CZOL446EIT14","CZOL446EUS016","CZOL446EUS024","CZOL446EUS032","CZOL446EUS045","CZOL446EUS129","CZOL446EUS32A2","CZOL446EUS75","CZOL446EUS93","CZOL446EUS97","CZOL446EUS99","CZOL446G2408","CZOL446G2419","CZOL446GDE13","CZOL446GDE19","CZOL446GDE21","CZOL446GGB12","CZOL446GIB01","CZOL446GUS45","CZOL446H-MAX-BD-01","CZOL446H-MAX-EG-01","CZOL446H-MAX-GT-01","CZOL446H-MAX-IN-01","CZOL446H-MAX-SA-01","CZOL446H-MAX-TH-01","CZOL446H2202","CZOL446H2202E1","CZOL446H2301","CZOL446H2301E1","CZOL446H2301E2","CZOL446H2304","CZOL446H2305","CZOL446H2310","CZOL446H2313","CZOL446H2315","CZOL446H2337","CZOL446H2337E1","CZOL446H2407","CZOL446H2409","CZOL446H2412","CZOL446HDE31","CZOL446HDE40","CZOL446HUS121","CZOL446HUS136","CZOL446H_MAX_AZURE","CZOL446H_MAX_BD01","CZOL446H_MAX_CAC01","CZOL446H_MAX_EG01","CZOL446H_MAX_IN01","CZOL446H_MAX_PK01","CZOL446H_MAX_SA01","CZOL446H_MAX_SAU01","CZOL446H_MAX_TH01","CZOL446H_MAX_TW01","CZOL446I2201","CZOL446IA03E1","CZOL446IB01","CZOL446K2401","CZOL446K2418","CZOL446K2419","CZOL446M2308","CZOL446M2309","CZOL446N2312","CZOL446O2306","NV-02B-001","NV-02B-002","NV-02B-003","NV-02B-004","NV-02B-005","NV-02B-006","NV-02B-007","NV-02B-008","NV-02B-009","NV-02B-010","NV-02B-011","NV-02B-012","NV-02B-013","NV-02B-014","NV-02B-015","NV-02B-016","NV-02B-017","NV-02B-018","NV-02B-019","NV-02B-020","NV-02B-021","NV-02B-022","NV-02B-023","NV-02B-024","NV-02B-025","NV-02B-028","NV-02C-003","NV-02C-004","QAB149_ADJUDICATION","REPORT_HF","SASDAY001","SASDAY002","SASDAY003","SASDAY004","SASDAY005","SASDAY006","SASDAY007","SASDAY008","SASDAY009","SASDAY010","SASDAY011","SASDAY012","SCS_CANADA_01","SCS_JAPAN_01","SCS_ROW_01","SDZ_RESPBUFO","SDZ_RESPFLUSA","SDZ_RESPSALBU","SDZ_RESPTIO","V9MIGRTEST","biomarker","source_pool"]

/***/ }),
/* 109 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return getYPSourcedata; });
var getYPSourcedata = function getYPSourcedata(root, _ref) {
  var pattern = _ref.pattern;
  return ['/path1', '/path2', '/'].filter(function (protocol) {
    return protocol.match(new RegExp(pattern, 'gi'));
  });
};



/***/ }),
/* 110 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return getAllEntries; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_mongodb__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_mongodb___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_mongodb__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_bluebird__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_bluebird___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_bluebird__);
function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } } function _next(value) { step("next", value); } function _throw(err) { step("throw", err); } _next(); }); }; }

/* global process */



var getAllEntries =
/*#__PURE__*/
function () {
  var _ref2 = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee(root, _ref) {
    var protocol, MONGO_URI;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            protocol = _ref.protocol;
            MONGO_URI = process.env.MONGO_URI;

            if (!(MONGO_URI && protocol && protocol.length)) {
              _context.next = 8;
              break;
            }

            _context.next = 5;
            return __WEBPACK_IMPORTED_MODULE_0_mongodb__["MongoClient"].connect(MONGO_URI, {
              promiseLibrary: __WEBPACK_IMPORTED_MODULE_1_bluebird___default.a
            }).then(function (db) {
              var query = {
                'protocol': {
                  $regex: '.*' + protocol.toUpperCase() + '.*'
                }
              };
              return db.db('mm').collection('entries').find(query).toArray().finally(db.close());
            });

          case 5:
            return _context.abrupt("return", _context.sent);

          case 8:
            return _context.abrupt("return", []);

          case 9:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, this);
  }));

  return function getAllEntries(_x, _x2) {
    return _ref2.apply(this, arguments);
  };
}();



/***/ }),
/* 111 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__createNewEntry__ = __webpack_require__(112);

/* harmony default export */ __webpack_exports__["a"] = ({
  createNewEntry: __WEBPACK_IMPORTED_MODULE_0__createNewEntry__["a" /* createNewEntry */]
});

/***/ }),
/* 112 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return createNewEntry; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_mongodb__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_mongodb___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_mongodb__);
function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } } function _next(value) { step("next", value); } function _throw(err) { step("throw", err); } _next(); }); }; }

/* global process */


var createNewEntry =
/*#__PURE__*/
function () {
  var _ref2 = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee3(root, _ref) {
    var meta, MONGO_URI, message, entry;
    return regeneratorRuntime.wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            meta = _ref.meta;
            // getting MONGO_URI from environment variables
            MONGO_URI = process.env.MONGO_URI;
            // creation event message variable
            entry = _extends({}, meta, {
              visible: false,
              archive: false,
              creation: new Date()
            });

            if (!MONGO_URI) {
              _context3.next = 8;
              break;
            }

            _context3.next = 6;
            return __WEBPACK_IMPORTED_MODULE_0_mongodb__["MongoClient"].connect(MONGO_URI,
            /*#__PURE__*/
            function () {
              var _ref3 = _asyncToGenerator(
              /*#__PURE__*/
              regeneratorRuntime.mark(function _callee2(err, db) {
                var dbo, count;
                return regeneratorRuntime.wrap(function _callee2$(_context2) {
                  while (1) {
                    switch (_context2.prev = _context2.next) {
                      case 0:
                        _context2.next = 2;
                        return db.db('mm');

                      case 2:
                        dbo = _context2.sent;
                        _context2.next = 5;
                        return dbo.collection('entries').find(meta).count();

                      case 5:
                        count = _context2.sent;

                        if (!(count === 0)) {
                          _context2.next = 14;
                          break;
                        }

                        _context2.next = 9;
                        return dbo.collection('entries').insertOne(entry,
                        /*#__PURE__*/
                        function () {
                          var _ref4 = _asyncToGenerator(
                          /*#__PURE__*/
                          regeneratorRuntime.mark(function _callee(err) {
                            return regeneratorRuntime.wrap(function _callee$(_context) {
                              while (1) {
                                switch (_context.prev = _context.next) {
                                  case 0:
                                    if (!err) {
                                      _context.next = 2;
                                      break;
                                    }

                                    throw err;

                                  case 2:
                                    _context.next = 4;
                                    return db.close();

                                  case 4:
                                  case "end":
                                    return _context.stop();
                                }
                              }
                            }, _callee, this);
                          }));

                          return function (_x5) {
                            return _ref4.apply(this, arguments);
                          };
                        }());

                      case 9:
                        _context2.next = 11;
                        return {
                          success: 'New Metadata Manager entry created.'
                        };

                      case 11:
                        message = _context2.sent;
                        _context2.next = 17;
                        break;

                      case 14:
                        _context2.next = 16;
                        return {
                          error: 'Metadata Manager entry already exists.'
                        };

                      case 16:
                        message = _context2.sent;

                      case 17:
                      case "end":
                        return _context2.stop();
                    }
                  }
                }, _callee2, this);
              }));

              return function (_x3, _x4) {
                return _ref3.apply(this, arguments);
              };
            }());

          case 6:
            _context3.next = 11;
            break;

          case 8:
            _context3.next = 10;
            return {
              error: '`MONGO_URI` environment variable is not defined.'
            };

          case 10:
            message = _context3.sent;

          case 11:
            return _context3.abrupt("return", JSON.stringify(message));

          case 12:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee3, this);
  }));

  return function createNewEntry(_x, _x2) {
    return _ref2.apply(this, arguments);
  };
}();



/***/ }),
/* 113 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Types__ = __webpack_require__(114);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__Inputs__ = __webpack_require__(121);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__Query__ = __webpack_require__(122);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__Mutation__ = __webpack_require__(123);
function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }





/* harmony default export */ __webpack_exports__["a"] = (_toConsumableArray(__WEBPACK_IMPORTED_MODULE_0__Types__["a" /* default */]).concat(_toConsumableArray(__WEBPACK_IMPORTED_MODULE_1__Inputs__["a" /* default */]), _toConsumableArray(__WEBPACK_IMPORTED_MODULE_2__Query__["a" /* default */]), _toConsumableArray(__WEBPACK_IMPORTED_MODULE_3__Mutation__["a" /* default */])));

/***/ }),
/* 114 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__DefaultVariable__ = __webpack_require__(115);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__Domain__ = __webpack_require__(116);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__EntryMM__ = __webpack_require__(117);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__Settings__ = __webpack_require__(118);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__Standard__ = __webpack_require__(119);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__YPVariable__ = __webpack_require__(120);
function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }







/* harmony default export */ __webpack_exports__["a"] = (_toConsumableArray(__WEBPACK_IMPORTED_MODULE_0__DefaultVariable__["a" /* default */]).concat(_toConsumableArray(__WEBPACK_IMPORTED_MODULE_1__Domain__["a" /* default */]), _toConsumableArray(__WEBPACK_IMPORTED_MODULE_2__EntryMM__["a" /* default */]), _toConsumableArray(__WEBPACK_IMPORTED_MODULE_3__Settings__["a" /* default */]), _toConsumableArray(__WEBPACK_IMPORTED_MODULE_4__Standard__["a" /* default */]), _toConsumableArray(__WEBPACK_IMPORTED_MODULE_5__YPVariable__["a" /* default */])));

/***/ }),
/* 115 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony default export */ __webpack_exports__["a"] = (["type DefaultVariable {\n    name: String\n    domain: String\n    code: String\n  }"]);

/***/ }),
/* 116 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony default export */ __webpack_exports__["a"] = (["type Domain {\n    name: String\n    code: String\n  }\n  "]);

/***/ }),
/* 117 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony default export */ __webpack_exports__["a"] = (["\n  type EntryMM {\n    protocol: String\n    sourcedata: String\n    domain: String\n    standard: String\n    preprocessing: Boolean\n  }\n\n  "]);

/***/ }),
/* 118 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony default export */ __webpack_exports__["a"] = (["\n  type EntryMMSettings {\n    _id: ID\n    protocol: String\n    sourcedata: String\n    domain: String\n    standard: String\n    flag: String\n  }\n\n  type DomainMMSettings {\n    _id: ID\n    name: String\n    label: String\n    flag: String\n  }\n\n  type StandardMMSettings {\n    _id: ID\n    name: String\n    label: String\n    flag: String\n  }\n\n  type TargetMMSettings {\n    _id: ID\n    label: String\n    flag: String\n  }\n\n  type DefaultVariableMMSettings {\n    _id: ID\n    varName: String\n    varLabel: String\n    sourceVarName: String\n    domainName: String\n    standardName: String\n    flag: String\n  }\n\n  type CommitLogMessage {\n    _id: ID\n    protocol: String\n    domain: String\n    standard: String\n    message: String\n    timestamp: String\n    messageType: String\n    username: String\n    entryId: String\n  }\n\n  "]);

/***/ }),
/* 119 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony default export */ __webpack_exports__["a"] = (["type Standard {\n    name: String\n    code: String\n  }\n  "]);

/***/ }),
/* 120 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony default export */ __webpack_exports__["a"] = (["type YPVariable {\n    varname: String\n    label: String\n  }\n  "]);

/***/ }),
/* 121 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony default export */ __webpack_exports__["a"] = (["\n    input EntryMM_Meta {\n      protocol: String,\n      preprocessing: Boolean,\n      sourcedata: String,\n      domain: String,\n      standard: String\n    }\n  "]);

/***/ }),
/* 122 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony default export */ __webpack_exports__["a"] = (["type Query {\n    getAllStandards: [Standard]\n    getAllDomains: [Domain]\n    getAllTargets: [String]\n    getYPProtocols(pattern: String): [String]\n    getYPSourcedata(pattern: String): [String]\n    getAllEntries(protocol: String): [EntryMM]\n    # Not checked\n    # getAllEntriesSettings: [EntryMMSettings]\n    # getAllDomainsSettings(flag: String): [DomainMMSettings]\n    # getAllStandardsSettings(flag: String): [StandardMMSettings]\n    # getAllTargetsSettings(flag: String): [TargetMMSettings]\n    # getAllDefaultVariablesSettings(flag: String): [DefaultVariableMMSettings]\n    # getAllNotifications(protocol: String, domain: String, standard: String): [CommitLogMessage]\n  }"]);

/***/ }),
/* 123 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony default export */ __webpack_exports__["a"] = (["type Mutation {\n    createNewEntry(meta: EntryMM_Meta): String\n  }"]);

/***/ }),
/* 124 */
/***/ (function(module, exports) {

module.exports = require("https");

/***/ }),
/* 125 */
/***/ (function(module, exports) {

module.exports = require("fs");

/***/ })
/******/ ]);
//# sourceMappingURL=server.js.map